package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.drawerlayout.widget.DrawerLayout
import com.tbs.generic.vansales.Model.LoginDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.AdminLoginRequest
import com.tbs.generic.vansales.Requests.LoginTimeCaptureRequest
import com.tbs.generic.vansales.collector.CollectorCustomerActivity
import com.tbs.generic.vansales.utils.AppPrefs
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.net.NetworkInterface
import java.text.SimpleDateFormat
import java.util.*


class LoginActivity : BaseActivity() {
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String
    lateinit var llSplash: RelativeLayout
    internal lateinit var companyId: String

    override fun initializeControls() {
    }

    override fun initialize() {

        llSplash = layoutInflater.inflate(R.layout.activity_login, null) as RelativeLayout
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        changeLocale()
        toolbar.visibility = View.GONE
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        val btnLogin = findViewById<Button>(R.id.btnLogin)
        val etUserName = findViewById<EditText>(R.id.etUserName)
        val etPassword = findViewById<EditText>(R.id.etPassword)
        val ivLogo = findViewById<ImageView>(R.id.ivLogo)
        val etCompanyId = findViewById<EditText>(R.id.etCompanyId)
        var img = preferenceUtils.getStringFromPreference(PreferenceUtils.COMPANY_IMG, "")
        if(img!=null){
            try {
               var bitmap  = null

                val decodedString = android.util.Base64.decode(img, android.util.Base64.DEFAULT)
                val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
                if(decodedByte.sameAs(bitmap)){
                    ivLogo.setImageDrawable(resources.getDrawable(R.drawable.tema_logo))

                }else{
                    ivLogo.setImageBitmap(decodedByte)

                }
            }catch (e:java.lang.Exception){
                ivLogo.setImageDrawable(resources.getDrawable(R.drawable.tema_logo))

            }

        }
        etCompanyId.setText("" + resources.getString(R.string.company_name))
        etUserName.requestFocus()

//        etUserName.setText("RENUKA")
//        etPassword.setText("TU024")

//        applyFont(etUserName, AppConstants.Mo)
//        applyFont(etPassword, AppConstants.Goudy_Old_Style_Bold)
//        applyFont(etCompanyId\, AppConstants.Goudy_Old_Style_Bold)
//        applyFont(tvCopyRight, AppConstants.Goudy_Old_Style_Normal)

//        etCompanyId.setText("Brothers Gas")
//        etUserName.setText("MY003")
//        etPassword.setText("12345")

        btnLogin.setOnClickListener {
            Util.preventTwoClick(it)
            dbUser = etUserName.text.toString().trim()
            dbPass = etPassword.text.toString().trim()
            companyId = etCompanyId.text.toString().trim()

            if (companyId.length == 0) {
                showAlert("" + resources.getString(R.string.login_company_id))
            } else if (dbUser.length == 0) {
                showAlert("" + resources.getString(R.string.login_username))
            } else if (dbPass.length == 0) {
                showAlert("" + resources.getString(R.string.login_password))
            } else if (isExpired()) {
                showAppCompatAlert("" + resources.getString(R.string.login_info),
                        "" + resources.getString(R.string.login_licence),
                        "" + resources.getString(R.string.ok), "",
                        "" + resources.getString(R.string.from_expired), false)
            } else {
                loginSuccess()
            }

        }


        val ivConfiguration = findViewById<ImageView>(R.id.ivConfiguration)

        ivConfiguration.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this@LoginActivity, ConfigurationActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
    }

    override fun onButtonYesClick(from: String) {
        if (from.equals(resources.getString(R.string.from_expired), true)) {

        }
        if (from.equals("PLAYSTORE", true)) {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }
    }

    private fun isExpired(): Boolean {
        try {
            val dateFormat = "dd/MM/yyyy"
            val expireDate = "30/11/2022"
            val sdf = SimpleDateFormat(dateFormat, Locale.getDefault()) // 28-01-2019
            val today = getToday(dateFormat)
            val eDate = sdf.parse(expireDate)
            val curDate = sdf.parse(today)
            if (eDate.compareTo(curDate) <= 0) {
                return true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    private fun getToday(format: String): String {
        val date = Date()
        return SimpleDateFormat(format).format(date)
    }

    private fun loginSuccess() {
        if (Util.isNetworkAvailable(this)) {
           var macaddress= getMacAddr()
            val adminLoginRequest = AdminLoginRequest(macaddress,this@LoginActivity)
            adminLoginRequest.setOnResultListener(object : AdminLoginRequest.OnResultListener {
                override fun onCompleted(isError: Boolean, isValid: Boolean, loginDO: LoginDO) {
                    if (isError) {
                        if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        } else {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        }
                    } else {
                        if (loginDO.flag == 1) {
                            if (loginDO.version.equals(resources.getString(R.string.app_version))) {

                                preferenceUtils = PreferenceUtils(this@LoginActivity)
                                preferenceUtils.saveString(PreferenceUtils.USER_NAME, dbUser)
                                preferenceUtils.saveString(PreferenceUtils.PASSWORD, dbPass)
                                preferenceUtils.saveString(PreferenceUtils.PROFILE_PICTURE, loginDO.Image)
                                preferenceUtils.saveString(PreferenceUtils.DRIVER_ID, loginDO.driverCode)
                                AppPrefs.putString(AppPrefs.DRIVER_ID, loginDO.driverCode)
                                preferenceUtils.saveString(PreferenceUtils.B_SITE_ID, loginDO.site)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_DRIVER_NAME, loginDO.driverName)
                                preferenceUtils.saveInt(PreferenceUtils.CHEQUE_DATE, loginDO.chequeDays)
                                preferenceUtils.saveString(PreferenceUtils.B_SITE_NAME, loginDO.siteDescription)
                                preferenceUtils.saveString(PreferenceUtils.COMPANY, loginDO.company)
                                preferenceUtils.saveString(PreferenceUtils.COMPANY_DES, loginDO.companyDescription)
                                preferenceUtils.saveString(PreferenceUtils.COMPANY, companyId)
                                preferenceUtils.saveInt(PreferenceUtils.USER_ROLE, loginDO.role)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_EMAIL, loginDO.email)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_SUCCESS, "" + resources.getString(R.string.from_success))
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_TIME_FORMAT, loginDO.time)
                                preferenceUtils.saveString(PreferenceUtils.LOGIN_DATE_FORMAT, loginDO.date)
                                preferenceUtils.saveString(PreferenceUtils.SEQUENCE, loginDO.sequence)

                                loginTimeCapture()

                                var role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
                                if (role == 10) {
                                    val intent = Intent(this@LoginActivity, DashBoardActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else if (role == 20) {
                                    val intent = Intent(this@LoginActivity, DashBoardActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else if (role == 30) {
                                    val intent = Intent(this@LoginActivity, CollectorCustomerActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    startActivity(intent)
                                    finish()
                                } else {
                                    showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_user_not_available), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                                }

//                            loginMultipleUsersCapture()
                            } else {
                                showAppCompatAlert("" + resources.getString(R.string.login_info), "" + resources.getString(R.string.playstore_messsage), "" + resources.getString(R.string.ok), "Cancel", "PLAYSTORE", true)
                            }

                        } else if (loginDO.flag == 0) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_invalid), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDO.flag == 2) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_inactive), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else if (loginDO.flag == 3) {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.login_another_device), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                        } else {
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "").isEmpty()) {
                                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                            } else {
                                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                            }
                        }
                    }
                }
            })
            adminLoginRequest.execute(dbUser, dbPass)
        } else {
            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.internet_connection), "" + resources.getString(R.string.ok), "", "", false)

        }
    }

    private fun loginTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = LoginTimeCaptureRequest(driverId, date, date, time, this@LoginActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
//                    Toast.makeText(this@LoginActivity, "Failed", Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 1) {
//                       showToast("Success")

                        } else {
//                        showToast("Success")

                        }

                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), ""+resources.getString(R.string.ok), "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }


    }

    fun getMacAddr(): String? {
        try {
            val all: List<NetworkInterface> = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (nif in all) {
                if (!nif.getName().equals("wlan0")) continue
                val macBytes: ByteArray = nif.getHardwareAddress() ?: return ""
                val res1 = StringBuilder()
                for (b in macBytes) { //res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b))
                }
                if (res1.length > 0) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString()
            }
        } catch (ex: java.lang.Exception) {
        }
        return "02:00:00:00:00:00"
    }
    override fun onBackPressed() {
    }
}