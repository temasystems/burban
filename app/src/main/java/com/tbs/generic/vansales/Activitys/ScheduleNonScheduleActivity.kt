package com.tbs.generic.vansales.Activitys


import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.generic.vansales.Adapters.VehicleRouteAdapter
import com.tbs.generic.vansales.Model.PickUpDo
import com.tbs.generic.vansales.Model.VanStockDetailsDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CheckinTimeCaptureRequest
import com.tbs.generic.vansales.Requests.VRTimeCaptureRequest
import com.tbs.generic.vansales.Requests.VanStockRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.fragments.NonScheduleStockFragment
import com.tbs.generic.vansales.fragments.ScheduleStockFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.*
import kotlinx.android.synthetic.main.activity_schedule_non_schedule.*
import kotlinx.android.synthetic.main.configuration_layout.*

class ScheduleNonScheduleActivity : BaseActivity(),
        ScheduleStockFragment.OnFragmentInteractionListener,
        NonScheduleStockFragment.OnFragmentInteractionListener {


    private var vanStockDetails: VanStockDetailsDO? = null
    private var rlPartyActivity: RelativeLayout? = null
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView

    var flag: Int = 0
    var reading = 0.0
    private var screenType: String = ""

    override fun initialize() {
        rlPartyActivity = layoutInflater.inflate(R.layout.activity_schedule_non_schedule, null) as RelativeLayout
        llBody.addView(rlPartyActivity, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
        if (intent.hasExtra(Constants.SCREEN_TYPE)) {
            screenType = intent.extras?.getString(Constants.SCREEN_TYPE)!!

            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                tv_title.text = getString(R.string.stock_)
            } else {

                tv_title.text = screenType
            }

        } else {
            tv_title.text = getString(R.string.stock_)
        }


        getDataFromServer()

    }

    private fun getDataFromServer() {

        try {
            val loasStck = VanStockRequest(this);
            loasStck.setOnResultListener { isError, vanStockDetails ->
                if (isError) {

                    Util.showToast(this, getString(R.string.error_messge))
                    finish()
                    return@setOnResultListener
                }
                if (vanStockDetails != null) {
                    this.vanStockDetails = vanStockDetails
                    Util.startNewFragment(this, R.id.container, ScheduleStockFragment.newInstance(vanStockDetails, ResultListner { `object`, isSuccess ->

                        if (isSuccess) {
                            enableButtons()
                        }
                    }), "ScheduleStockFragment", false)
                }

            }
            loasStck.execute()
        } catch (e: Exception) {
            e.localizedMessage
        }
    }

    override fun initializeControls() {
    }


    override fun onFragmentNonScheduleStockInteraction() {
        Util.startNewFragment(this, R.id.container, NonScheduleStockFragment.newInstance(vanStockDetails!!, ResultListner { `object`, isSuccess ->
            if (isSuccess) {
                enableButtons()
            }

        }), "NonScheduleStockFragment", false)
    }

    override fun onFragmentScheduleStockInteraction() {
        Util.startNewFragment(this, R.id.container, ScheduleStockFragment.newInstance(this.vanStockDetails!!, ResultListner { `object`, isSuccess ->

            if (isSuccess) {
                enableButtons()
            }
        }), "ScheduleStockFragment", false)
    }

    private fun enableButtons() {
        if (!TextUtils.isEmpty(screenType)) {
            if (screenType.equals(getString(R.string.confirm_stock), true)) {
                if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false))
                    ll_buttons.visibility = View.GONE
                else {
                    ll_buttons.visibility = View.VISIBLE
                    if (AppPrefs.getBoolean(AppPrefs.STOCK_CONFIRMED, false)) {
                        btn_checkin.visibility = View.VISIBLE
                        btn_confirm.visibility = View.GONE
                    } else {
                        btn_checkin.visibility = View.GONE
                        btn_confirm.visibility = View.VISIBLE

                    }
                }

                btn_confirm.setOnClickListener({
//                  if(preferenceUtils.getStringFromPreference(PreferenceUtils.INSTRUCTIONS,"").isNotEmpty()){
//                      showInstructions()
//                  }else{

                      YesOrNoDialogFragment().newInstance("", getString(R.string.are_you_sure_you_want_confirm_load), ResultListner { `object`, isSuccess ->
                          if (isSuccess) {
                              var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
                              vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
                              vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
                              vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()
                              AppPrefs.putBoolean(AppPrefs.STOCK_CONFIRMED, true)
                              StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
                              enableButtons()
                              btn_checkin.performClick()
                          }
                      }, true).show(supportFragmentManager, "YesOrNoDialogFragment")
//                  }



                })

                btn_checkin.setOnClickListener({
                    val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)

                    if (vehicleCheckInDo.checkInStatus.equals(resources.getString(R.string.checkin_vehicle))) {
                        YesOrNoDialogFragment().newInstance("Title", getString(R.string.already_checked_in), ResultListner { `object`, isSuccess -> }, false).show(supportFragmentManager, "YesOrNoDialogFragment")
                    } else if (vehicleCheckInDo.loadStock.equals("")) {
                        YesOrNoDialogFragment().newInstance("Title", getString(R.string.please_complete_load_stock), ResultListner { `object`, isSuccess -> }, false).show(supportFragmentManager, "YesOrNoDialogFragment")
                    } else {
                        var stringBuilder = StringBuilder()
                        stringBuilder.append(resources.getString(R.string.check_in_at) + " ")
                        stringBuilder.append(CalendarUtils.getCurrentDate(this))
                        stringBuilder.append(" ?")
                        YesOrNoDialogFragment().newInstance("", stringBuilder.toString(), ResultListner { `object`, isSuccess ->
                            if (isSuccess) {
                                showLoader()
                                location(ResultListner { `object`, isSuccess ->
                                    if (isSuccess) {
                                        vehicleCheckInDo.checkinLattitude = lattitudeFused
                                        vehicleCheckInDo.checkinLongitude = longitudeFused
                                        StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)

                                        hideLoader()
                                        stopLocationUpdates()
                                        checkInSuccess()

                                    }
                                })
                            }

                        }, true).show(supportFragmentManager, "YesOrNoDialogFragment")
                    }
                })
            } else {
                ll_buttons.visibility = View.GONE
            }
        }


    }

    private fun checkInSuccess() {

        if (Util.isNetworkAvailable(this)) {
            checkinTimeCapture()

        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }


    }

    private fun showInstructions() {
        val dialog = Dialog(this)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.instruction_vr_dialog)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        dialog.window!!.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val tvInstruction = dialog.findViewById<TextView>(R.id.tvInstruction)
        val tvClose = dialog.findViewById<TextView>(R.id.tvClose)
        tvInstruction.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.INSTRUCTIONS, ""))

        tvClose.setOnClickListener {
            dialog.dismiss()

            YesOrNoDialogFragment().newInstance("", getString(R.string.are_you_sure_you_want_confirm_load), ResultListner { `object`, isSuccess ->
                if (isSuccess) {
                    var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
                    vehicleCheckInDo.loadStock = resources.getString(R.string.loaded_stock)
                    vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
                    vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()
                    AppPrefs.putBoolean(AppPrefs.STOCK_CONFIRMED, true)
                    StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
                    enableButtons()
                    btn_checkin.performClick()
                }
            }, true).show(supportFragmentManager, "YesOrNoDialogFragment")
        }
        dialog.show()
    }

    private fun vrSuccess() {

        val siteListRequest = VRTimeCaptureRequest(1, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            if (isError) {
                hideLoader()
                val intent = Intent(this, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                overridePendingTransition(R.anim.enter, R.anim.exit)
                AppPrefs.putBoolean(AppPrefs.CHECK_IN_DONE, true)
            } else {
                hideLoader()
                if (loginDo.flag == 20) {
                    val intent = Intent(this, DashBoardActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                    AppPrefs.putBoolean(AppPrefs.CHECK_IN_DONE, true)

                } else {
                    val intent = Intent(this, DashBoardActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    overridePendingTransition(R.anim.enter, R.anim.exit)
                    AppPrefs.putBoolean(AppPrefs.CHECK_IN_DONE, true)
                }

            }


        }

        siteListRequest.execute()


    }

    private fun checkinTimeCapture() {
        var driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        var date = CalendarUtils.getDate()
        var time = CalendarUtils.getTime()
//        08-01-2019   10:30
        var idd = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")


        var vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
        vehicleCheckInDo.checkinTimeCaptureArrivalDate = date
        vehicleCheckInDo.checkinTimeCaptureArrivalTime = time
        vehicleCheckInDo.checkinTimeCaptureStartLoadingTime = time
        vehicleCheckInDo.checkinTimeCaptureStartLoadingDate = date
        vehicleCheckInDo.checkinTimeCaptureEndLoadingTime = time
        vehicleCheckInDo.checkinTimeCaptureEndLoadingDate = date
        vehicleCheckInDo.checkinTimeCaptureVehicleInspectionTime = time
        vehicleCheckInDo.checkinTimeCaptureVehicleInspectionDate = date
        vehicleCheckInDo.checkinTimeCaptureGateInspectionTime = time
        vehicleCheckInDo.checkinTimeCaptureGateInspectionDate = date
        vehicleCheckInDo.checkinTimeCapturCheckinDate = date
        vehicleCheckInDo.checkinTimeCaptureCheckinTime = time
        preferenceUtils.saveInt(PreferenceUtils.OPERATION, 3)

        StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)


        val siteListRequest = CheckinTimeCaptureRequest(driverId, date, idd, this)
        siteListRequest.setOnResultListener { isError, loginDo ->
            hideLoader()
            if (loginDo != null) {
                if (isError) {
                    hideLoader()
                    showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                } else {
                    if (loginDo.flag == 1) {
                        if (loginDo.version.equals(resources.getString(R.string.app_version))) {
                            preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, CalendarUtils.getCurrentDate(this))

                            preferenceUtils.saveString(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, idd)

                            var vCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
                            var aTime = preferenceUtils.getStringFromPreference(PreferenceUtils.A_TIME, "")
                            var aDate = preferenceUtils.getStringFromPreference(PreferenceUtils.A_DATE, "")
                            var dDate = preferenceUtils.getStringFromPreference(PreferenceUtils.D_DATE, "")
                            var dTime = preferenceUtils.getStringFromPreference(PreferenceUtils.D_TIME, "")
                            var vPlate = preferenceUtils.getStringFromPreference(PreferenceUtils.V_PLATE, "")
                            var nShipments = preferenceUtils.getStringFromPreference(PreferenceUtils.N_SHIPMENTS, "")

                            preferenceUtils.saveString(PreferenceUtils.CVEHICLE_CODE, vCode)
                            preferenceUtils.saveString(PreferenceUtils.CA_TIME, aTime)
                            preferenceUtils.saveString(PreferenceUtils.CA_DATE, aDate)
                            preferenceUtils.saveString(PreferenceUtils.CD_DATE, dDate)
                            preferenceUtils.saveString(PreferenceUtils.CD_TIME, dTime)
                            preferenceUtils.saveString(PreferenceUtils.CV_PLATE, vPlate)
                            preferenceUtils.saveString(PreferenceUtils.CN_SHIPMENTS, nShipments)
                            preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.checkin_vehicle))
                            preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.checkin_vehicle))
                            val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this)
                            vehicleCheckInDo.checkInStatus = resources.getString(R.string.checkin_vehicle)//resources.getString(R.string.checkin_vehicle)
                            vehicleCheckInDo.checkInTime = CalendarUtils.getCurrentDate(this)
                            vehicleCheckInDo.checkinTimeCaptureCheckinTime = CalendarUtils.getTime()
                            vehicleCheckInDo.checkinTimeCapturCheckinDate = CalendarUtils.getDate()
                            vehicleCheckInDo.actualvehicleCheckin = CalendarUtils.getCurrentDate(this)
                            StorageManager.getInstance(this).deleteVehCheckOutData(this)

                            var routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
                            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, routeId)
                            StorageManager.getInstance(this).insertCheckInData(this, vehicleCheckInDo)
//                            val intent = Intent(this, DashBoardActivity::class.java)
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                            startActivity(intent)
//                            overridePendingTransition(R.anim.enter, R.anim.exit)
//                            AppPrefs.putBoolean(AppPrefs.CHECK_IN_DONE, true)
                            vrSuccess()
                        } else {
                            hideLoader()
                            showAppCompatAlert("" + resources.getString(R.string.login_info), "" + resources.getString(R.string.playstore_messsage), "" + resources.getString(R.string.ok), "Cancel", "PLAYSTORE", true)
                        }

                    } else if (loginDo.flag == 30) {
                        hideLoader()
                        Util.showToast(this, getString(R.string.force_logout))
                        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID)
                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS)
                        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this)
                        AppPrefs.clearPref(this)
                        clearDataLocalData()
                    } else {
                        hideLoader()

                        showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

                    }

                }
            } else {
                hideLoader()
                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.server_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

            }

        }

        siteListRequest.execute()
    }

    private fun clearDataLocalData() {
        val preferenceUtils = PreferenceUtils(this)

        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
        preferenceUtils.removeFromPreference(PreferenceUtils.OPENING_READING)
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_UNIT)

        preferenceUtils.removeFromPreference(PreferenceUtils.CLOSING_READING)
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
        AppPrefs.clearPref(this)
        StorageManager.getInstance(this).deleteCheckInData(this as Activity?)
        StorageManager.getInstance(this).deleteCompletedShipments(this as Activity?)
        StorageManager.getInstance(this).deleteSkipShipmentList(this as Activity?)
        StorageManager.getInstance(this).deleteVanNonScheduleProducts(this as Activity?)
        StorageManager.getInstance(this).deleteVanScheduleProducts(this as Activity?)
        StorageManager.getInstance(this).deleteSpotSalesCustomerList(this as Activity?)
        StorageManager.getInstance(this).deleteVehicleInspectionList(this as Activity?)
        StorageManager.getInstance(this).deleteGateInspectionList(this as Activity?)
        StorageManager.getInstance(this).deleteSiteListData(this as Activity?)
        StorageManager.getInstance(this).deleteShipmentListData(this as Activity?)
        StorageManager.getInstance(this).deleteCheckOutData(this)
        StorageManager.getInstance(this).deleteDepartureData(this as Activity?)
        StorageManager.getInstance(this).deleteUpdateData(this as Activity?)
        StorageManager.getInstance(this).deleteVehCheckOutData(this)

        StorageManager.getInstance(this).deleteScheduledNonScheduledReturnData()// clearing all tables
//        preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.logged_in))
//        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.logged_in))
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

}