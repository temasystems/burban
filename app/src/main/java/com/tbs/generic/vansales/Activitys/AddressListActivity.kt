package com.tbs.generic.vansales.Activitys

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.AddressAdapter
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.AddressListRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*
import java.util.ArrayList


class AddressListActivity : BaseActivity() {
    lateinit var invoiceAdapter: AddressAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    var fromId = 0
    lateinit var code: String
    lateinit var customer: String
    lateinit var customerDo: CustomerDo
    var type=0
    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.address_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
        tv_title.text = getString(R.string.address_title)
        act_toolbar.setNavigationOnClickListener {
            Util.preventTwoClick(it)
            finish()
        }
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.text = resources.getString(R.string.address_title)
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)

        recycleview.layoutManager = linearLayoutManager

        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@AddressListActivity)


        if (intent.hasExtra("CODE")) {
            customer = intent.extras!!.getString("CODE").toString()
        } else {
            customer = ""
        }
        if (intent.hasExtra("CUSTOMER_DO")) {
            customerDo = intent.extras!!.getSerializable("CUSTOMER_DO") as CustomerDo
        }
        if (intent.hasExtra("TYPE")) {
            type= intent.extras?.getInt("TYPE")!!
        }
        if (Util.isNetworkAvailable(this)) {

            val addressListRequest = AddressListRequest(customer, type,this@AddressListActivity)
            addressListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    Toast.makeText(this@AddressListActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                } else {

                    if (invoiceHistoryMainDO.addressDOS.size > 0) {
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                        if (intent.hasExtra("MASTER")) {
                            code = intent.extras!!.getString("MASTER").toString()
                        } else {
                            code = ""
                        }
                        if (code.length > 0) {
                            invoiceAdapter = AddressAdapter(this@AddressListActivity,type, invoiceHistoryMainDO.addressDOS, code, customer, customerDo)

                            recycleview.adapter = invoiceAdapter
                        } else {
                            invoiceAdapter = AddressAdapter(this@AddressListActivity,type, invoiceHistoryMainDO.addressDOS, "", customer, customerDo)

                            recycleview.adapter = invoiceAdapter
                        }

                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }


                }
            }
            addressListRequest.execute()
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 63 && resultCode == 63) {
            finish()
        } else if (requestCode == 63 && resultCode == 19) {
            if (data != null && data.hasExtra("CapturedReturns") && data.getBooleanExtra("CapturedReturns", false)) {

                val intent = Intent()
                intent.putExtra("CapturedReturns", AppConstants.CapturedReturns)
                setResult(19, intent)
                finish()
                // uncomment if required
            }
        }
    }

}