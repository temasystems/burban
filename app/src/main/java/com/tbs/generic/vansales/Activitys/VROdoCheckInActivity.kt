package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.generic.vansales.Adapters.ReasonListAdapter
import com.tbs.generic.vansales.Model.PickUpDo
import com.tbs.generic.vansales.Model.TrailerSelectionDO
import com.tbs.generic.vansales.Model.VehicleCheckInDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.LoadBayRequest
import com.tbs.generic.vansales.Requests.LogoutTimeCaptureRequest
import com.tbs.generic.vansales.Requests.ReasonsDashboardListRequest
import com.tbs.generic.vansales.Requests.StartRouteService
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.*
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.reading_dialog.*
import kotlinx.android.synthetic.main.vr_odo_check_in_screen.*
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.ArrayList


class VROdoCheckInActivity : BaseActivity() {
    lateinit var vehicleCheckInDo: VehicleCheckInDo

    lateinit var preferenceUtilss: PreferenceUtils
    lateinit var dialog: Dialog
    lateinit var tvLoad: TextView

    var trailer1 = ""
    var trailer2 = ""
    var equipment1 = ""
    var equipment2 = ""
    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.vr_odo_check_in_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        preferenceUtilss = PreferenceUtils(this@VROdoCheckInActivity)
        flToolbar.visibility = View.GONE
        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        changeLocale()
        tv_title.text = getString(R.string.select_vr_screen_in)

    }

    override fun onResume() {
        super.onResume()
        initializeControls()
    }

    override fun initializeControls() {


        ll_vehicle_route.setBackgroundResource(R.drawable.card_white)
        ll_odo_meter_read.setBackgroundResource(R.drawable.card_white)
        ll_check_in_odo.setBackgroundResource(R.drawable.card_white)
        tvLoad = findViewById(R.id.tvLoad)
        val odoreading = preferenceUtils.getIntFromPreference(PreferenceUtils.ODO_READ, 0)
        val loadBay = preferenceUtils.getStringFromPreference(PreferenceUtils.LOAD_BAY_VALUE, "")

        val startRoute = preferenceUtils.getIntFromPreference(PreferenceUtils.START_ROUTE, 0)
        val routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        var equipDos = StorageManager.getInstance(this).getEquipmentSelectionDO(this)
        var serialequipDos = StorageManager.getInstance(this).getSerialEquipmentSelectionDO(this)
        var isUpdated = false

        for (k in serialequipDos.indices) {
            if (serialequipDos.get(k).isUpdated == 0) {
                isUpdated = false
                break
            } else {
                isUpdated = true
            }
        }
        if (isUpdated == true) {
            ll_Equipment.setBackgroundResource(R.drawable.card_white_green_boarder)

        } else {
            ll_Equipment.setBackgroundResource(R.drawable.card_white)

        }
        var trailerDOs = StorageManager.getInstance(this).getTrailerSelectionDO(this)
        if (!TextUtils.isEmpty(routeId)) {
            ll_vehicle_route.setBackgroundResource(R.drawable.card_white_green_boarder)
            tvVR.setText(routeId)
            tvVR.visibility = View.VISIBLE


        } else {
            tvVR.visibility = View.GONE


        }
        if (startRoute == 2) {
            ll_startRoute.setBackgroundResource(R.drawable.card_white_green_boarder)

        }
        if (trailerDOs.size > 0) {
            ll_Trailer.setBackgroundResource(R.drawable.card_white_green_boarder)
            for (i in trailerDOs.indices) {
                if (i == 0) {
                    trailer1 = trailerDOs.get(i).trailer
                }
                if (i == 1) {
                    trailer2 = trailerDOs.get(i).trailer

                }
            }
            tvTrailer.setText(trailer1 + "\n" + trailer2)
            tvTrailer.visibility = View.VISIBLE


        } else {
            tvTrailer.visibility = View.GONE


        }

        if (equipDos.size > 0) {
            ll_Equipment.setBackgroundResource(R.drawable.card_white_green_boarder)
            for (i in equipDos.indices) {
                if (i == 0) {
                    equipment1 = equipDos.get(i).trailer + "\n" + equipDos.get(i).trailerDescription
                }
                if (i == 1) {
                    equipment2 = equipDos.get(i).trailer + "\n" + equipDos.get(i).trailerDescription

                }
            }
            tvEquioment.setText(equipment1 + "\n" + equipment2)
            tvEquioment.visibility = View.VISIBLE


        } else {
            tvEquioment.visibility = View.GONE


        }
        ll_vehicle_route.setOnClickListener {
            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.check_in_process_done), getString(R.string.ok), "", getString(R.string.failure), false)

            } else {
                val intent = Intent(this, VRselectionScreen::class.java)
                startActivity(intent)
            }


        }
        ll_Trailer.setOnClickListener {
            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.check_in_process_done), getString(R.string.ok), "", getString(R.string.failure), false)

            } else {
                val intent = Intent(this, TrailerselectionScreen::class.java)
                startActivityForResult(intent, 11)
            }

        }
        ll_Equipment.setOnClickListener {
            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.check_in_process_done), getString(R.string.ok), "", getString(R.string.failure), false)

            } else {
                val intent = Intent(this, ProductSelectionScreen::class.java)
                startActivityForResult(intent, 12)
            }

        }
        ll_startRoute.setOnClickListener {
            startRouteTimeCapture()
        }
        val read = preferenceUtilss.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
        val unit = preferenceUtilss.getStringFromPreference(PreferenceUtils.ODO_UNIT, "")
        if (loadBay.isNotEmpty()) {

            tvLoad.setText("" + loadBay)
        }
        if (read.isNotEmpty()) {
            ll_odo_meter_read.setBackgroundResource(R.drawable.card_white_green_boarder)
            tvEdit.visibility = View.VISIBLE
            if (read.isNullOrEmpty()) {
                tvEdit.setText("" + odoreading + " " + unit)

            } else {
                tvEdit.setText("" + read + " " + unit)

            }


        } else {
            if (read.isNullOrEmpty()) {
                tvEdit.setText("" + odoreading + " " + unit)
                tvEdit.visibility = View.VISIBLE

            } else {
                tvEdit.visibility = View.GONE

            }


        }
        llLoadBay.setOnClickListener {
            reasonDialog()
        }

        ll_odo_meter_read.setOnClickListener {
            if (AppPrefs.getBoolean(AppPrefs.CHECK_IN_DONE, false)) {
                showAppCompatAlert(getString(R.string.alert), getString(R.string.check_in_process_done), getString(R.string.ok), "", getString(R.string.failure), false)

            } else {
                if (TextUtils.isEmpty(routeId)) {
                    showAppCompatAlert("", getString(R.string.please_select_vr_selection), getString(R.string.ok), "", getString(R.string.failure), false)
                    return@setOnClickListener;
                }
                var customDialog = Dialog(this);
                customDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
                customDialog.setContentView(R.layout.reading_dialog);
                customDialog.show();
                val unit = preferenceUtilss.getStringFromPreference(PreferenceUtils.ODO_UNIT, "")
                customDialog.tvUnit.setText("" + unit)

                var odoReading = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "")
                if (odoReading.isNotEmpty()) {
                    customDialog.etReading.setText(odoReading + "")
                    val pos: Int = customDialog.etReading.getText().length
                    customDialog.etReading.setSelection(pos)
                } else {
                    customDialog.etReading.setText(odoreading.toString())
                    val pos: Int = customDialog.etReading.getText().length
                    customDialog.etReading.setSelection(pos)
                }
                customDialog.button1.setOnClickListener {
                    customDialog.dismiss();
                }
                customDialog.button2.setOnClickListener {
                    customDialog.dismiss();
                    if (customDialog.etReading.text.toString().isNotEmpty()) {
                        var reading = customDialog.etReading.text.toString()
                        if (reading.toDouble() >= odoreading.toDouble()) {
                            preferenceUtils.saveString(PreferenceUtils.OPENING_READING, reading)

                        } else {
                            showToast(getString(R.string.greater_alert))
                        }

                    }
                    onResume()
                }
            }


        }
        val vehicleCheckInDo = StorageManager.getInstance(this@VROdoCheckInActivity).getVehicleCheckInData(this@VROdoCheckInActivity)
        if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
            ll_check_in_odo.setBackgroundResource(R.drawable.card_white_green_boarder)
        } else {
            val flag = preferenceUtilss.getIntFromPreference(PreferenceUtils.PO_FLAG, 0)

            ll_check_in_odo.setOnClickListener {
                if (TextUtils.isEmpty(routeId)) {
                    showAppCompatAlert("", getString(R.string.please_select_vr_selection), getString(R.string.ok), "", getString(R.string.failure), false)
                    return@setOnClickListener;

                } else if (flag == 2 && isUpdated != true) {
                    showAppCompatAlert("", getString(R.string.please_select_equip), getString(R.string.ok), "", getString(R.string.failure), false)
                    return@setOnClickListener;
                } else {
                    val intent = Intent(this, ScheduleNonScheduleActivity::class.java)
                    intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.confirm_stock))
                    startActivity(intent)
                }
//                if (odoreading==null&&read.isEmpty()) {
//                    showAppCompatAlert("", getString(R.string.please_enter_odo_reading), getString(R.string.ok), "", getString(R.string.failure), false)
//                    return@setOnClickListener;
//                }


            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val trailerSelectionDOS = data?.getSerializableExtra("AddedProducts") as ArrayList<TrailerSelectionDO>
            if (requestCode == 11 && resultCode == 11) {
                StorageManager.getInstance(this).saveTrailerSelectionDO(this, trailerSelectionDOS)

            }
            if (requestCode == 12 && resultCode == 12) {
                StorageManager.getInstance(this).saveEquipmentSelectionDO(this, trailerSelectionDOS)

            }
        }


    }

    private fun startRouteTimeCapture() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
        val date = CalendarUtils.getDate()
        val time = CalendarUtils.getTime()
        val num = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

        val siteListRequest = StartRouteService(id, date, time, num, this)
        siteListRequest.setOnResultListener { isError, loginDo ->

            if (isError) {
                Util.showToast(this, getString(R.string.server_error))
            } else {

                if (loginDo != null) {
                    if (loginDo.flag == 20) {
                        ll_startRoute.setBackgroundResource(R.drawable.card_white_green_boarder)
                        preferenceUtils.saveInt(PreferenceUtils.START_ROUTE, 2)

                        Util.showToast(this, getString(R.string.updated_successfully))


                    } else {
                        Util.showToast(this, getString(R.string.server_error))

                    }
                } else {
                    Util.showToast(this, getString(R.string.server_error))


                }
            }

        }

        siteListRequest.execute()
    }

    fun reasonDialog() {
        dialog = Dialog(this, R.style.NewDialog)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.simple_list_dialog)
        val window = dialog.window
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        }
        var recyclerView = dialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        showLoader()
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = LoadBayRequest(this)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null) {
                    if (isError) {
                        showAppCompatAlert("", getString(R.string.server_error), getString(R.string.ok), getString(R.string.cancel), "", false)

                    } else {

                        val siteAdapter = ReasonListAdapter(4, this@VROdoCheckInActivity, unPaidInvoiceMainDO.reasonDOS)
                        recyclerView.adapter = siteAdapter
                    }
                } else {
                    hideLoader()
                    showAppCompatAlert("", getString(R.string.no_reasong_found_at_this_movment), getString(R.string.ok), getString(R.string.cancel), "", false)

                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }

        dialog.show()
    }



}