package com.tbs.generic.vansales.utils;

import org.jetbrains.annotations.Nullable;

/*
 * Created by developer on 22/2/19.
 */
public interface Constants {

    String FB_TABLE_USERS = "table_users";
    String USERTIMINGS = "user_timings";
    String CURRENTTIME = "current_time";
//

//    String CONFIG_COMPANY_ID = "RMC01";
//    String CONFIG_IP_ADDRESS = "10.0.0.244";
//    String CONFIG_PORT_NUMER = "8124";
//    String CONFIG_ALIAS = "PILTMS";
//    String CONFIG_USER_NAME = "TBSADMIN";
//    String CONFIG_PASSWORD = "Tema@789*";

//bpre7
//    String CONFIG_COMPANY_ID = "SAUVE";
//    String CONFIG_IP_ADDRESS = "burbantest-x3.ccsage.gfi.fr";
//    String CONFIG_PORT_NUMER = "433";
//    String CONFIG_ALIAS = "TMS";
//    String CONFIG_USER_NAME = "wserv";
//    String CONFIG_PASSWORD = "d3f3ee5a4aF";
//
    String CONFIG_COMPANY_ID = "FR050";
    String CONFIG_IP_ADDRESS = "125.18.84.155";
    String CONFIG_PORT_NUMER = "8124";
    String CONFIG_ALIAS = "DEMOTMSFR";
    String CONFIG_USER_NAME = "TESTUSER";
    String CONFIG_PASSWORD = "TU@123*";

//    String CONFIG_COMPANY_ID = "MA10";
//    String CONFIG_IP_ADDRESS = "125.18.84.158";
//    String CONFIG_PORT_NUMER = "8124";
//    String CONFIG_ALIAS = "TMSBURBAN";
//    String CONFIG_USER_NAME = "VANSALES";
//    String CONFIG_PASSWORD = "Tb$V@N$@le%";

    String PRODUCT_CODE = "PRODUCT_CODE";
    String INPUT_FLAG = "INPUT_FLAG";
    String LINE_NUMBER = "LINE_NUMBER";
    String RESULT = "RESULT";
    String GRP = "GRP";
    String FLD = "FLD";
    String NAME = "NAME";
    String CONTENT = "content";


    String SCREEN_TYPE = "screen_type";

    String NAMESPACE = "http://www.adonix.com/WSS";
    String METHOD_NAME_RUN = "run";
    String SOAP_ACTION = "CAdxWebServiceXmlCC";
    String URL_GENERIC = "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
    String CONTRACT_NUMBER = "CONTRACT_NUMBER";
    String SERIAL_NUMBER = "SERIAL_NUMBER";
    String PUBLIC_NAME = "publicName";
    String INPUT_XML = "inputXml";
    String CODE_LANG = "codeLang";
    String ENG = "ENG";
    String POOL_ALIAS = "poolAlias";
    String POOL_ID = "poolId";
    String CODE_USER = "codeUser";
    String PASSWORD = "password";
    String REQUEST_CONFIG = "requestConfig";
    String ADXESS = "adxwss.trace.on=off";

    String TAB = "TAB";
    String LIN = "LIN";
    String I_XCONNUM = "I_XCONNUM";
    String I_XNUM = "I_XNUM";
    String I_XLINNUM = "I_XLINNUM";
    String I_XSITE = "I_XSITE";
    String I_XPRD = "I_XPRD";
    String O_XLOTNUM = "O_XLOTNUM";
    String O_XSERNUM = "O_XSERNUM";
    String O_XTOTALC = "O_XTOTALC";
    String I_XSERNUM = "I_XSERNUM";
    String GRP2 = "GRP2";
    String REQUST_SCHEDULE_ID = "I_XSCHVR";
    String REQUST_NON_SCHEDULE_ID = "I_XUNSCHVR";

    String TOTAL_SCHEDULE_STOCK_QTY = "O_XSCHSTK";
    String TOTAL_SCHEDULE_STOCK_VOLUME = "O_XSCHVOL";
    String TOTAL_SCHEDULE_STOCK_UNITS = "O_XSCHSTKUN";

    String TOTAL_UN_SCHEDULE_STOCK_QTY = "O_XUNSCHSTK";
    String TOTAL_UN_SCHEDULE_STOCK_VOLUME = "O_XUNSCHVOL";
    String TOTAL_UN_SCHEDULE_STOCK_UNITS = "O_XUNSCHSTKUN";

    String VEHICEL_CAPACITY = "O_XVANMASS";
    String VEHICLE_CAPACITY_UNITS = "O_XVNMASUN";

    String VEHICLE_VOLUME = "O_XVANVOL";
    String VEHICLE_VOLUME_UNITS = "O_XVNVOLUN";

    String SHIPMENT_NUMBER = "O_XSDHNUM";
    String STOCK_TYPE = "O_XSTKTYP";
    String PRODUCT_NAME = "O_XITM";
    String PRODUCT_DESC = "O_XITMDES";
    String PRODUCT_STOCK_UNIT = "O_XSTU";
    String PRODUCT_WEIGHT_UNIT = "O_XWEU";
    String PRODUCT_VOLUME = "O_XITMVOU";
    String PRODUCT_TOTAL_STOCK = "O_XTOTSTK";
    String PRODUCT_TOTAL_STOCK_VOLUME = "O_XTOTSTKVOL";
    String PRODUCT_QTY = "O_XQTY";
    String DELETE = "delete";
    String EQUIP_CODE = "O_YEQPCODE";
    String EQUIP_DES = "O_YEQPDES";
    String SERIAL = "O_YSERNUM";
    String SERIAL_NUM = "O_XSERNUM";


    String O_XPICKUP_DROP = "O_XPICKUP_DROP";
    String O_XPICKUPSTK = "O_XPICKUPSTK";
    String O_XDROPSTK = "O_XDROPSTK";
}
