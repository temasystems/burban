//
//
//package com.tbs.generic.vansales.here;
//
//import android.Manifest;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.pm.ApplicationInfo;
//import android.content.pm.PackageManager;
//import android.os.Build;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.app.ActivityCompat;
//
//import com.here.android.mpa.common.GeoBoundingBox;
//import com.here.android.mpa.common.GeoCoordinate;
//import com.here.android.mpa.common.GeoPosition;
//import com.here.android.mpa.common.Image;
//import com.here.android.mpa.common.LocationDataSourceHERE;
//import com.here.android.mpa.common.OnEngineInitListener;
//import com.here.android.mpa.common.PositioningManager;
//import com.here.android.mpa.guidance.NavigationManager;
//import com.here.android.mpa.guidance.TrafficUpdater;
//import com.here.android.mpa.guidance.VoiceCatalog;
//import com.here.android.mpa.guidance.VoiceGuidanceOptions;
//import com.here.android.mpa.guidance.VoicePackage;
//import com.here.android.mpa.mapping.AndroidXMapFragment;
//import com.here.android.mpa.mapping.Map;
//import com.here.android.mpa.mapping.MapMarker;
//import com.here.android.mpa.mapping.MapRoute;
//import com.here.android.mpa.mapping.MapState;
//import com.here.android.mpa.routing.CoreRouter;
//import com.here.android.mpa.routing.Maneuver;
//import com.here.android.mpa.routing.Route;
//import com.here.android.mpa.routing.RouteOptions;
//import com.here.android.mpa.routing.RoutePlan;
//import com.here.android.mpa.routing.RouteResult;
//import com.here.android.mpa.routing.RouteTta;
//import com.here.android.mpa.routing.RouteWaypoint;
//import com.here.android.mpa.routing.Router;
//import com.here.android.mpa.routing.RoutingError;
//import com.here.android.positioning.StatusListener;
//import com.tbs.generic.vansales.Activitys.BaseActivity;
//import com.tbs.generic.vansales.R;
//
//import java.io.File;
//import java.io.IOException;
//import java.lang.ref.WeakReference;
//import java.util.List;
//import java.util.Locale;
//
//public class HEREActivity extends BaseActivity implements PositioningManager.OnPositionChangedListener, Map.OnTransformListener {
//    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
//    private static final String[] RUNTIME_PERMISSIONS = {
//            Manifest.permission.ACCESS_FINE_LOCATION,
//            Manifest.permission.WRITE_EXTERNAL_STORAGE,
//            Manifest.permission.INTERNET,
//            Manifest.permission.ACCESS_WIFI_STATE,
//            Manifest.permission.ACCESS_NETWORK_STATE
//    };
//    private TrafficUpdater.RequestInfo m_requestInfo;
//
//    private AndroidXMapFragment m_mapFragment;
//    private Route m_route;
//    private GeoBoundingBox m_geoBoundingBox;
//    MapRoute mapRoute;
//    private Map map;
//    private Button m_naviControlButton;
//    private ImageView ivCar, ivTruck, ivWalk;
//
//    // positioning manager instance
//    private PositioningManager mPositioningManager;
//    private LinearLayout llInstructions,llNavigation,llBottom;
//    private TextView tvTime,tvDTime,tvDDistance;
//    private TextView tvDistance,tvDirection;
//
//    // HERE location data source instance
//    private LocationDataSourceHERE mHereLocation;
//
//    // flag that indicates whether maps is being transformed
//    private boolean mTransforming;
//
//    // callback that is called when transforming ends
//    private Runnable mPendingUpdate;
//    private NavigationManager m_navigationManager;
//    Button btnReCenter;
//
//    // text view instance for showing location information
//    @Override
//    public void initialize() {
//        if (hasPermissions(this, RUNTIME_PERMISSIONS)) {
//            initializeMapsAndPositioning();
//        } else {
//            ActivityCompat.requestPermissions(this, RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS);
//        }
//    }
//
//    @Override
//    public void initializeControls() {
//
//    }
//
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (mPositioningManager != null) {
//            mPositioningManager.stop();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (mPositioningManager != null) {
//            mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK_INDOOR);
//        }
//    }
//
//
//    @Override
//    public void onPositionUpdated(final PositioningManager.LocationMethod locationMethod, final GeoPosition geoPosition, final boolean mapMatched) {
//        final GeoCoordinate coordinate = geoPosition.getCoordinate();
//        if (mTransforming) {
//            mPendingUpdate = new Runnable() {
//                @Override
//                public void run() {
//                    onPositionUpdated(locationMethod, geoPosition, mapMatched);
//                }
//            };
//        } else {
//            map.setCenter(coordinate, Map.Animation.BOW);
//            Constants.Src_Lattitude = geoPosition.getCoordinate().getLatitude();
//            Constants.Src_Longitude = geoPosition.getCoordinate().getLongitude();
//
//            updateLocationInfo(locationMethod, geoPosition);
//
//        }
//    }
//
//    @Override
//    public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {
//        // ignored
//    }
//
//    @Override
//    public void onMapTransformStart() {
//        mTransforming = true;
//    }
//
//    @Override
//    public void onMapTransformEnd(MapState mapState) {
//        mTransforming = false;
//        if (mPendingUpdate != null) {
//            mPendingUpdate.run();
//            mPendingUpdate = null;
//        }
//    }
//
//    private static boolean hasPermissions(Context context, String... permissions) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
//            for (String permission : permissions) {
//                if (ActivityCompat.checkSelfPermission(context, permission)
//                        != PackageManager.PERMISSION_GRANTED) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//                                           @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case REQUEST_CODE_ASK_PERMISSIONS: {
//                for (int index = 0; index < permissions.length; index++) {
//                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
//                        if (!ActivityCompat
//                                .shouldShowRequestPermissionRationale(this, permissions[index])) {
//                            Toast.makeText(this, "Required permission " + permissions[index]
//                                            + " not granted. "
//                                            + "Please go to settings and turn on for sample app",
//                                    Toast.LENGTH_LONG).show();
//                        } else {
//                            Toast.makeText(this, "Required permission " + permissions[index]
//                                    + " not granted", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }
//
//                initializeMapsAndPositioning();
//                break;
//            }
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
//    }
//
//    private AndroidXMapFragment getMapFragment() {
//        return (AndroidXMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
//    }
//
//    private void initializeMapsAndPositioning() {
//        setContentView(R.layout.activity_here_map);
//        initMapFragment();
//        initNaviControlButton();
//
//    }
//
//    private void updateLocationInfo(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition) {
//
//        final StringBuffer sb = new StringBuffer();
//        final GeoCoordinate coord = geoPosition.getCoordinate();
//        sb.append("Type: ").append(String.format(Locale.US, "%s\n", locationMethod.name()));
//        sb.append("Coordinate:").append(String.format(Locale.US, "%.6f, %.6f\n", coord.getLatitude(), coord.getLongitude()));
//        if (coord.getAltitude() != GeoCoordinate.UNKNOWN_ALTITUDE) {
//            sb.append("Altitude:").append(String.format(Locale.US, "%.2fm\n", coord.getAltitude()));
//        }
//        if (geoPosition.getHeading() != GeoPosition.UNKNOWN) {
//            sb.append("Heading:").append(String.format(Locale.US, "%.2f\n", geoPosition.getHeading()));
//        }
//        if (geoPosition.getSpeed() != GeoPosition.UNKNOWN) {
//            sb.append("Speed:").append(String.format(Locale.US, "%.2fm/s\n", geoPosition.getSpeed()));
//        }
//        if (geoPosition.getBuildingName() != null) {
//            sb.append("Building: ").append(geoPosition.getBuildingName());
//            if (geoPosition.getBuildingId() != null) {
//                sb.append(" (").append(geoPosition.getBuildingId()).append(")\n");
//            } else {
//                sb.append("\n");
//            }
//        }
//        if (geoPosition.getFloorId() != null) {
//            sb.append("Floor: ").append(geoPosition.getFloorId()).append("\n");
//        }
//        sb.deleteCharAt(sb.length() - 1);
//            Maneuver maneuver = NavigationManager.getInstance().getNextManeuver();
//        if(maneuver!=null){
//            tvDistance.setText("Next Stop : " +maneuver.getDistanceToNextManeuver()+" m "  + " \nRemaining :  "+maneuver.getCoordinate().distanceTo(geoPosition.getCoordinate()));
//            if((m_route.getTtaIncludingTraffic(0))!=null){
//              int timeInSeconds=m_route.getTtaIncludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
//                tvTime.setText(""+timeInSeconds + "min");
//
//            }
//        }
//
//
//    }
//
//    private void initMapFragment() {
//        m_mapFragment = getMapFragment();
//        m_mapFragment.setRetainInstance(false);
//        String diskCacheRoot = getFilesDir().getPath() + File.separator + ".isolated-here-maps";
//        String intentName = "";
//        try {
//            ApplicationInfo ai = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
//            Bundle bundle = ai.metaData;
//            intentName = bundle.getString("com.tbs.generic.vansales");
//        } catch (PackageManager.NameNotFoundException e) {
//            Log.e(this.getClass().toString(), "Failed to find intent name, NameNotFound: " + e.getMessage());
//        }
//
//        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(diskCacheRoot, intentName);
//        if (!success) {
//
//        } else {
//            if (m_mapFragment != null) {
//                m_mapFragment.init(new OnEngineInitListener() {
//                    @Override
//                    public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
//                        if (error == Error.NONE) {
//
//
//                            map = m_mapFragment.getMap();
//                            m_navigationManager = NavigationManager.getInstance();
//                            map.setCenter(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude), Map.Animation.BOW);
//                            map.setZoomLevel(map.getMaxZoomLevel() - 1);
//                            com.here.android.mpa.common.Image image = new Image();
//
//                            map.addTransformListener(HEREActivity.this);
//                            mPositioningManager = PositioningManager.getInstance();
//                            mHereLocation = LocationDataSourceHERE.getInstance(new StatusListener() {
//                                @Override
//                                public void onOfflineModeChanged(boolean offline) {
//                                    // called when offline mode changes
//                                }
//
//                                @Override
//                                public void onAirplaneModeEnabled() {
//                                    // called when airplane mode is enabled
//                                }
//
//                                @Override
//                                public void onWifiScansDisabled() {
//                                    // called when Wi-Fi scans are disabled
//                                }
//
//                                @Override
//                                public void onBluetoothDisabled() {
//                                    // called when Bluetooth is disabled
//                                }
//
//                                @Override
//                                public void onCellDisabled() {
//                                    // called when Cell radios are switch off
//                                }
//
//                                @Override
//                                public void onGnssLocationDisabled() {
//                                    // called when GPS positioning is disabled
//                                }
//
//                                @Override
//                                public void onNetworkLocationDisabled() {
//                                    // called when network positioning is disabled
//                                }
//
//                                @Override
//                                public void onServiceError(ServiceError serviceError) {
//                                    // called on HERE service error
//                                }
//
//                                @Override
//                                public void onPositioningError(PositioningError positioningError) {
//                                    // called when positioning fails
//                                }
//
//                                @Override
//                                @SuppressWarnings("deprecation")
//                                public void onWifiIndoorPositioningNotAvailable() {
//                                    // called when running on Android 9.0 (Pie) or newer
//                                }
//
//                                @Override
//                                public void onWifiIndoorPositioningDegraded() {
//                                    // called when running on Android 9.0 (Pie) or newer
//                                }
//                            });
//                            if (mHereLocation == null) {
//                                Toast.makeText(HEREActivity.this, "LocationDataSourceHERE.getInstance(): failed, exiting", Toast.LENGTH_LONG).show();
//                                finish();
//                            }
//                            mPositioningManager.setDataSource(mHereLocation);
//                            mPositioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(
//                                    HEREActivity.this));
//                            if (mPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK_INDOOR)) {
//                                m_mapFragment.getPositionIndicator().setVisible(true);
//                                try {
//                                    image.setImageResource(R.drawable.gps_position);
//                                    m_mapFragment.getPositionIndicator().setVisible(true);
//                                    m_mapFragment.getPositionIndicator().setMarker(image);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                Toast.makeText(HEREActivity.this, "PositioningManager.start: failed, exiting", Toast.LENGTH_LONG).show();
//                                finish();
//                            }
//                        } else {
//                            new AlertDialog.Builder(HEREActivity.this).setMessage(
//                                    "Error : " + error.name() + "\n\n" + error.getDetails())
//                                    .setTitle(R.string.engine_init_error)
//                                    .setNegativeButton(android.R.string.cancel,
//                                            new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(
//                                                        DialogInterface dialog,
//                                                        int which) {
//                                                    finish();
//                                                }
//                                            }).create().show();
//                        }
//                    }
//                });
//            }
//        }
//    }
//
//    private void initNaviControlButton() {
//        m_naviControlButton = (Button) findViewById(R.id.naviCtrlButton);
//        ivCar = (ImageView) findViewById(R.id.ivCar);
//        ivTruck = (ImageView) findViewById(R.id.ivTruck);
//        ivWalk = (ImageView) findViewById(R.id.ivWalk);
//        btnReCenter = (Button) findViewById(R.id.btnReCenter);
//        llInstructions = (LinearLayout) findViewById(R.id.llInstructions);
//        tvDTime = (TextView) findViewById(R.id.tvDTime);
//        tvDDistance = (TextView) findViewById(R.id.tvDDistance);
//        tvTime = (TextView) findViewById(R.id.tvTime);
//        tvDistance = (TextView) findViewById(R.id.tvDistance);
//        llNavigation = (LinearLayout) findViewById(R.id.ll1);
//        tvDirection = (TextView) findViewById(R.id.tvDirection);
//        llBottom = (LinearLayout) findViewById(R.id.llBottom);
//
//        btnReCenter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NavigationManager.RoadView roadView = m_navigationManager.getRoadView();
//                roadView.addListener(new WeakReference<NavigationManager.RoadView.Listener>(mNavigationManagerRoadViewListener));
//                roadView.setOrientation(NavigationManager.RoadView.Orientation.DYNAMIC);  //heading is at the top of the screen
//
//            }
//        });
//
//        ivCar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ivCar.setImageDrawable(getResources().getDrawable(R.drawable.car2));
//                ivTruck.setImageDrawable(getResources().getDrawable(R.drawable.truck1));
//                ivWalk.setImageDrawable(getResources().getDrawable(R.drawable.walk1));
//                createRouteCar();
//
//            }
//        });
//        ivTruck.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ivCar.setImageDrawable(getResources().getDrawable(R.drawable.car1));
//                ivTruck.setImageDrawable(getResources().getDrawable(R.drawable.truck2));
//                ivWalk.setImageDrawable(getResources().getDrawable(R.drawable.walk1));
//                createRouteTruck();
//            }
//        });
//
//        m_naviControlButton.setText(R.string.start_navi);
//        m_naviControlButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//
//            public void onClick(View v) {
//                String text = m_naviControlButton.getText().toString();
//                if (text.equalsIgnoreCase(getResources().getString(R.string.start_navi))) {
//                    if (m_route != null) {
//                        startNavigation();
//                    } else {
//                        llNavigation.setVisibility(View.VISIBLE);
//                        llInstructions.setVisibility(View.GONE);
//                        llBottom.setVisibility(View.VISIBLE);
//
//                        Toast.makeText(HEREActivity.this, "please select Travel mode", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    btnReCenter.setVisibility(View.GONE);
//                    llNavigation.setVisibility(View.VISIBLE);
//                    llInstructions.setVisibility(View.GONE);
//                    llBottom.setVisibility(View.VISIBLE);
//
//                    m_naviControlButton.setText(getResources().getString(R.string.start_navi));
//                    m_navigationManager.stop();
//
//                }
//            }
//        });
//    }
//
//
//    private void createRouteCar() {
//        if (map != null && mapRoute != null) {
//            map.removeMapObject(mapRoute);
//            mapRoute = null;
//        }
//        showLoader();
//
//        CoreRouter coreRouter = new CoreRouter();
//        RoutePlan routePlan = new RoutePlan();
//        RouteOptions routeOptions = new RouteOptions();
//        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
////        routeOptions.setHighwaysAllowed(true);
//        routeOptions.setRouteType(RouteOptions.Type.FASTEST);
//        routeOptions.setRouteCount(1);
//        routePlan.setRouteOptions(routeOptions);
//        try {
//
//            com.here.android.mpa.common.Image image2 = new Image();
//            image2.setImageResource(R.drawable.marker);
//
//            MapMarker customMarker2 = new MapMarker(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude, 0.0), image2);
//            map.addMapObject(customMarker2);
//
//            com.here.android.mpa.common.Image image3 = new Image();
//            image3.setImageResource(R.drawable.gps_position);
//            map.getPositionIndicator().setVisible(true);
//            map.getPositionIndicator().setMarker(image3);
//        } catch (Exception e) {
//            Log.e("HERE", e.getMessage());
//        }
//        RouteWaypoint startPoint = new RouteWaypoint(new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude));
//        RouteWaypoint destination = new RouteWaypoint(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude));
//        routePlan.addWaypoint(startPoint);
//        routePlan.addWaypoint(destination);
//        coreRouter.calculateRoute(routePlan,
//                new Router.Listener<List<RouteResult>, RoutingError>() {
//                    @Override
//                    public void onProgress(int i) {
//
//
//                    }
//
//                    @Override
//                    public void onCalculateRouteFinished(List<RouteResult> routeResults,
//                                                         RoutingError routingError) {
//
//                        hideLoader();
//                        if (routingError == RoutingError.NONE) {
//                            if (mapRoute != null) {
//                                map.removeMapObject(mapRoute);
//                            }
//
//                            if (routeResults.get(0).getRoute() != null) {
//                                m_route = routeResults.get(0).getRoute();
//                                mapRoute = new MapRoute(routeResults.get(0).getRoute());
//                                mapRoute.setManeuverNumberVisible(true);
////                                mapRoute.setColor(ContextCompat.getColor(m_activity, R.color.colorAccent));
//                                map.addMapObject(mapRoute);
//                                m_geoBoundingBox = routeResults.get(0).getRoute().getBoundingBox();
//                                map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION);
//                                llBottom.setVisibility(View.VISIBLE);
//                                Maneuver maneuver = NavigationManager.getInstance().getNextManeuver();
//                                if(maneuver!=null){
//                                    tvDDistance.setText(""+m_navigationManager.getDestinationDistance());
//                                    int timeInSeconds = m_route.getTtaExcludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
//                                    tvDTime.setText(""+timeInSeconds + "min");
//                                }
//                            } else {
//                                Toast.makeText(getApplicationContext(), "Error:route results returned is not valid", Toast.LENGTH_LONG).show();
//                            }
//
//                        } else {
//                            if(routingError==RoutingError.NO_START_POINT){
//                                Toast.makeText(getApplicationContext(), "Error: Source Location not found", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.GRAPH_DISCONNECTED){
//                                Toast.makeText(getApplicationContext(), "Error: No route was found", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.NO_END_POINT){
//                                Toast.makeText(getApplicationContext(), "Error: Destination Location not found", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.CANNOT_DO_PEDESTRIAN){
//                                Toast.makeText(getApplicationContext(), "Error: possibly the route is too long", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INSUFFICIENT_MAP_DATA){
//                                Toast.makeText(getApplicationContext(), "Error:The route cannot be calculated because there is not enough local map data to perform route calculation.", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INVALID_CREDENTIALS){
//                                Toast.makeText(getApplicationContext(), "Error: The route cannot be calculated because the HERE Developer credentials are invalid or were not provided. ", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INVALID_OPERATION){
//                                Toast.makeText(getApplicationContext(), "Error: The operation is not allowed at this time because another request is in progress.", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INVALID_PARAMETERS){
//                                Toast.makeText(getApplicationContext(), "Error: Parameters passed were invalid.", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.NETWORK_COMMUNICATION){
//                                Toast.makeText(getApplicationContext(), "Error: The online route calculation request failed because of a networking error.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.NO_CONNECTIVITY){
//                                Toast.makeText(getApplicationContext(), "Error: No internet connection is available.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.OPERATION_NOT_ALLOWED){
//                                Toast.makeText(getApplicationContext(), "Error: Access to this operation is denied.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.OUT_OF_MEMORY){
//                                Toast.makeText(getApplicationContext(), "Error: An out-of-memory error prevented calculation.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.ROUTE_CORRUPTED){
//                                Toast.makeText(getApplicationContext(), "Error: Could not decode the route as received from the server.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else {
//                                Toast.makeText(getApplicationContext(), "Error:route calculation returned error code: " + routingError.toString(), Toast.LENGTH_LONG).show();
//                            }
//
//                        }
//                    }
//                });
//    }
//
//    private void createRouteTruck() {
//        if (map != null && mapRoute != null) {
//            map.removeMapObject(mapRoute);
//            mapRoute = null;
//        }
//        showLoader();
//
//        CoreRouter coreRouter = new CoreRouter();
//        RoutePlan routePlan = new RoutePlan();
//
//        RouteOptions routeOptions = new RouteOptions();
//        routeOptions.setTransportMode(RouteOptions.TransportMode.TRUCK);
//        routeOptions.setRouteType(RouteOptions.Type.FASTEST);
//        //shortest and Balance wont support for Truck
//        /* Calculate 1 route. */
//        routeOptions.setTruckTunnelCategory(RouteOptions.TunnelCategory.E)
//                .setTruckLength(135.25f)
//                .setTruckHeight(15.6f)
//                .setTruckTrailersCount(1);
//        routePlan.setRouteOptions(routeOptions);
//
//        try {
////            com.here.android.mpa.common.Image image = new Image();
////            image.setImageResource(R.drawable.marker2);
//            com.here.android.mpa.common.Image image2 = new Image();
//            image2.setImageResource(R.drawable.marker);
////            MapMarker customMarker = new MapMarker(new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude, 0.0), image);
////            map.addMapObject(customMarker);
//
//            MapMarker customMarker2 = new MapMarker(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude, 0.0), image2);
//            map.addMapObject(customMarker2);
//
//            com.here.android.mpa.common.Image image3 = new Image();
//            image3.setImageResource(R.drawable.gps_position);
//            map.getPositionIndicator().setVisible(true);
//            map.getPositionIndicator().setMarker(image3);
//        } catch (Exception e) {
//            Log.e("HERE", e.getMessage());
//        }
//        RouteWaypoint startPoint = new RouteWaypoint(new GeoCoordinate(Constants.Src_Lattitude, Constants.Src_Longitude));
//        RouteWaypoint destination = new RouteWaypoint(new GeoCoordinate(Constants.Des_Lattitude, Constants.Des_Longitude));
//        routePlan.addWaypoint(startPoint);
//        routePlan.addWaypoint(destination);
//        coreRouter.calculateRoute(routePlan,
//                new Router.Listener<List<RouteResult>, RoutingError>() {
//                    @Override
//                    public void onProgress(int i) {
//                    }
//
//                    @Override
//                    public void onCalculateRouteFinished(List<RouteResult> routeResults,
//                                                         RoutingError routingError) {
//                        hideLoader();
//                        if (routingError == RoutingError.NONE) {
//                            if (mapRoute != null) {
//                                map.removeMapObject(mapRoute);
//                            }
//                            if (routeResults.get(0).getRoute() != null) {
//                                m_route = routeResults.get(0).getRoute();
//                                mapRoute = new MapRoute(routeResults.get(0).getRoute());
//                                mapRoute.setManeuverNumberVisible(true);
//                                map.addMapObject(mapRoute);
//                                m_geoBoundingBox = routeResults.get(0).getRoute().getBoundingBox();
//                                map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION);
//                                llBottom.setVisibility(View.VISIBLE);
//                                Maneuver maneuver = NavigationManager.getInstance().getNextManeuver();
//                                if(maneuver!=null){
//                                    tvDDistance.setText(""+m_navigationManager.getDestinationDistance());
//                                    int timeInSeconds = m_route.getTtaExcludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
//                                    tvDTime.setText(""+timeInSeconds + "min");
//                                }
//                            } else {
//                                Toast.makeText(getApplicationContext(), "Error:route results returned is not valid", Toast.LENGTH_LONG).show();
//                            }
//                        }  else {
//                            if(routingError==RoutingError.NO_START_POINT){
//                                Toast.makeText(getApplicationContext(), "Error: Source Location not found", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.GRAPH_DISCONNECTED){
//                                Toast.makeText(getApplicationContext(), "Error: No route was found", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.NO_END_POINT){
//                                Toast.makeText(getApplicationContext(), "Error: Destination Location not found", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.CANNOT_DO_PEDESTRIAN){
//                                Toast.makeText(getApplicationContext(), "Error: possibly the route is too long", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INSUFFICIENT_MAP_DATA){
//                                Toast.makeText(getApplicationContext(), "Error:The route cannot be calculated because there is not enough local map data to perform route calculation.", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INVALID_CREDENTIALS){
//                                Toast.makeText(getApplicationContext(), "Error: The route cannot be calculated because the HERE Developer credentials are invalid or were not provided. ", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INVALID_OPERATION){
//                                Toast.makeText(getApplicationContext(), "Error: The operation is not allowed at this time because another request is in progress.", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.INVALID_PARAMETERS){
//                                Toast.makeText(getApplicationContext(), "Error: Parameters passed were invalid.", Toast.LENGTH_LONG).show();
//
//                            }else if(routingError==RoutingError.NETWORK_COMMUNICATION){
//                                Toast.makeText(getApplicationContext(), "Error: The online route calculation request failed because of a networking error.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.NO_CONNECTIVITY){
//                                Toast.makeText(getApplicationContext(), "Error: No internet connection is available.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.OPERATION_NOT_ALLOWED){
//                                Toast.makeText(getApplicationContext(), "Error: Access to this operation is denied.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.OUT_OF_MEMORY){
//                                Toast.makeText(getApplicationContext(), "Error: An out-of-memory error prevented calculation.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else if(routingError==RoutingError.ROUTE_CORRUPTED){
//                                Toast.makeText(getApplicationContext(), "Error: Could not decode the route as received from the server.", Toast.LENGTH_LONG).show();
//
//                            }
//                            else {
//                                Toast.makeText(getApplicationContext(), "Error:route calculation returned error code: " + routingError.toString(), Toast.LENGTH_LONG).show();
//                            }
//
//                        }
//                    }
//                });
//    }
//
//    private void startNavigation() {
//        m_naviControlButton.setText(getResources().getString(R.string.stop_navi));
//        m_navigationManager.setMap(map);
//        map.setMapScheme(Map.Scheme.NORMAL_DAY);
////        llNavigation.setVisibility(View.GONE);
//        btnReCenter.setVisibility(View.GONE);
//        llBottom.setVisibility(View.VISIBLE);
//
//        llInstructions.setVisibility(View.VISIBLE);
//
//        m_navigationManager.startNavigation(m_route);
//
//
//        map.setTilt(60);
////        startForegroundService();
//        m_mapFragment.getPositionIndicator().setVisible(true);
//        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.POSITION_ANIMATION);
//        m_navigationManager.addNavigationManagerEventListener(new WeakReference<NavigationManager.NavigationManagerEventListener>(new NavigationManager.NavigationManagerEventListener() {
//
//            @Override
//            public void onRunningStateChanged() {
////                Toast.makeText(getApplicationContext(), "Running state changed", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onNavigationModeChanged() {
////                Toast.makeText(getApplicationContext(), "Navigation mode changed", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onEnded(NavigationManager.NavigationMode navigationMode) {
//                Toast.makeText(getApplicationContext(), "Reached destination", Toast.LENGTH_SHORT).show();
//                m_navigationManager.stop();
//            }
//
//            @Override
//            public void onMapUpdateModeChanged(NavigationManager.MapUpdateMode mapUpdateMode) {
////                Toast.makeText(getApplicationContext(), "Map update mode is changed to " + mapUpdateMode,
////                        Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onRouteUpdated(Route route) {
////                Toast.makeText(getApplicationContext(), "Route updated", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onCountryInfo(String s, String s1) {
////                Toast.makeText(getApplicationContext(), "Country info updated from " + s + " to " + s1,
////                        Toast.LENGTH_SHORT).show();
//            }
//        }));
//
//        Maneuver maneuver = NavigationManager.getInstance().getNextManeuver();
//        if(maneuver!=null){
//            tvDDistance.setText(""+m_navigationManager.getDestinationDistance());
//            int timeInSeconds = m_route.getTtaExcludingTraffic(Route.WHOLE_ROUTE).getDuration()/60;
//            tvDTime.setText(""+timeInSeconds + "min");
//        }
//        m_navigationManager.addNewInstructionEventListener(new WeakReference<NavigationManager.NewInstructionEventListener>(new NavigationManager.NewInstructionEventListener() {
//            @Override
//            public void onNewInstructionEvent() {
//                Maneuver maneuver = m_navigationManager.getNextManeuver();
//                if (maneuver != null) {
//                    if (maneuver.getAction() == Maneuver.Action.END) {
////                        Toast.makeText(HEREActivity.this, "Reached to destination", Toast.LENGTH_LONG).show();
//                    }
//                    tvDirection.setText(""+maneuver.getTurn());
//
////                    Toast.makeText(HEREActivity.this, "getTurn : "+maneuver.getTurn(), Toast.LENGTH_LONG).show();
////                    Toast.makeText(HEREActivity.this, "getIcon : "+maneuver.getIcon(), Toast.LENGTH_LONG).show();
//
//                    // display current or next road information
//                    // display maneuver.getDistanceToNextManeuver()
//                }
//            }
//        }));
//
//        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.NONE);
//        calculateTtaUsingDownloadedTraffic();
//
////        m_navigationManager.getEta();
//
//        //set up road view in navigation
//
//        NavigationManager.RoadView roadView = m_navigationManager.getRoadView();
//
////        roadView.addListener(new WeakReference<NavigationManager.RoadView.Listener>(mNavigationManagerRoadViewListener));
////        roadView.setOrientation(NavigationManager.RoadView.Orientation.DYNAMIC);  //heading is at the top of the screen
//
//        //set up route recalculation in navigation
//        m_navigationManager.addRerouteListener(new WeakReference<NavigationManager.RerouteListener>(mNavigaionRerouteListener));
////        m_navigationManager.addRerouteListener(new WeakReference<NavigationManager.RerouteListener>(new NavigationManager.RerouteListener() {
////            @Override
////            public void onRerouteBegin() {
////                Toast.makeText(HEREActivity.this, "Re route ......", Toast.LENGTH_LONG).show();
////            }
////        }));
//    }
//    private NavigationManager.RoadView.Listener mNavigationManagerRoadViewListener = new NavigationManager.RoadView.Listener() {
//        @Override
//        public void onPositionChanged(GeoCoordinate geoCoordinate) {
//
//        }
//    };
//    private NavigationManager.RerouteListener mNavigaionRerouteListener = new NavigationManager.RerouteListener() {
//        @Override
//        public void onRerouteBegin() {
//            Toast.makeText(getApplicationContext(), "reroute begin", Toast.LENGTH_SHORT).show();
//        }
//
//
//    };
//
//
//    private void calculateTtaUsingDownloadedTraffic() {
//        /* Turn on traffic updates */
//        TrafficUpdater.getInstance().enableUpdate(true);
//
//
//        m_requestInfo = TrafficUpdater.getInstance().request(
//                m_route, new TrafficUpdater.Listener() {
//                    @Override
//                    public void onStatusChanged(TrafficUpdater.RequestState requestState) {
//                        final RouteTta ttaDownloaded = m_route.getTtaUsingDownloadedTraffic(
//                                Route.WHOLE_ROUTE);
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                if (tvTime != null) {
//                                    tvTime.setText("" +
//                                            String.valueOf(ttaDownloaded.getDuration()/60));
//                                }
//                            }
//                        });
//                    }
//                });
//    }
//}
