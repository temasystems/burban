package com.tbs.generic.vansales.utils;

import android.content.Context;
import android.util.Log;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.common.FireBaseOperations;
import com.tbs.generic.vansales.listeners.StringListner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarUtils {

    private static Context mcontext;

    private static final String DATE_STD_PATTERN_PRINT = "dd-MM-yyyy";//2018-01-06
    private static final String DATE = "yyyyMMdd";//2018-01-06
    private static final String TIME = "HHmm";
    private static final String PDF_DATE = "dd/MM/yyyy";//2018-01-06
    public static String PDF_TIME = "HH:mm:ss";

    public CalendarUtils(Context context) {
        this.mcontext = context;
    }
    public static String getFormatedDate(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            Date date1 = simpleDateFormat.parse(date);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
            if (date1 != null) {
                return simpleDateFormat1.format(date1);
            } else {
                return date;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }
    public static String getStringOfHexadecilmal(String s) {
        return s.replace("&#x0a;", " ");
    }

    public static void getCurrentDateFromFireBase(Context context,
                                                  StringListner dateString) {

        if (Util.isNetworkAvailable(context)) {
            FireBaseOperations.setCurrentUserTime();

            String formattedDate = "";
            FireBaseOperations.getUserTime((object, isSuccess) -> {
                if (isSuccess) {
                    Log.d("userTimings0--->", object + "");
                    SimpleDateFormat df = new SimpleDateFormat(((BaseActivity) mcontext).preferenceUtils.getStringFromPreference(PreferenceUtils.DATE_FORMAT, ""));
                    DateFormat d = new SimpleDateFormat("HH:mm");
                    Date date = (Date) object;
                    String time = d.format(date);
                    dateString.getString(df.format(date) + " " + time);
                } else {
                    dateString.getString(getCurrentDate(mcontext));
                }
            });
        } else {
            dateString.getString(getCurrentDate(mcontext));
        }
    }

    public static String getCurrentDate(Context context) {

        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            PreferenceUtils preferenceUtils = new PreferenceUtils(context);
            String tf = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_TIME_FORMAT, "");
            String ddf = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DATE_FORMAT, "");
            SimpleDateFormat df = new SimpleDateFormat(ddf);
//            formattedDate = df.format(calendar.getTime()+" "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));
            if (tf.equalsIgnoreCase("12")) {
                DateFormat d = new SimpleDateFormat("hh:mm aa");
                String date = d.format(Calendar.getInstance().getTime());


                formattedDate = df.format(c.getTime()) + "   " + date;
            } else {
                DateFormat d = new SimpleDateFormat("HH:mm");
                String date = d.format(Calendar.getInstance().getTime());


                formattedDate = df.format(c.getTime()) + "   " + date;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String getTime() {


        return getTimeBasedOnPattern(TIME);
    }

    public static String getDate() {

        return getDateBasedOnPattern(DATE);
    }

    public static String getPDFDate(Context context) {
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        String ddf = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DATE_FORMAT, "");
        return getDateBasedOnPattern(ddf);
    }

    public static String getPDFTime(Context context) {
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        String tf = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_TIME_FORMAT, "");
        String ddf = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DATE_FORMAT, "");
        if (tf.equalsIgnoreCase("12")) {
            PDF_TIME = "hh:mm:ss aa";
        } else {
            PDF_TIME = "HH:mm:ss";

        }
        return getDateBasedOnPattern(PDF_TIME);
    }

    private static String getDateBasedOnPattern(String formate) {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(formate);
            formattedDate = df.format(c.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    private static String getTimeBasedOnPattern(String formateTime) {
        String formattedDate = "";
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat(DATE_STD_PATTERN_PRINT);
//            formattedDate = df.format(calendar.getTime()+" "+calendar.get(Calendar.HOUR_OF_DAY)+":"+calendar.get(Calendar.MINUTE));

            DateFormat d = new SimpleDateFormat(formateTime);
            String date = d.format(Calendar.getInstance().getTime());
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            formattedDate = date;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
    public static String revisedTimeDifference(String revDate,String revTime,String date, String time) {
        String diffTime = "";
        SimpleDateFormat smf = new SimpleDateFormat("yyyyMMddHHmm");
        long minuts = 0;

        try {
            Date finaldepDate = smf.parse(date +time);
            Date finalCheckDate = smf.parse(revDate +revTime);

            long date1 = finaldepDate.getTime();
            long date2 = finalCheckDate.getTime();
            long diff;
            if (date1 > date2) {

                diff = date1 - date2;
                long seconds = diff / 1000;
                minuts = seconds / 60;
                SimpleDateFormat sdf = new SimpleDateFormat("mm");

                try {
                    Date dt = sdf.parse(String.valueOf(minuts));
                    sdf = new SimpleDateFormat("HH:mm");
                    System.out.println(sdf.format(dt));
                    diffTime="Delay  by "+sdf.format(dt);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
//                String hr= String.valueOf(hours);
//                if(hr.length()<2){
//                    if(min<2){
//                        diffTime="Delay  by 0"+hours+":0"+min;
//
//                    }else {
//                        diffTime="Delay  by 0"+hours+":"+min;
//
//                    }
//
//                }else {
//                    if(min<2){
//                        diffTime="Delay  by "+hours+":0"+min;
//
//                    }else {
//                        diffTime="Delay  by "+hours+":"+min;
//
//                    }
//                }
            } else {
                diff = date2 - date1;
                long seconds = diff / 1000;
                minuts = seconds / 60;
                SimpleDateFormat sdf = new SimpleDateFormat("mm");

                try {
                    Date dt = sdf.parse(String.valueOf(minuts));
                    sdf = new SimpleDateFormat("HH:mm");
                    System.out.println(sdf.format(dt));
                    diffTime="Arriving Early  by "+sdf.format(dt);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffTime;
    }
    public static String depRevisedTimeDifference(String revDate,String revTime,String date, String time) {
        String diffTime = "";
        SimpleDateFormat smf = new SimpleDateFormat("yyyyMMddHHmm");
        long minuts = 0;

        try {
            Date finaldepDate = smf.parse(date +time);
            Date finalCheckDate = smf.parse(revDate +revTime);

            long date1 = finaldepDate.getTime();
            long date2 = finalCheckDate.getTime();
            long diff;
            if (date1 > date2) {

                diff = date1 - date2;
                long seconds = diff / 1000;
                minuts = seconds / 60;
                long hours = minuts / 60;
                long days = hours / 24;
                String hr= String.valueOf(hours);
                if(hr.length()<2){
                    if(minuts<2){
                        diffTime="0"+hours+":0"+minuts;

                    }else {
                        diffTime="0"+hours+":"+minuts;

                    }

                }else {
                    if(minuts<2){
                        diffTime=""+hours+":0"+minuts;

                    }else {
                        diffTime=""+hours+":"+minuts;

                    }
                }
            } else {
                diff = date2 - date1;
                long seconds = diff / 1000;
                minuts = seconds / 60;
                long hours = minuts / 60;
                long days = hours / 24;
                String hr= String.valueOf(hours);
                if(hr.length()<2){
                    if(minuts<2){
                        diffTime=""+hours+":0"+minuts;

                    }else {
                        diffTime="0"+hours+":"+minuts;

                    }

                }else {
                    if(minuts<2){
                        diffTime=""+hours+":0"+minuts;

                    }else {
                        diffTime=""+hours+":"+minuts;

                    }
                }

            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffTime;
    }

    public static String timeDifference(String date, String time,Context context) {
        String diffTime = "";
        SimpleDateFormat smf = new SimpleDateFormat("yyyyMMddHHmm");
        long minuts = 0;

        try {
            Date finaldepDate = smf.parse(date + "" + time);
            Date finalCheckDate = smf.parse(getDate()+getTime());

            long date1 = finaldepDate.getTime();
            long date2 = finalCheckDate.getTime();
            long diff;
            if (date1 > date2) {

                diff = date1 - date2;
            } else {
                diff = date2 - date1;
            }
            long seconds = diff / 1000;
            minuts = seconds / 60;
            long hours = minuts / 60;
            long days =  hours / 24;
            String hr= String.valueOf(hours);
            String mn= String.valueOf(minuts);
            if(hr.length()<2){
                if(mn.length()<2){
                    diffTime="0"+hours+":0"+minuts;

                }else {
                    diffTime="0"+hours+":"+minuts;

                }

            }else {
                if(mn.length()<2){
                    diffTime=""+hours+":0"+minuts;

                }else {
                    diffTime=""+hours+":"+minuts;

                }

            }
            Log.d("Seconds-->", seconds + "-->minutes" + minuts + "-->hours->" + hours + "-->days->" + days);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffTime;
    }
}



