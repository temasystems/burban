package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.RecyclerView
import com.alert.rajdialogs.ProgressDialog
import com.tbs.generic.pod.Activitys.StartDayActivity
import com.tbs.generic.vansales.Adapters.EquipProdSelectionAdapter
import com.tbs.generic.vansales.Adapters.ProductSelectionAdapter
import com.tbs.generic.vansales.Adapters.TrailerSelectionAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.TrailerSelectionDO
import com.tbs.generic.vansales.Model.TrailerSelectionMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ProductEquipmentRequest
import com.tbs.generic.vansales.Requests.TrailerSelectionRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.prod_eqip_data.*
import kotlinx.android.synthetic.main.product_selection_screen.*
import kotlinx.android.synthetic.main.trailer_selection_screen.*
import kotlinx.android.synthetic.main.vr_selection_screen.tvNoData
import java.util.*


class ProductSelectionScreen : BaseActivity() {
    lateinit var adapter: ProductSelectionAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: RecyclerView
    private var progressDialog: ProgressDialog? = null
    var selectDOS: ArrayList<TrailerSelectionDO>? = null
    lateinit var productDO: TrailerSelectionMainDO

    override fun onResume() {
        super.onResume()
        initializeControls()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.product_selection_screen, null) as NestedScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun initializeControls() {
        tv_title.text = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "") + "\n" +
                preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

        recycleview = findViewById<RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview.layoutManager = linearLayoutManager
        VR()
//        btnSelect.setOnClickListener {
//            val intent = Intent(this@ProductSelectionScreen, SerializationActivity::class.java)
//            intent.putExtra("PRODUCTDO",productDO );
//
//            startActivity(intent)
//        }
    }

    fun VR() {
        if (Util.isNetworkAvailable(this)) {
            progressDialog?.show()
            val driverListRequest = ProductEquipmentRequest(this)
            driverListRequest.setOnResultListener { isError, modelDO ->
                //hideLoader()
                if (isError) {
                    progressDialog?.hide()

                    showAlert(getString(R.string.server_error))
                } else {
                    progressDialog?.hide()


                    selectDOS = modelDO.trailerSelectionDOS
                    if (selectDOS != null && selectDOS!!.size > 0) {
                        productDO = modelDO
                        adapter = ProductSelectionAdapter(this@ProductSelectionScreen, selectDOS)
                        recycleview.adapter = adapter
                        tvNoData.visibility = View.GONE
                        tvSerialText.visibility = View.VISIBLE
                        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
                        recycleviewEquip.layoutManager = linearLayoutManager

                        if (productDO.equipmentDOS.size > 0) {
                            var serialequipDos = StorageManager.getInstance(this).getSerialEquipmentSelectionDO(this)
                                if(serialequipDos==null||serialequipDos.isEmpty()){
                                    StorageManager.getInstance(this).saveSerialEquipmentSelectionDO(this,  productDO.equipmentDOS)

                                }

                            var adapter = EquipProdSelectionAdapter(this@ProductSelectionScreen, productDO.equipmentDOS)
                            recycleviewEquip.adapter = adapter
                        }

//                        llEquipment.setBackgroundDrawable(getResources().getDrawable(R.drawable.edit_text_background_lg));
//                        tvQty.setText(""+modelDO.totalqty)
//                        tvEquioment.setText(modelDO.item + " - "+modelDO.itemDes)

                    } else {

                        showToast(getString(R.string.no_data_found))
                        tvNoData.visibility = View.VISIBLE
                    }

                }
            }
            driverListRequest.execute()
        } else {
            YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.internet_connection), ResultListner { `object`, isSuccess -> }, false).show((this as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
        }
    }
}