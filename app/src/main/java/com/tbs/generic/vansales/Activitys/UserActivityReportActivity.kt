package com.tbs.generic.vansales.Activitys

import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.*
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.UserActivityReportRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.pdfs.ReceiptPDF
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.user_aactivity_report.*
import kotlinx.android.synthetic.main.user_aactivity_report.view.*
import java.util.*
import kotlin.collections.ArrayList


class UserActivityReportActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var sdRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var siRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var cashRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var chequeRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var productsRecycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var stockRecycleview: androidx.recyclerview.widget.RecyclerView

    lateinit var btnPrint: Button

    private var userActivityReportMainDO: UserActivityReportMainDO = UserActivityReportMainDO()

    lateinit var tvNoDataFound: TextView
    var fromId = 0
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""
    lateinit var tvCashTotalAmount: TextView
    lateinit var tvSdTotalAmount: TextView
    lateinit var tvTotalSIAmount: TextView
    lateinit var tvTotalChequeAmount: TextView
    lateinit var tvTotalProductQuantity: TextView
    lateinit var tvTotalStockQuantity: TextView

    lateinit var tvSpotDeliveries: TextView
    lateinit var tvSalesInvoices: TextView
    lateinit var tvCashReciepts: TextView
    lateinit var tvChequeReciepts: TextView
    lateinit var tvProducts: TextView
    lateinit var tvStock: TextView

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var tvTransactionDate: TextView
    lateinit var tvUserId: TextView
    lateinit var tvUserName: TextView
    lateinit var llSpotDeliveryItem: LinearLayout
    lateinit var llSalesInvoicesItem: LinearLayout
    lateinit var llCashItem: LinearLayout
    lateinit var llChequeItem: LinearLayout
    lateinit var llProducts: LinearLayout
    lateinit var llStock: LinearLayout

    lateinit var llSdQuantity: LinearLayout
    lateinit var llSIAmount: LinearLayout
    lateinit var llProductQuantity: LinearLayout
    lateinit var llStockQuantity: LinearLayout

    lateinit var llCashAmount: LinearLayout
    lateinit var llChequeAmount: LinearLayout
    lateinit var llUserId: LinearLayout
    lateinit var llName: LinearLayout
    lateinit var view1: View
    lateinit var view2: View
    lateinit var view3: View
    lateinit var view4: View
    lateinit var view5: View
    lateinit var view6: View
    lateinit var view7: View
    lateinit var view8: View
    lateinit var view9: View
    lateinit var view10: View
    lateinit var view11: View
    lateinit var view12: View
    lateinit var view13: View
    lateinit var view14: View
    lateinit var view15: View
    lateinit var view16: View

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.user_aactivity_report, null) as View
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        val svCode = preferenceUtils.getStringFromPreference(PreferenceUtils.CV_PLATE, "")
        val nvCode = preferenceUtils.getStringFromPreference(PreferenceUtils.NON_VEHICLE_CODE, "")
        tvScreenTitle.maxLines = 2
        tvScreenTitle.isSingleLine = false
        if (svCode.isNotEmpty()) {
            tvScreenTitle.text = name + " \n Vehicle Number - " + svCode
        } else {
            tvScreenTitle.text = name + " \n Vehicle Number - " + nvCode
        }
        sdRecycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.sdRecycleview)
        siRecycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.siRecycleview)
        cashRecycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.cashRecycleview)
        chequeRecycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.chequeRecycleview)
        productsRecycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.productsRecycleview)
        stockRecycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.stockRecycleview)

        view1 = findViewById<View>(R.id.view1)
        view2 = findViewById<View>(R.id.view2)
        view3 = findViewById<View>(R.id.view3)
        view4 = findViewById<View>(R.id.view4)
        view5 = findViewById<View>(R.id.view5)
        view6 = findViewById<View>(R.id.view6)
        view7 = findViewById<View>(R.id.view7)
        view8 = findViewById<View>(R.id.view8)
        view9 = findViewById<View>(R.id.view9)
        view10 = findViewById<View>(R.id.view10)
        view11 = findViewById<View>(R.id.view11)
        view12 = findViewById<View>(R.id.view12)
        view13 = findViewById<View>(R.id.view13)
        view14 = findViewById<View>(R.id.view14)
        view15 = findViewById<View>(R.id.view15)
        view16 = findViewById<View>(R.id.view16)

        tvNoDataFound = findViewById<TextView>(R.id.tvNoData)
        tvTransactionDate = findViewById<TextView>(R.id.tvTransactionDate)
        tvSdTotalAmount = findViewById<TextView>(R.id.tvSdTotalAmount)
        tvTotalSIAmount = findViewById<TextView>(R.id.tvTotalSIAmount)
        tvCashTotalAmount = findViewById<TextView>(R.id.tvCashTotalAmount)
        tvTotalChequeAmount = findViewById<TextView>(R.id.tvTotalChequeAmount)
        tvTotalProductQuantity = findViewById<TextView>(R.id.tvTotalProductQuantity)
        tvTotalStockQuantity = findViewById<TextView>(R.id.tvTotalStockQuantity)

        tvUserId = findViewById<TextView>(R.id.tvUserId)
        tvUserName = findViewById<TextView>(R.id.tvUserName)
        tvSpotDeliveries = findViewById<TextView>(R.id.tvSpotDeliveries)
        tvSalesInvoices = findViewById<TextView>(R.id.tvSalesInvoices)
        tvCashReciepts = findViewById<TextView>(R.id.tvCashReciepts)
        tvChequeReciepts = findViewById<TextView>(R.id.tvChequeReciepts)
        tvProducts = findViewById<TextView>(R.id.tvProducts)
        tvStock = findViewById<TextView>(R.id.tvStock)


        tvUserId.text = id
        tvUserName.text = name
        llUserId = findViewById<LinearLayout>(R.id.llUserId)
        llName = findViewById<LinearLayout>(R.id.llName)

        llSpotDeliveryItem = findViewById<LinearLayout>(R.id.llSpotDeliveryItem)
        llSalesInvoicesItem = findViewById<LinearLayout>(R.id.llSalesInvoicesItem)
        llCashItem = findViewById<LinearLayout>(R.id.llCashItem)
        llChequeItem = findViewById<LinearLayout>(R.id.llChequeItem)
        llProducts = findViewById<LinearLayout>(R.id.llProducts)
        llStock = findViewById<LinearLayout>(R.id.llStock)

        llSdQuantity = findViewById<LinearLayout>(R.id.llSdQuantity)
        llSIAmount = findViewById<LinearLayout>(R.id.llSIAmount)
        llCashAmount = findViewById<LinearLayout>(R.id.llCashAmount)
        llChequeAmount = findViewById<LinearLayout>(R.id.llChequeAmount)
        llProductQuantity = findViewById<LinearLayout>(R.id.llProductQuantity)
        llStockQuantity = findViewById<LinearLayout>(R.id.llStockQuantity)

        sdRecycleview.isNestedScrollingEnabled = false
        siRecycleview.isNestedScrollingEnabled = false
        cashRecycleview.isNestedScrollingEnabled = false
        chequeRecycleview.isNestedScrollingEnabled = false
        productsRecycleview.isNestedScrollingEnabled = false
        stockRecycleview.isNestedScrollingEnabled = false
        var from = CalendarUtils.getDate()

        val aMonth = from.substring(4, 6)
        val ayear = from.substring(0, 4)
        val aDate = from.substring(Math.max(from.length - 2, 0))


        tvTransactionDate.text = "" + aDate + " - " + aMonth + " - " + ayear
        startDate = ayear + aMonth + aDate
        endDate = ayear + aMonth + aDate
        var linearLayoutManager1 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager2 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager3 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager4 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager5 = androidx.recyclerview.widget.LinearLayoutManager(this)
        var linearLayoutManager6 = androidx.recyclerview.widget.LinearLayoutManager(this)

        sdRecycleview.layoutManager = linearLayoutManager1
        siRecycleview.layoutManager = linearLayoutManager2
        cashRecycleview.layoutManager = linearLayoutManager3
        chequeRecycleview.layoutManager = linearLayoutManager4
        productsRecycleview.layoutManager = linearLayoutManager5
        stockRecycleview.layoutManager = linearLayoutManager6

        btnPrint = findViewById<Button>(R.id.btnPrint)
        val btnSearch = findViewById<Button>(R.id.btnSearch)

        btnSearch.setOnClickListener {
            Util.preventTwoClick(it)
            userActivityReportMainDO.userDeliveryDOS.clear()
            userActivityReportMainDO.salesInvoiceDOS.clear()
            userActivityReportMainDO.cashDOS.clear()
            userActivityReportMainDO.chequeDOS.clear()

            var frm = tvTransactionDate.text.toString().trim()
            if (frm.isNotEmpty()) {
                selectActivityList()
            } else {
                showToast(getString(R.string.please_select_from_to_dates))
            }

        }
        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvTransactionDate.setOnClickListener {
            val datePicker = DatePickerDialog(this@UserActivityReportActivity, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }



        btnPrint.setOnClickListener {

//            prepareActivityReport()
        }

    }

    private fun selectActivityList() {


        if (Util.isNetworkAvailable(this)) {
            var userActivityReportActivity = UserActivityReportRequest(startDate, this@UserActivityReportActivity)

            userActivityReportActivity.setOnResultListener { isError, userActivityMainDO ->
                hideLoader()

                if (isError) {
                    tvSpotDeliveries.visibility = View.GONE
                    llSpotDeliveryItem.visibility = View.GONE
                    sdRecycleview.visibility = View.GONE
                    llSdQuantity.visibility = View.GONE
                    view1.visibility = View.GONE
                    view2.visibility = View.GONE
                    tvSalesInvoices.visibility = View.GONE
                    llSalesInvoicesItem.visibility = View.GONE
                    siRecycleview.visibility = View.GONE
                    llSIAmount.visibility = View.GONE
                    view3.visibility = View.GONE
                    view4.visibility = View.GONE
                    tvCashReciepts.visibility = View.GONE
                    llCashItem.visibility = View.GONE
                    cashRecycleview.visibility = View.GONE
                    llCashAmount.visibility = View.GONE
                    view5.visibility = View.GONE
                    view6.visibility = View.GONE
                    tvChequeReciepts.visibility = View.GONE
                    llChequeAmount.visibility = View.GONE
                    llChequeItem.visibility = View.GONE
                    chequeRecycleview.visibility = View.GONE
                    view7.visibility = View.GONE
                    view8.visibility = View.GONE
                    view9.visibility = View.GONE
                    view10.visibility = View.GONE

                    tvProducts.visibility = View.GONE
                    llProductQuantity.visibility = View.GONE
                    llProducts.visibility = View.GONE
                    productsRecycleview.visibility = View.GONE
                    view11.visibility = View.GONE
                    view12.visibility = View.GONE
                    view13.visibility = View.GONE

                    tvStock.visibility = View.GONE
                    llStockQuantity.visibility = View.GONE
                    llStock.visibility = View.GONE
                    stockRecycleview.visibility = View.GONE
                    view14.visibility = View.GONE
                    view15.visibility = View.GONE
                    view16.visibility = View.GONE

                    btnPrint.visibility = View.GONE
                    tvNoDataFound.visibility = View.VISIBLE
                    Toast.makeText(this@UserActivityReportActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    userActivityReportMainDO = userActivityMainDO
                    if (userActivityMainDO != null) {
                        if (userActivityReportMainDO.userDeliveryDOS.size > 0 || userActivityReportMainDO.purchaseReceiptDOS.size > 0 ||
                                userActivityReportMainDO.purchaseReturnDOS.size > 0 || userActivityReportMainDO.customerReturnDOS.size > 0 ||
                                userActivityReportMainDO.pickTicketDOS.size > 0 || userActivityReportMainDO.miscStopDOS.size > 0 || userActivityReportMainDO.salesInvoiceDOS.size > 0 ||
                                userActivityReportMainDO.cashDOS.size > 0 || userActivityReportMainDO.chequeDOS.size > 0 ||
                                userActivityReportMainDO.productDOS.size > 0 || userActivityReportMainDO.stockDOS.size > 0
                        ) {
                            tvNoDataFound.visibility = View.GONE

                            if (userActivityReportMainDO.userDeliveryDOS != null && userActivityReportMainDO.userDeliveryDOS.size > 0) {
                                tvSpotDeliveries.visibility = View.VISIBLE
                                llSpotDeliveryItem.visibility = View.VISIBLE
                                sdRecycleview.visibility = View.VISIBLE
                                llSdQuantity.visibility = View.VISIBLE
                                view1.visibility = View.VISIBLE
                                view2.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                var siteAdapter = UserActivityDeliveryAdapter(this@UserActivityReportActivity, userActivityReportMainDO.userDeliveryDOS)
                                sdRecycleview.adapter = siteAdapter
                                tvSdTotalAmount.text = getString(R.string.total) + " : " + userActivityMainDO.deliveryTotalQty + " " + userActivityReportMainDO.userDeliveryDOS.get(0).productUnit

                            } else {
                                tvSpotDeliveries.visibility = View.GONE
                                llSpotDeliveryItem.visibility = View.GONE
                                sdRecycleview.visibility = View.GONE
                                llSdQuantity.visibility = View.GONE
                                view1.visibility = View.GONE
                                view2.visibility = View.GONE

                            }


                            if (userActivityReportMainDO.purchaseReceiptDOS != null && userActivityReportMainDO.purchaseReceiptDOS.size > 0) {
                                tvPurchaseReciept.visibility = View.VISIBLE
                                llPurchaseReceiptItem.visibility = View.VISIBLE
                                purchaseReceiptRecycleview.visibility = View.VISIBLE
                                var linearLayoutManager7 = androidx.recyclerview.widget.LinearLayoutManager(this)

                                purchaseReceiptRecycleview.layoutManager = linearLayoutManager7
                                llReceiptQuantity.visibility = View.VISIBLE
                                viewPurchaseReceipt1.visibility = View.VISIBLE
                                viewPurchaseReceipt2.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                var siteAdapter = UserActivityReceiptAdapter(this@UserActivityReportActivity, userActivityReportMainDO.purchaseReceiptDOS)
                                purchaseReceiptRecycleview.adapter = siteAdapter
                                tvReceiptTotalAmount.text = getString(R.string.total) + " : " + userActivityMainDO.receiptTotalQty + " " + userActivityReportMainDO.purchaseReceiptDOS.get(0).productUnit

                            } else {
                                tvPurchaseReciept.visibility = View.GONE
                                llPurchaseReceiptItem.visibility = View.GONE
                                purchaseReceiptRecycleview.visibility = View.GONE
                                llReceiptQuantity.visibility = View.GONE
                                viewPurchaseReceipt1.visibility = View.GONE
                                viewPurchaseReceipt2.visibility = View.GONE

                            }

                            if (userActivityReportMainDO.customerReturnDOS != null && userActivityReportMainDO.customerReturnDOS.size > 0) {
                                tvCustomerReturn.visibility = View.VISIBLE
                                llCRItem.visibility = View.VISIBLE
                                var linearLayoutManager8 = androidx.recyclerview.widget.LinearLayoutManager(this)

                                crRecycleview.layoutManager = linearLayoutManager8
                                crRecycleview.visibility = View.VISIBLE
                                llCRQuantity.visibility = View.VISIBLE
                                viewCR1.visibility = View.VISIBLE
                                viewCR2.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                var siteAdapter = UserActivityCusReturnAdapter(this@UserActivityReportActivity, userActivityReportMainDO.customerReturnDOS)
                                crRecycleview.adapter = siteAdapter
                                tvCRTotalAmount.text = getString(R.string.total) + " : " + userActivityMainDO.customerReturnTotalQty + " " + userActivityReportMainDO.customerReturnDOS.get(0).productUnit

                            } else {
                                tvCustomerReturn.visibility = View.GONE
                                llCRItem.visibility = View.GONE
                                crRecycleview.visibility = View.GONE
                                llCRQuantity.visibility = View.GONE
                                viewCR1.visibility = View.GONE
                                viewCR2.visibility = View.GONE

                            }

                            if (userActivityReportMainDO.pickTicketDOS != null && userActivityReportMainDO.pickTicketDOS.size > 0) {
                                tvPickTickets.visibility = View.VISIBLE
                                llPCItem.visibility = View.VISIBLE
                                pcRecycleview.visibility = View.VISIBLE
                                var linearLayoutManager9 = androidx.recyclerview.widget.LinearLayoutManager(this)

                                pcRecycleview.layoutManager = linearLayoutManager9
                                llPCQuantity.visibility = View.VISIBLE
                                viewPC1.visibility = View.VISIBLE
                                viewPC2.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                var siteAdapter = UserActivitypickTicketAdapter(this@UserActivityReportActivity, userActivityReportMainDO.pickTicketDOS)
                                pcRecycleview.adapter = siteAdapter
                                tvPCTotalAmount.text = getString(R.string.total) + " : " + userActivityMainDO.pickTicketTotalQty + " " + userActivityReportMainDO.pickTicketDOS.get(0).productUnit

                            } else {
                                tvCustomerReturn.visibility = View.GONE
                                llPCItem.visibility = View.GONE
                                pcRecycleview.visibility = View.GONE
                                llPCQuantity.visibility = View.GONE
                                viewPC1.visibility = View.GONE
                                viewPC2.visibility = View.GONE

                            }
                            if (userActivityReportMainDO.purchaseReturnDOS != null && userActivityReportMainDO.purchaseReturnDOS.size > 0) {
                                tvPurchaseReturns.visibility = View.VISIBLE
                                llPRItem.visibility = View.VISIBLE
                                prRecycleview.visibility = View.VISIBLE
                                llPRQuantity.visibility = View.VISIBLE
                                viewPR1.visibility = View.VISIBLE
                                viewPR2.visibility = View.VISIBLE
                                var linearLayoutManager10 = androidx.recyclerview.widget.LinearLayoutManager(this)

                                prRecycleview.layoutManager = linearLayoutManager10
                                btnPrint.visibility = View.VISIBLE
                                var siteAdapter = UserActivityReturnAdapter(this@UserActivityReportActivity, userActivityReportMainDO.purchaseReturnDOS)
                                prRecycleview.adapter = siteAdapter
                                tvPRTotalAmount.text = getString(R.string.total) + " : " + userActivityMainDO.purchaseReturnTotalQty + " " + userActivityReportMainDO.purchaseReturnDOS.get(0).productUnit

                            } else {
                                tvPurchaseReturns.visibility = View.GONE
                                llPRItem.visibility = View.GONE
                                prRecycleview.visibility = View.GONE
                                llPRQuantity.visibility = View.GONE
                                viewPR1.visibility = View.GONE
                                viewPR2.visibility = View.GONE

                            }
                            if (userActivityReportMainDO.miscStopDOS != null && userActivityReportMainDO.miscStopDOS.size > 0) {
                                tvMiscStops.visibility = View.VISIBLE
                                llMSItem.visibility = View.VISIBLE
                                viewMS2.visibility = View.VISIBLE
                                viewMS1.visibility = View.VISIBLE
                                tvMSNumber.setText("" + userActivityMainDO.miscStopDOS.get(0).documentNumber)
                                tvMSCustomerName.setText("" + userActivityMainDO.miscStopDOS.get(0).customerName);
                                btnPrint.visibility = View.VISIBLE


                            } else {
                                tvPurchaseReturns.visibility = View.GONE
                                llMSItem.visibility = View.GONE
                                viewMS1.visibility = View.GONE
                                viewMS2.visibility = View.GONE

                            }
                            if (userActivityReportMainDO.salesInvoiceDOS != null && userActivityReportMainDO.salesInvoiceDOS.size > 0) {
                                tvSalesInvoices.visibility = View.VISIBLE
                                llSalesInvoicesItem.visibility = View.VISIBLE
                                siRecycleview.visibility = View.VISIBLE
                                llSIAmount.visibility = View.VISIBLE
                                view3.visibility = View.VISIBLE
                                view4.visibility = View.VISIBLE
                                view5.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalSIAmount.text = getString(R.string.total) + " : " + userActivityMainDO.invoiceTotalAmount + " AED"

                                var siteAdapter = UserActivityInvoiceAdapter(this@UserActivityReportActivity, userActivityReportMainDO.salesInvoiceDOS)
                                siRecycleview.adapter = siteAdapter
                            } else {
                                tvSalesInvoices.visibility = View.GONE
                                llSalesInvoicesItem.visibility = View.GONE
                                siRecycleview.visibility = View.GONE
                                llSIAmount.visibility = View.GONE
                                view3.visibility = View.GONE
                                view4.visibility = View.GONE
                                view5.visibility = View.GONE
                            }
                            if (userActivityReportMainDO.cashDOS != null && userActivityReportMainDO.cashDOS.size > 0) {
                                tvCashReciepts.visibility = View.VISIBLE
                                llCashItem.visibility = View.VISIBLE
                                cashRecycleview.visibility = View.VISIBLE
                                llCashAmount.visibility = View.VISIBLE
                                view6.visibility = View.VISIBLE
                                view7.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvCashTotalAmount.text = getString(R.string.total) + " : " + userActivityMainDO.cashTotalAmount + " AED"

                                var siteAdapter = UserActivityCashAdapter(this@UserActivityReportActivity, userActivityReportMainDO.cashDOS)
                                cashRecycleview.adapter = siteAdapter

                            } else {
                                tvCashReciepts.visibility = View.GONE
                                llCashItem.visibility = View.GONE
                                cashRecycleview.visibility = View.GONE
                                llCashAmount.visibility = View.GONE
                                view6.visibility = View.GONE
                                view7.visibility = View.GONE

                            }
                            if (userActivityReportMainDO.chequeDOS != null && userActivityReportMainDO.chequeDOS.size > 0) {
                                tvChequeReciepts.visibility = View.VISIBLE
                                llChequeAmount.visibility = View.VISIBLE
                                llChequeItem.visibility = View.VISIBLE
                                chequeRecycleview.visibility = View.VISIBLE
                                view8.visibility = View.VISIBLE
                                view9.visibility = View.VISIBLE
                                view10.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalChequeAmount.text = getString(R.string.total) + " : " + userActivityMainDO.chequeTotalAmount + " AED"

                                var siteAdapter = UserActivityChequeAdapter(this@UserActivityReportActivity, userActivityReportMainDO.chequeDOS)
                                chequeRecycleview.adapter = siteAdapter

                            } else {
                                tvChequeReciepts.visibility = View.GONE
                                llChequeAmount.visibility = View.GONE
                                llChequeItem.visibility = View.GONE
                                chequeRecycleview.visibility = View.GONE
                                view8.visibility = View.GONE
                                view9.visibility = View.GONE
                                view10.visibility = View.GONE

                            }


                            if (userActivityReportMainDO.productDOS != null && userActivityReportMainDO.productDOS.size > 0) {
                                tvTotalProductQuantity.visibility = View.VISIBLE
                                tvProducts.visibility = View.VISIBLE
                                llProductQuantity.visibility = View.VISIBLE
                                llProducts.visibility = View.VISIBLE
                                productsRecycleview.visibility = View.VISIBLE
                                view11.visibility = View.VISIBLE
                                view12.visibility = View.VISIBLE
                                view13.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalProductQuantity.text = getString(R.string.total) + " : " + userActivityMainDO.productsTotalQty + " UN"

                                var siteAdapter = UserActivityProductAdapter(this@UserActivityReportActivity, userActivityReportMainDO.productDOS)
                                productsRecycleview.adapter = siteAdapter

                            } else {
                                tvTotalProductQuantity.visibility = View.GONE
                                tvProducts.visibility = View.GONE
                                llProductQuantity.visibility = View.GONE
                                llProducts.visibility = View.GONE
                                productsRecycleview.visibility = View.GONE
                                view11.visibility = View.GONE
                                view12.visibility = View.GONE
                                view13.visibility = View.GONE

                            }

                            if (userActivityReportMainDO.stockDOS != null && userActivityReportMainDO.stockDOS.size > 0) {
                                tvStock.visibility = View.VISIBLE
                                llStockQuantity.visibility = View.VISIBLE
                                llStock.visibility = View.VISIBLE
                                stockRecycleview.visibility = View.VISIBLE
                                view14.visibility = View.VISIBLE
                                view15.visibility = View.VISIBLE
                                view16.visibility = View.VISIBLE
                                btnPrint.visibility = View.VISIBLE
                                tvTotalStockQuantity.text = getString(R.string.total) + " : " + userActivityMainDO.stockTotalQty + " UN"

                                var siteAdapter = UserActivityProductAdapter(this@UserActivityReportActivity, userActivityReportMainDO.stockDOS)
                                stockRecycleview.adapter = siteAdapter

                            } else {
                                tvStock.visibility = View.GONE
                                llStockQuantity.visibility = View.GONE
                                llStock.visibility = View.GONE
                                stockRecycleview.visibility = View.GONE
                                view14.visibility = View.GONE
                                view15.visibility = View.GONE
                                view16.visibility = View.GONE
                                btnPrint.visibility = View.GONE

                            }
                        } else {
                            tvNoDataFound.visibility = View.VISIBLE
                            tvSpotDeliveries.visibility = View.GONE
                            llSpotDeliveryItem.visibility = View.GONE
                            sdRecycleview.visibility = View.GONE
                            llSdQuantity.visibility = View.GONE
                            view1.visibility = View.GONE
                            view2.visibility = View.GONE
                            tvSpotDeliveries.visibility = View.GONE

                            tvSalesInvoices.visibility = View.GONE
                            llSalesInvoicesItem.visibility = View.GONE
                            siRecycleview.visibility = View.GONE
                            llSIAmount.visibility = View.GONE
                            view3.visibility = View.GONE
                            view4.visibility = View.GONE

                            tvCashReciepts.visibility = View.GONE
                            llCashItem.visibility = View.GONE
                            cashRecycleview.visibility = View.GONE
                            llCashAmount.visibility = View.GONE
                            view5.visibility = View.GONE
                            view6.visibility = View.GONE

                            tvChequeReciepts.visibility = View.GONE
                            llChequeAmount.visibility = View.GONE
                            llChequeItem.visibility = View.GONE
                            chequeRecycleview.visibility = View.GONE
                            view7.visibility = View.GONE
                            view8.visibility = View.GONE
                            view9.visibility = View.GONE
                            view10.visibility = View.GONE


                            tvProducts.visibility = View.GONE
                            llProductQuantity.visibility = View.GONE
                            llProducts.visibility = View.GONE
                            productsRecycleview.visibility = View.GONE
                            view11.visibility = View.GONE
                            view12.visibility = View.GONE
                            view13.visibility = View.GONE

                            tvStock.visibility = View.GONE
                            llStockQuantity.visibility = View.GONE
                            llStock.visibility = View.GONE
                            stockRecycleview.visibility = View.GONE
                            view14.visibility = View.GONE
                            view15.visibility = View.GONE
                            view16.visibility = View.GONE


                            btnPrint.visibility = View.GONE
                            tvNoDataFound.visibility = View.VISIBLE
                            Toast.makeText(this@UserActivityReportActivity, resources.getString(R.string.no_data), Toast.LENGTH_SHORT).show()

                        }

                    } else {
                        tvSpotDeliveries.visibility = View.GONE
                        llSpotDeliveryItem.visibility = View.GONE
                        sdRecycleview.visibility = View.GONE
                        llSdQuantity.visibility = View.GONE
                        view1.visibility = View.GONE
                        view2.visibility = View.GONE

                        tvSalesInvoices.visibility = View.GONE
                        llSalesInvoicesItem.visibility = View.GONE
                        siRecycleview.visibility = View.GONE
                        llSIAmount.visibility = View.GONE
                        view3.visibility = View.GONE
                        view4.visibility = View.GONE

                        tvCashReciepts.visibility = View.GONE
                        llCashItem.visibility = View.GONE
                        cashRecycleview.visibility = View.GONE
                        llCashAmount.visibility = View.GONE
                        view5.visibility = View.GONE
                        view6.visibility = View.GONE

                        tvChequeReciepts.visibility = View.GONE
                        llChequeAmount.visibility = View.GONE
                        llChequeItem.visibility = View.GONE
                        chequeRecycleview.visibility = View.GONE
                        view7.visibility = View.GONE
                        view8.visibility = View.GONE
                        view9.visibility = View.GONE
                        view10.visibility = View.GONE

                        tvProducts.visibility = View.GONE
                        llProductQuantity.visibility = View.GONE
                        llProducts.visibility = View.GONE
                        productsRecycleview.visibility = View.GONE
                        view11.visibility = View.GONE
                        view12.visibility = View.GONE
                        view13.visibility = View.GONE

                        tvStock.visibility = View.GONE
                        llStockQuantity.visibility = View.GONE
                        llStock.visibility = View.GONE
                        stockRecycleview.visibility = View.GONE
                        view14.visibility = View.GONE
                        view15.visibility = View.GONE
                        view16.visibility = View.GONE

                        btnPrint.visibility = View.GONE
                        tvNoDataFound.visibility = View.VISIBLE
                        Toast.makeText(this@UserActivityReportActivity, resources.getString(R.string.no_data), Toast.LENGTH_SHORT).show()

                    }


                }


            }

            userActivityReportActivity.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)
            showToast("" + resources.getString(R.string.internet_connection))
        }

//


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
//            selectInvoiceList()
        }
    }


    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString
        tvTransactionDate.text = "" + dayString + " - " + monthString + " - " + cyear
        from = tvTransactionDate.text.toString()

    }


    private fun prepareActivityReport() {

//        val objLlist = ArrayList<Serializable>();
//        objLlist.add(transactionCashMainDO);
//        objLlist.add(transactionChequeMainDO)
        AppConstants.Trans_Date = tvTransactionDate.text.toString().trim().replace("-", "/")
//        printDocument(userActivityReportMainDO, PrinterConstants.PrintUserActivityReport)

    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@UserActivityReportActivity)
        if (printConnection.isBluetoothEnabled) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast(getString(R.string.please_enable_your_bluetooth))
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.name
                val mac = device.address // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac)
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            ReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
            return

        } else {
            //Display error message
        }
    }
}