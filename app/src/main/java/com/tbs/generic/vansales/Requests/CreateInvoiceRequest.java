package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CreateInvoiceDO;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CreateInvoiceRequest extends AsyncTask<String, Void, Boolean> {

    private CreateInvoiceDO createInvoiceDO;
    private Context mContext;
    private String id, notes, message, imgsign;
    PreferenceUtils preferenceUtils;

    public CreateInvoiceRequest(String img, String note, String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
        this.notes = note;
        this.imgsign = img;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, CreateInvoiceDO createInvoiceDO, String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String idd = preferenceUtils.getStringFromPreference(PreferenceUtils.EMIRATES_ID, "");
        String role = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YSDLNUM", id);
            jsonObject.put("I_YCREUSR", role);

            jsonObject.put("I_YEMIRATEID", idd);
            jsonObject.put("I_YMSG", notes);
            jsonObject.put("I_YSIGNATURE", imgsign);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }

        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CREATE_INVOICE, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("invoice xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            createInvoiceDO = new CreateInvoiceDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("I_YSDLNUM")) {
                            if (text.length() > 0) {
                                createInvoiceDO.shipmentNumber = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YSDLNUM")) {
                            if (text.length() > 0) {
                                createInvoiceDO.o_shipmentNumber = text;

                            }


                        } else if (attribute.equalsIgnoreCase("O_YSINVNO")) {
                            if (text.length() > 0) {
                                createInvoiceDO.salesInvoiceNumber = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YFLG")) {
                            if (text.length() > 0) {
                                createInvoiceDO.flag = Integer.parseInt(text);

                            }

                        } else if (attribute.equalsIgnoreCase("O_YAMT")) {
                            if (text.length() > 0) {
                                createInvoiceDO.amount = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            if (text.length() > 0) {
                                createInvoiceDO.currency = text;

                            }

                        } else if (attribute.equalsIgnoreCase("O_YDOC")) {
                            if (text.length() > 0) {
                                createInvoiceDO.doc = Integer.parseInt(text);

                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRI")) {
                            if (text.length() > 0) {
                                createInvoiceDO.email = Integer.parseInt(text);

                            }

                        } else if (attribute.equalsIgnoreCase("O_YEMAIL")) {
                            if (text.length() > 0) {
                                createInvoiceDO.print = Integer.parseInt(text);

                            }

                        } else if (attribute.equalsIgnoreCase("O_YMESSAGE")) {
                            if (text.length() > 0) {
                                createInvoiceDO.message = text;

                            }

                        }
                        text = "";
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, createInvoiceDO, ServiceURLS.message);
        }
    }
}