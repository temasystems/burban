package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class VRSelectionDO implements Serializable {
    public String transactionID = "";
    public String driverName = "";
    public int trip = 0;
    public int poFlag = 0;
    public String vehicleCode = "";
    public String instructions="";
    public String vehicleNumber = "";
    public String scheduleDate = "";
    public String siteID = "";
    public String siteDescription = "";
    public String vrID = "";
    public String locationType = "";
    public String vehicleCarrier = "";
    public String unit = "";
    public String departudate = "";

    public String aDate = "";
    public String dDate = "";
    public String plate = "";
    public String dTime = "";
    public String aTime    = "";
    public String nShipments = "";
    public String driver = "";
    public String notes          = "";
    public String location          = "";
    public int checkinFlag          = 0;
    public int odometer = 0;


    public String loadbay="";
}
