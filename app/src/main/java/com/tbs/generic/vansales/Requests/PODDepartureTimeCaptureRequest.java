package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class PODDepartureTimeCaptureRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String userId, currentDate, documentType, documentNumber;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;
    String message = "";
    public PODDepartureTimeCaptureRequest(String userID, String date, String documentTypE, String documentNumbeR, Context mContext) {

        this.mContext = mContext;
        this.userId = userID;
        this.currentDate = date;
        this.documentType = documentTypE;
        this.documentNumber = documentNumbeR;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO createInvoiceDO ,String message);

    }

    public boolean runRequest() {
        preferenceUtils    = new PreferenceUtils(mContext);
       String id          = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "");
        String vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.NON_VEHICLE_CODE, "");
       PodDo podDo = StorageManager.getInstance(mContext).getDepartureData(mContext);

        String time= CalendarUtils.timeDifference(podDo.getPodTimeCapturpodDepartureDate(),podDo.getPodTimeCapturepodDepartureTime(),mContext);
        String mins=CalendarUtils.depRevisedTimeDifference(podDo.getPodTimeCaptureArrivalDate(),podDo.getPodTimeCaptureArrivalTime(),podDo.getPodTimeCapturpodDepartureDate(),podDo.getPodTimeCapturepodDepartureTime());

        int dropFlag       = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
       JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_XUSRID", userId);
            jsonObject.put("I_XDAT", currentDate);

                if(dropFlag==2){
                    jsonObject.put("I_XDOCNUM", preferenceUtils.getStringFromPreference(PreferenceUtils.PRE_RECEIPT,""));
                }else {
                    jsonObject.put("I_XDOCNUM", documentNumber);
                }
                if(dropFlag==0){
                    jsonObject.put("I_XDOCTYP", 8);

                }else {
                    jsonObject.put("I_XDOCTYP", dropFlag);

                }
            jsonObject.put("I_XSERVICETIME", mins);


            jsonObject.put("I_XTIMDIFF", time);


            jsonObject.put("I_XVEHCODE", vehicleCode);
            jsonObject.put("I_XVR", id);
            jsonObject.put("I_XCONARRDAT", podDo.getPodTimeCaptureArrivalDate());
            jsonObject.put("I_XCONARRTIM", podDo.getPodTimeCaptureArrivalTime());
            jsonObject.put("I_XSTRUNLDAT", podDo.getPodTimeCaptureStartLoadingDate());
            jsonObject.put("I_XSTRUNLTIM", podDo.getPodTimeCaptureStartLoadingTime());
            jsonObject.put("I_XCAPDELDAT", podDo.getPodTimeCaptureCaptureDeliveryDate());
            jsonObject.put("I_XCAPDELTIM", podDo.getPodTimeCaptureCaptureDeliveryTime());
            jsonObject.put("I_XENDUNLDAT", podDo.getPodTimeCaptureEndLoadingDate());
            jsonObject.put("I_XENDUNLTIM", podDo.getPodTimeCaptureEndLoadingTime());
            jsonObject.put("I_XVALDOCDAT", podDo.getPodTimeCaptureValidateDeliveryDate());
            jsonObject.put("I_XVALDOCTIM", podDo.getPodTimeCaptureValidateDeliveryTime());
            jsonObject.put("I_XCONDEPDAT", podDo.getPodTimeCapturpodDepartureDate());
            jsonObject.put("I_XCONDEPTIM", podDo.getPodTimeCapturepodDepartureTime());

            jsonObject.put("I_XCONARRLAT", podDo.getArrivalLattitude());
            jsonObject.put("I_XCONARRLOG", podDo.getArrivalLongitude());
            jsonObject.put("I_XCREDOCLAT", podDo.getCreDocLattitude());
            jsonObject.put("I_XCREDOCLOG", podDo.getCreDOCLongitude());
            jsonObject.put("I_XCONDEPLAT", podDo.getDepartureLattitude());
            jsonObject.put("I_XCONDEPLOG", podDo.getDepartureLongitude());
            jsonObject.put("I_XBPRATING", Double.valueOf(podDo.getDepartureRating()));

            jsonObject.put("I_XNOTE", podDo.getNotes());
            jsonObject.put("I_XNOP", podDo.getName());


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.POD_TIME_CAPTURE, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("DEPARTURE xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO,ServiceURLS.message);
        }
    }
}