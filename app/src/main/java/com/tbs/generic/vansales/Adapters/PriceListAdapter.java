package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.PriceDetailsDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.MyViewHolder> {

    private ArrayList<PriceDetailsDO> listOrderDos;
    private String imageURL;
    private Context context;
    private String userId, paymentType, cardId;
    PreferenceUtils preferenceUtils;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView product, customer, min, max, unit, price;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId, customerDescription, productDescription;

        public MyViewHolder(View view) {
            super(view);
            product = view.findViewById(R.id.product);
            customer = view.findViewById(R.id.customer);
            price = view.findViewById(R.id.price);
            unit = view.findViewById(R.id.unit);
            min = view.findViewById(R.id.MinQuantity);
            max = view.findViewById(R.id.MaxQuantity);
            productDescription = view.findViewById(R.id.productDescription);
            customerDescription = view.findViewById(R.id.customerDescription);


        }
    }


    public PriceListAdapter(Context context, ArrayList<PriceDetailsDO> listOrderDos) {
        this.context = context;
        this.listOrderDos = listOrderDos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.price_details_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
//
        final PriceDetailsDO customerDo = listOrderDos.get(position);
        if (customerDo.customerName.length() > 0) {

            holder.customer.setText(context.getString(R.string.customer) + " : " + customerDo.customerName);
        } else {
            holder.customer.setText("");

        }
        if (customerDo.productName.length() > 0) {

            holder.product.setText(context.getString(R.string.product) + " : " + customerDo.productName);
        } else {
            holder.product.setText("");

        }
        if (customerDo.customerDescription.length() > 0) {
            holder.customerDescription.setText(context.getString(R.string.description) + " : " + customerDo.customerDescription);

        } else {
            holder.customerDescription.setText("");

        }
        if (customerDo.productDescription.length() > 0) {
            holder.productDescription.setText(context.getString(R.string.description) + " : " + customerDo.productDescription);

        } else {
            holder.productDescription.setText("");

        }
        holder.price.setText(context.getString(R.string.price) + customerDo.price);
        holder.min.setText(context.getString(R.string.min_qty) + customerDo.minQuantity);
        holder.unit.setText(context.getString(R.string.unit) + customerDo.salesUnit);
        holder.max.setText(context.getString(R.string.max_qty) + customerDo.maxQuantity);
    }

    @Override
    public int getItemCount() {
        return listOrderDos.size();
    }

}
