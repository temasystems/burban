package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.CreateDeliveryDO;
import com.tbs.generic.vansales.Model.CreateDeliveryMainDO;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class CreateLotRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    ArrayList<ActiveDeliveryDO> purchaseRecieptDos;
    PreferenceUtils preferenceUtils;


    public CreateLotRequest( ArrayList<ActiveDeliveryDO> purchaseRecieptDOs, Context mContext) {

        this.mContext = mContext;
        this.purchaseRecieptDos = purchaseRecieptDOs;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }
    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO createDeliveryMainDO, String msg);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String vrID = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");

        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("I_XVCRNUM", vrID);
            jsonObject1.put("I_XVCRTYP", 6);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONArray jsonArray = new JSONArray();
        if (purchaseRecieptDos != null && purchaseRecieptDos.size() > 0) {
            for (int i = 0; i < purchaseRecieptDos.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("I_XVCRLIN", purchaseRecieptDos.get(i).linenumber);

                    jsonObject.put("I_XITMREF", purchaseRecieptDos.get(i).product);
                    jsonObject.put("I_XQTYSTU",  purchaseRecieptDos.get(i).orderedQuantity);
                    jsonObject.put("I_XUOM",  purchaseRecieptDos.get(i).unit);
                    jsonObject.put("I_XLOT", purchaseRecieptDos.get(i).purchaseOrderLotNumber);
                    jsonObject.put("I_XSTA", purchaseRecieptDos.get(i).purchaseOrderStatus);
                    jsonArray.put(i, jsonObject);


                } catch (Exception e) {
                    System.out.println("Exception " + e);
                }
            }
            try {
                jsonObject1.put("GRP2", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CREATE_LOT, jsonObject1);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("create delivery xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if(text.length()>0){
                                successDO.flag = Integer.parseInt(text);

                            }


                        }


                        text="";

                    }



                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO,ServiceURLS.message);

        }
    }
}