package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.tbs.generic.vansales.Adapters.LoanReturnAdapter
import com.tbs.generic.vansales.Adapters.TrailerSelectionAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.LoanReturnRequest
import com.tbs.generic.vansales.Requests.SalesReturnListRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.capture_return_details.*
import kotlinx.android.synthetic.main.include_toolbar.*

//
class CaptureLoanReturnDetailsActivity : BaseActivity() {
    lateinit var loanReturnAdapter: LoanReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    var selectDOS: java.util.ArrayList<LoanReturnDO>? = null

    var shipmentIds = ArrayList<String>()

    override fun initialize() {
      val  llCategories = layoutInflater.inflate(R.layout.capture_return_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        tv_title.text = getString(R.string.loan_return)
        initializeControls()
        btnScan.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(applicationContext, TestBarcodeActivity::class.java)

            startActivityForResult(intent, 10)
        }
        ivClearSearch.setOnClickListener{
            Util.preventTwoClick(it)
            val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            etSearch.requestFocus()
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
            llSearch.visibility = View.VISIBLE

        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if(etSearch.text.toString().equals("", true)){
                    if(selectDOS!=null &&selectDOS!!.size>0){
                        val loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()
                        var shipmentIds = ArrayList<String>()
                        if(selectDOS!!.size>0){
                            for (i in selectDOS!!.indices){
                                if(!shipmentIds.contains(selectDOS!!.get(i).shipmentNumber)){
                                    shipmentIds.add(selectDOS!!.get(i).shipmentNumber)
                                }
                            }
                        }

                        if(shipmentIds.size>0){
                            for (j in shipmentIds.indices){//4
                                var loanReturnDOS = ArrayList<LoanReturnDO>()
                                for (i in selectDOS!!.indices) {//10
                                    if (selectDOS!!.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                                        loanReturnDOS.add(selectDOS!!.get(i))
                                    }
                                }
                                loanReturnDOsMap.put(shipmentIds.get(j), selectDOS!!)
                            }
                        }
                        loanReturnAdapter = LoanReturnAdapter(this@CaptureLoanReturnDetailsActivity, loanReturnDOsMap,"Products")
                        recycleview.adapter = loanReturnAdapter
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    }
                    else{
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                }
                else if(etSearch.text.toString().length>1){
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun filter(filtered : String) : java.util.ArrayList<LoanReturnDO> {
        val customerDOs = java.util.ArrayList<LoanReturnDO>()
        for (i in selectDOS!!.indices){
            if(selectDOS!!.get(i).serialDO.contains(filtered, true)
                    ||selectDOS!!.get(i).productDescription.contains(filtered, true)){
                customerDOs.add(selectDOS!!.get(i))
            }
        }
        val loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()
        var shipmentIds = ArrayList<String>()
        if(customerDOs!!.size>0){
            for (i in customerDOs!!.indices){
                if(!shipmentIds.contains(customerDOs!!.get(i).shipmentNumber)){
                    shipmentIds.add(customerDOs!!.get(i).shipmentNumber)
                }
            }
        }

        if(shipmentIds.size>0){
            for (j in shipmentIds.indices){//4
                var loanReturnDOS = ArrayList<LoanReturnDO>()
                for (i in customerDOs!!.indices) {//10
                    if (customerDOs!!.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                        loanReturnDOS.add(customerDOs!!.get(i))
                    }
                }
                loanReturnDOsMap.put(shipmentIds.get(j), loanReturnDOS)
            }
        }
        if(customerDOs.size>0){

            loanReturnAdapter.refreshAdapter(loanReturnDOsMap,"Products")
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else{
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun initializeControls() {

        recycleview             = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        tvNoDataFound           = findViewById<TextView>(R.id.tvNoDataFound)

        val tvCustomerName = findViewById<View>(R.id.tvCustomerName) as TextView
        val btnConfirm      = findViewById<Button>(R.id.btnConfirm)
        val linearLayoutManager     = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recycleview.layoutManager = linearLayoutManager

        tvCustomerName.text = getString(R.string.customer)+" : "+preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER,"")
        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = LoanReturnRequest(this@CaptureLoanReturnDetailsActivity)
            driverListRequest.setOnResultListener { isError, loanReturnMainDO ->
                hideLoader()
                if (isError) {
                    tvNoDataFound.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    btnConfirm.visibility = View.GONE
                    showToast(resources.getString(R.string.server_error))
                } else {
                    selectDOS=loanReturnMainDO.loanReturnDOS
                    val loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()
                    var shipmentIds = ArrayList<String>()
                    if(loanReturnMainDO!=null && loanReturnMainDO.loanReturnDOS.size>0){
                        for (i in loanReturnMainDO.loanReturnDOS.indices){
                            if(!shipmentIds.contains(loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber)){
                                shipmentIds.add(loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber)
                            }
                        }
                    }

                    if(shipmentIds.size>0){
                        for (j in shipmentIds.indices){//4
                            var loanReturnDOS = ArrayList<LoanReturnDO>()
                            for (i in loanReturnMainDO.loanReturnDOS.indices) {//10
                                if (loanReturnMainDO.loanReturnDOS.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                                    loanReturnDOS.add(loanReturnMainDO.loanReturnDOS.get(i))
                                }
                            }
                            loanReturnDOsMap.put(shipmentIds.get(j), loanReturnDOS)
                        }
                    }

                    if(loanReturnDOsMap.size>0){
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                        btnConfirm.visibility = View.VISIBLE
                        loanReturnAdapter = LoanReturnAdapter(this@CaptureLoanReturnDetailsActivity, loanReturnDOsMap,"Products")
                        recycleview.adapter = loanReturnAdapter
                    }else{
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                        btnConfirm.visibility = View.GONE
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "",false)

        }


        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)


            if(loanReturnAdapter!=null){
                val loanReturnDos = loanReturnAdapter.selectedLoadStockDOs
                if(loanReturnDos!=null && !loanReturnDos.isEmpty()){
                    val intent = Intent(this@CaptureLoanReturnDetailsActivity, SelectedLoanReturnDetailsActivity::class.java)
                    intent.putExtra("Products", loanReturnDos)

                    startActivityForResult(intent, 11)
                }
                else{
                    showToast(getString(R.string.please_select_items))
                }
            }
            else{
                showToast(getString(R.string.no_return_item_found))
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 11 && resultCode == 11){
            val intent = Intent()
            intent.putExtra("CapturedReturns", true)
            var CRELAT = data!!.getStringExtra("CRELAT")
            var CRELON = data!!.getStringExtra("CRELON")
            var rating = data!!.getStringExtra("RATING")
            var notes = data!!.getStringExtra("NOTES")
            var loanReturnname = data!!.getStringExtra("NAME")

            intent.putExtra("CRELAT", CRELAT)
            intent.putExtra("CRELON", CRELON)
            intent.putExtra("RATING",rating)
            intent.putExtra("NOTES",notes)
            intent.putExtra("NAME", loanReturnname)
            intent.putExtra("CapturedReturns", true)
           // AppConstants.CapturedReturns= true
            setResult(11, intent)
            finish()
        }else{
            var serial = data?.getStringExtra("SERIAL")
            if(serial!=null&&serial.length>0){
                var boolean:Boolean=false

                for (i in selectDOS!!.indices) {
                    if (selectDOS!!.get(i).serialDO.equals(serial)) {
                        boolean=true
                        selectDOS!!.get(i).isChecked=true
                        selectDOS!!.get(i).state=2

                        break
                    } else {
                        boolean=false
                    }

                }
                if(boolean==true){
                    val loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()
                    if(selectDOS!!.size>0){
                        for (i in selectDOS!!.indices){
                            if(!shipmentIds.contains(selectDOS!!.get(i).shipmentNumber)){
                                shipmentIds.add(selectDOS!!.get(i).shipmentNumber)
                            }
                        }
                    }

                    if(shipmentIds.size>0){
                        for (j in shipmentIds.indices){//4
                            var loanReturnDOS = ArrayList<LoanReturnDO>()
                            for (i in selectDOS!!.indices) {//10
                                if (selectDOS!!.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                                    loanReturnDOS.add(selectDOS!!.get(i))
                                }
                            }
                            loanReturnDOsMap.put(shipmentIds.get(j), loanReturnDOS)
                        }
                    }
                    loanReturnAdapter.refreshAdapter(loanReturnDOsMap,"Products")

                }else{
                    showToast(getString(R.string.no_serial_found))
                }
            }

        }


    }
}