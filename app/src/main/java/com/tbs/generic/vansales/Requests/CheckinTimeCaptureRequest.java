package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.VehicleCheckInDo;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CheckinTimeCaptureRequest extends AsyncTask<String, Void, Boolean> {
    long minuts = 0;
    private SuccessDO successDO;
    private Context mContext;
    private String userID, currentDate, id,timeDifference;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private int flaG;

    public CheckinTimeCaptureRequest(String userId, String currentdate, String iD, Context mContext) {

        this.mContext = mContext;
        this.userID = userId;
        this.id = iD;

        this.currentDate = currentdate;



    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO createInvoiceDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String doc_number = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");
        String reading = preferenceUtils.getStringFromPreference(PreferenceUtils.OPENING_READING, "");
        int reading2 = preferenceUtils.getIntFromPreference(PreferenceUtils.ODO_READ, 0);
        String sequence = preferenceUtils.getStringFromPreference(PreferenceUtils.SEQUENCE, "");

        SimpleDateFormat smf = new SimpleDateFormat("yyyyMMddHHmm");
        String depatureDate = preferenceUtils.getStringFromPreference(PreferenceUtils.CD_DATE, "");
        String departureTime = preferenceUtils.getStringFromPreference(PreferenceUtils.CD_TIME, "");

        try {
            Date finaldepDate = smf.parse(depatureDate + "" + departureTime);
            Date finalCheckDate = smf.parse(CalendarUtils.getDate()+CalendarUtils.getTime());
            long date1 = finaldepDate.getTime();
            long date2 = finalCheckDate.getTime();
            long diff;
            if (date1 > date2) {

                diff = date1 - date2;
            } else {
                diff = date2 - date1;
            }
            long seconds = diff / 1000;
            minuts = seconds / 60;
            long hours = minuts / 60;
            timeDifference=hours+":"+minuts;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        VehicleCheckInDo vehicleCheckInDo = StorageManager.getInstance(mContext).getVehicleCheckInData(mContext);
        ArrayList<TrailerSelectionDO> equipDos = StorageManager.getInstance(mContext).getEquipmentSelectionDO(mContext);

        ArrayList<TrailerSelectionDO>  trailerDOs = StorageManager.getInstance(mContext).getTrailerSelectionDO(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
//
            jsonObject.put("I_XSRCNUM", doc_number);
            jsonObject.put("I_X10CUSRID", userID);
            jsonObject.put("I_XDATE", currentDate);
            jsonObject.put("I_XDOCNUM", id);
            jsonObject.put("I_XDSCONARRDAT", vehicleCheckInDo.getCheckinTimeCaptureArrivalDate());
            jsonObject.put("I_XDSCONARRTIM", vehicleCheckInDo.getCheckinTimeCaptureArrivalTime());
            jsonObject.put("I_XDSSTRLODDAT", vehicleCheckInDo.getCheckinTimeCaptureStartLoadingDate());
            jsonObject.put("I_XDSSTRLODTIM", vehicleCheckInDo.getCheckinTimeCaptureStartLoadingTime());
            jsonObject.put("I_XDSENDLODDAT", vehicleCheckInDo.getCheckinTimeCaptureEndLoadingDate());
            jsonObject.put("I_XDSENDLODTIM", vehicleCheckInDo.getCheckinTimeCaptureEndLoadingTime());
            jsonObject.put("I_XDSVEHINSPDAT", vehicleCheckInDo.getCheckinTimeCaptureVehicleInspectionDate());
            jsonObject.put("I_XDSVEHINSPTIM", vehicleCheckInDo.getCheckinTimeCaptureVehicleInspectionTime());
            jsonObject.put("I_XDSGATINSPDAT", vehicleCheckInDo.getCheckinTimeCaptureGateInspectionDate());
            jsonObject.put("I_XDSGATINSPTIM", vehicleCheckInDo.getCheckinTimeCaptureGateInspectionTime());
            jsonObject.put("I_XDSCHKINDAT", vehicleCheckInDo.getCheckinTimeCapturCheckinDate());
            jsonObject.put("I_XDSCHKINTIM", vehicleCheckInDo.getCheckinTimeCaptureCheckinTime());
            jsonObject.put("I_XCIGEOX", vehicleCheckInDo.getCheckinLattitude());
            jsonObject.put("I_XCIGEOY", vehicleCheckInDo.getCheckinLongitude());
            jsonObject.put("I_XTIM_DIFF", "" + timeDifference);
            jsonObject.put("I_XLOGINSEQNO", sequence);
            jsonObject.put("I_XLOADBAY", preferenceUtils.getIntFromPreference(PreferenceUtils.LOAD_BAY,0));


            for (int i=0;i<trailerDOs.size();i++) {
                if (i == 0) {
                    jsonObject.put("I_XTRAILER1", trailerDOs.get(i).trailer);
                }
                if (i == 1) {
                    jsonObject.put("I_XTRAILER2", trailerDOs.get(i).trailer);

                }
            }


            if (reading.isEmpty()) {
                jsonObject.put("I_XODORED", reading2);
                preferenceUtils.saveString(PreferenceUtils.OPENING_READING, String.valueOf(reading2));

            } else {
                jsonObject.put("I_XODORED", reading);
                preferenceUtils.saveString(PreferenceUtils.OPENING_READING, reading);

            }
//            for (int i=0;i<equipDos.size();i++) {
//                if (i == 0) {
//                    jsonObject.put("I_XEQUPMNT1", equipDos.get(i).trailer);
//                }
//                if (i == 1) {
//                    jsonObject.put("I_XEQUPMNT2", equipDos.get(i).trailer);
//
//                }
//            }
            JSONArray jsonArray = new JSONArray();
            if (equipDos != null && equipDos.size() > 0) {
                for (int i = 0; i < equipDos.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_XEQUPMNT", equipDos.get(i).trailer);

                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CHECKIN_TIME_CAPTURE, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_XAPPVERSION")) {
                            if (text.length() > 0) {

                                successDO.version = text;
                            }


                        }
                    }

                    text = "";
                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        try {
            ((BaseActivity) mContext).hideLoader();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }


}