package com.tbs.generic.vansales.dialogs

import android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.listeners.ResultListner
import kotlinx.android.synthetic.main.dialog_add_view_note.*

class AddViewNoteDialogFragment : DialogFragment() {
    private lateinit var resultListner: ResultListner
    private lateinit var message: String
    private lateinit var title: String


    fun newInstance(
        title: String,
        message: String,
        resultListner: ResultListner
    ): AddViewNoteDialogFragment {
        val addViewNoteDialogFragment = AddViewNoteDialogFragment()
        addViewNoteDialogFragment.title = title
        addViewNoteDialogFragment.message = message

        addViewNoteDialogFragment.resultListner = resultListner
        return addViewNoteDialogFragment
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, Theme_Holo_Light_Dialog_NoActionBar_MinWidth)

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)

        return dialog
    }


    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.dialog_add_view_note, null)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arrow_back_icon_iv.setOnClickListener { dismiss() }
        if (!TextUtils.isEmpty(message)) {
            ed_add_note.setText(message);
            ed_add_note.setSelection(message.length)
            tv_title.text = getString(R.string.edit_note)
        } else {
            tv_title.text = getString(R.string.add_note)
        }
        btn_yes.setOnClickListener {
            if (!TextUtils.isEmpty(ed_add_note.text)) {
                resultListner.onResultListner(ed_add_note.text.toString(), true)
                dismiss()
            } else {
                Toast.makeText(activity, "Please enter note", Toast.LENGTH_SHORT).show()
            }

        }

    }
}