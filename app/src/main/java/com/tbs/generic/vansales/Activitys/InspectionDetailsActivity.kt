package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.here.services.radiomap.internal.Utility
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Adapters.QuestionaryListAdapter
import com.tbs.generic.vansales.Adapters.SerialListAdapter
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.InspectionQuestionsListRequest
import com.tbs.generic.vansales.Requests.InspectionSerialListRequest
import com.tbs.generic.vansales.Requests.SaveInspectionQuestionariesRequest
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.PreferenceUtils.PRODUCT_CODE
import com.tbs.generic.vansales.utils.Util
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.activity_inspection_details.*
import kotlinx.android.synthetic.main.include_do_inspection.*
import kotlinx.android.synthetic.main.include_inspection_info.*
import kotlinx.android.synthetic.main.include_inspection_summary.*
import kotlinx.android.synthetic.main.price_details_data.*

import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class InspectionDetailsActivity : BaseActivity() {

    private lateinit var questionaryListAdapter: QuestionaryListAdapter
    private lateinit var file: File
    private var type: Int = 0
    var screenPostion = 0;
    var bitmap: Bitmap? = null
    var DIRECTORY =
            Environment.getExternalStorageDirectory().getPath() + "/InspectionUserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/
            "sign"

    var contract: String = ""
    var preparation: String = ""
    var product = ""
    var lineNumber = 0
    var shipment = ""
    var productDescription = ""
    lateinit var inspectMainDO: InspectionMainDO;
    private var fileDetailsList: ArrayList<FileDetails>? = null
    private  var serialListAdapter: SerialListAdapter = SerialListAdapter(this, ArrayList())

    private lateinit var filesPreviewAdapter: FilesPreviewAdapter

    override fun initialize() {
        val llCategories = layoutInflater.inflate(R.layout.activity_inspection_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            backClickOperations()
        }
        tvScreenTitle.text = getString(R.string.add_serial)
        if (intent.hasExtra(Constants.INPUT_FLAG)) {
            type = intent.extras!!.getInt(Constants.INPUT_FLAG)!!
        }
        if (intent.hasExtra(Constants.PRODUCT_CODE)) {
            product = intent.extras!!.getString(Constants.PRODUCT_CODE)!!
        }
        if (intent.hasExtra("PRODUCT")) {
            productDescription = intent.extras!!.getString("PRODUCT")!!
        }
        if (intent.hasExtra("SHIPMENT_NUMBER")) {
            shipment = intent.extras!!.getString("SHIPMENT_NUMBER")!!
        }
        if (intent.hasExtra(Constants.LINE_NUMBER)) {
            lineNumber = intent.extras!!.getInt(Constants.LINE_NUMBER)!!
        }

        fileDetailsList = ArrayList()

        if (type != 2) {
            screenPostion = 1
        }
        isStoragePermissionGranted()
        getDataFromServer()
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        initializeControls()

    }

    override fun initializeControls() {
        tv_product_title.setText(product + " - " + productDescription)


    }


    private fun getDataFromServer() {
        if (Util.isNetworkAvailable(this)) {
            val request = InspectionQuestionsListRequest(
                    this,
                    product,
                    contract,
                    preparation,
                    lineNumber,
                    type
            )
            request.setOnResultListener { isError, contarctMainDO ->
                if (isError) {
                    showToast(resources.getString(R.string.server_error));
                } else {
                    if (contarctMainDO != null) {
                        this.inspectMainDO = contarctMainDO;

                        //comment
//                        var serialDo =InspectionDO()
//                        serialDo.question="ABCD"
//                        inspectMainDO.inspectionDOS.add(serialDo)
//                        var serialDo1 =InspectionDO()
//                        serialDo1.question="FGHI"
//                        inspectMainDO.inspectionDOS.add(serialDo1)
//
                        if (inspectMainDO.inspectionDOS.size > 0) {
                            intitiateControls();
                        } else {
                            showToast(
                                    resources.getString(R.string.no_data)
                            )
                            finish()
                        }
                    } else {
                        //Utility.showToast(this, resources.getString(R.string.no_data));
                        rcv_inspection_questionary.visibility = View.GONE
                    }
                }
            }
            request.execute()
        } else {
            showToast(resources.getString(R.string.internet_connection));
        }

    }


    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                )
                return false
            }
        } else {
            return true
        }
    }

    private fun intitiateControls() {

        if (inspectMainDO == null) {
            return
        }

        screenNavigation()

        ll_back.setOnClickListener {

            backClickOperations()
        }

        ll_next.setOnClickListener {
            if (tv_next.text.equals(getString(R.string.generate_report))) {
                //mSignature.save(it, StoredPath);
                saveAndGenerateReport();

            } else if (tv_next.text.equals(getString(R.string.do_inspection))) {
                /*if (inspectMainDO.inspectionDOS != null && inspectMainDO.inspectionDOS.size > 0) {


                    for (inspectionDo in inspectMainDO.inspectionDOS) {
                        if (!inspectionDo.ischecked) {
                            if (TextUtils.isEmpty(inspectionDo.note)) {
                                Utility.showToast(
                                    this,
                                    "Please Check Or provide note for " + inspectionDo.question
                                )
                                return@setOnClickListener
                            }
                        }
                    }
                }*/
                if (screenPostion <= 2) {
                    screenPostion++
                    screenNavigation()
                }
            } else {
                if (screenPostion <= 2) {
                    screenPostion++
                    screenNavigation()
                }
            }
        }

        btn_clear.setOnClickListener {
            canvasLayout.clearCanvas()
        }
        /*  mSignature = signature(this, null)
          mSignature.setBackgroundColor(Color.WHITE)
          canvasLayout.addView(
              mSignature,
              ViewGroup.LayoutParams.MATCH_PARENT,
              ViewGroup.LayoutParams.MATCH_PARENT
          )

          file = File(DIRECTORY)
          if (!file.exists()) {
              file.mkdir()
          }
  */
    }

    private fun saveAndGenerateReport() {
        if (!TextUtils.isEmpty(ed_conducted_by.text.toString())) {
            val inputParmObj = InputParamSaveQuesModel();
            inputParmObj.contractNumber = inspectMainDO.contractNumber
            inputParmObj.preparationNumber = inspectMainDO.preparationNumber
            inputParmObj.lineNumber = lineNumber.toString()
            inputParmObj.type = inspectMainDO.type
            inputParmObj.product = product
            inputParmObj.serialNumber = inspectMainDO.serialNumber
            inputParmObj.customer = inspectMainDO.customer;
            inputParmObj.mainComment = ed_add_note.text.toString();
            inputParmObj.inspectionDOS = inspectMainDO.inspectionDOS
            inputParmObj.conductedBy = ed_conducted_by.text.toString()

            try {
                /*val fis = FileInputStream(savedPath)
            val bitmap = BitmapFactory.decodeStream(fis)
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val bytes = baos.toByteArray()
            val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
            Log.d("PIC-->", encImage)*/

                if (!canvasLayout.isBitmapEmpty) {


                    val stream = ByteArrayOutputStream();
                    val bmp = canvasLayout.signatureBitmap
                    bmp.compress(Bitmap.CompressFormat.PNG, 2, stream);
                    val byteArray = stream.toByteArray()
                    bmp.recycle();
                    inputParmObj.signatue =
                            Base64.encodeToString(byteArray, Base64.DEFAULT).trim();
                } else {
                    showToast("Please provide signature")
                    return
                }

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            for (list in this.fileDetailsList!!) {
                try {
                    //val fis = FileInputStream(File(list.filePath))
                    //val bitmap = BitmapFactory.decodeStream(fis)
                    val bitmap = Compressor(this).compressToBitmap(File(list.filePath));
                    val baos = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 40, baos)
                    val bytes = baos.toByteArray()
                    val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                    inputParmObj.capturedImagesListBulk!!.add(encImage)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }


            if (Util.isNetworkAvailable(this)) {
                val request = SaveInspectionQuestionariesRequest(
                        this,
                        inputParmObj
                )
                request.setOnResultListener { isError, contarctMainDO ->
                    if (isError) {
                       showToast( resources.getString(R.string.server_error));

                    } else {
                        if (contarctMainDO != null) {
                            if (contarctMainDO.flag == 20) {
                                YesOrNoDialogFragment().newInstance(
                                        "Success",
                                        "Generated Successfully",
                                        ResultListner { `object`, isSuccess ->
                                        val intent = Intent(this, ReportActivity::class.java)
                                        intent.putExtra(Constants.SCREEN_TYPE, type)
                                        intent.putExtra(Constants.CONTRACT_NUMBER, shipment)
//                                    intent.putExtra(Constants.PREPARATION_NUMBER, preparation)
                                        intent.putExtra(Constants.LINE_NUMBER, lineNumber)
                                        intent.putExtra(Constants.SERIAL_NUMBER, inspectMainDO.serialNumber)

                                        startActivity(intent)
                                            finish()
                                        },
                                        false
                                ).show(supportFragmentManager, "YesOrNoDialogFragment")
                            }
                            if (contarctMainDO.flag == 1) {
                                showToast(contarctMainDO.successFlag);
                                return@setOnResultListener
                            }

                        } else {
                            showToast(resources.getString(R.string.failure));
                        }
                    }
                }
                request.execute()
            } else {
                showToast(resources.getString(R.string.internet_connection));
            }
        } else {
            showToast("Please enter conducted by");
        }
    }

    private fun backClickOperations() {
        if (type != 2) {

            if (screenPostion == 1) {
                finish()
                return
            }

            screenPostion--
            screenNavigation()

        } else {
            if (screenPostion == 0) {
                finish()
                return
            }

            screenPostion--
            screenNavigation()

        }

    }


    private fun screenNavigation() {

        if (screenPostion == 0) {
            tvScreenTitle.text = getString(R.string.inspection_info)
            imv_back.visibility = View.GONE
            tv_back.text = getString(R.string.cancel)
            ll_inspecton_info.visibility = View.VISIBLE
            ll_inspecton_questionary.visibility = View.GONE
            ll_inspecton_summary.visibility = View.GONE
            imv_next.visibility = View.VISIBLE
            tv_next.text = getString(R.string.next)
            setScreenOneData()

        } else if (screenPostion == 1) {
            tvScreenTitle.text = getString(R.string.do_inspection)
            imv_back.visibility = View.VISIBLE
            tv_back.text = getString(R.string.previuos)
            ll_inspecton_info.visibility = View.GONE
            ll_inspecton_questionary.visibility = View.VISIBLE
            ll_inspecton_summary.visibility = View.GONE
            imv_next.visibility = View.VISIBLE
            tv_next.text = getString(R.string.do_inspection)
            setSecondScreenData()


        } else if (screenPostion == 2) {
            tvScreenTitle.text = getString(R.string.summary)
            imv_back.visibility = View.VISIBLE
            tv_back.text = getString(R.string.previuos)
            ll_inspecton_info.visibility = View.GONE
            ll_inspecton_questionary.visibility = View.GONE
            ll_inspecton_summary.visibility = View.VISIBLE
            imv_next.visibility = View.GONE
            tv_next.text = getString(R.string.generate_report)
            setThirdScreenData()
        }
    }

    private fun setThirdScreenData() {
        inspectMainDO.productDescription = productDescription
        inspectMainDO.contractNumber = shipment
        if (type == 2) {
            if(serialListAdapter!=null&&serialListAdapter.getSelectedSerialListDO()!!.size>0){
                inspectMainDO.serialNumber = serialListAdapter.getSelectedSerialListDO()!!.get(0).serialDO

            }
        }else{
            llSerials.visibility = View.GONE

        }
        inspectMainDO.date = CalendarUtils.getPDFDate(this)
        inspectMainDO.time = CalendarUtils.getPDFTime(this)

        tvConductedBy.text = inspectMainDO.productDescription
        tvDateSummary.text = inspectMainDO.date
        tvTimeSummary.text = inspectMainDO.time
        tv_serialNumber_summary.text = inspectMainDO.serialNumber
        imagesSetUp()
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(
                        com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT,
                        4 - fileDetailsList!!.size
                )
            } else {
                intent.putExtra(
                        com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT,
                        4
                )
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }
    }

    private fun setSecondScreenData() {

        if (inspectMainDO.inspectionDOS.size > 0) {
            rcv_inspection_questionary.visibility = View.VISIBLE
            questionaryListAdapter = QuestionaryListAdapter(this, inspectMainDO.inspectionDOS)
            rcv_inspection_questionary.adapter = questionaryListAdapter

            var isUnChecked = false
            for (isDOss: InspectionDO in inspectMainDO.inspectionDOS) {
                if (!isDOss.ischecked) {
                    isUnChecked = true;
                    break
                }
            }
//            if (isUnChecked) {
//                main_check.isChecked = false
//            } else {
//                main_check.isChecked = true
//            }
//            main_check.setOnClickListener {
//
//                questionaryListAdapter.setChecked(main_check.isChecked)
//            }
        } else {
            //Utility.showToast(this, resources.getString(R.string.no_data));
            rcv_inspection_questionary.visibility = View.GONE
        }
    }

    private fun setScreenOneData() {

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        recyclevieww.layoutManager = linearLayoutManager

        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = InspectionSerialListRequest(shipment, lineNumber, this@InspectionDetailsActivity)
            driverListRequest.setOnResultListener { isError, serialDOS ->
                hideLoader()
                if (isError) {
                    tvNoDataFoundd.visibility = View.VISIBLE
                    recyclevieww.visibility = View.GONE
//                    showToast(resources.getString(R.string.server_error))
                } else {
//                    var serialDo =SerialListDO()
//                    serialDo.serialDO="SR91222227678989"
//                    serialDOS.add(serialDo)
//                    var serialDo1 =SerialListDO()
//                    serialDo1.serialDO="SR68732698916981"
//                    serialDOS.add(serialDo1)
                    if (serialDOS != null && serialDOS.size > 0) {
                        serialListAdapter = SerialListAdapter(this@InspectionDetailsActivity, serialDOS)
                        recyclevieww.adapter = serialListAdapter
                    } else {
                        tvNoDataFoundd.visibility = View.VISIBLE
                        recyclevieww.visibility = View.GONE
//                        showToast(resources.getString(R.string.server_error))
                    }


                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }
    }

    private fun imagesSetUp() {

        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = View.VISIBLE

            } else {
                btnAddImages.visibility = View.GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = View.VISIBLE
            } else {
                recycleviewImages.visibility = View.GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 888) {
            val images =
                    data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {
                Log.d("image-->", images.size.toString() + "----" + images)
                var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    fileDetailsList?.add(0, fileDetails)
                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }
    }

    override fun onBackPressed() {

        super.onBackPressed()
        backClickOperations()

    }
}
