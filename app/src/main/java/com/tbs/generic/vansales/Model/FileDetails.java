package com.tbs.generic.vansales.Model;


import java.io.Serializable;

/**
 * The type File details.
 */
public class FileDetails implements Serializable {
    /**
     * The File name.
     */
    String fileName, /**
     * The File path.
     */
    filePath, /**

     * The File type.
     */
    fileType,
    fileImage,
    /**
     * The Size.
     */
    size;

    boolean fileSource;


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public void setFileImg(String img) {
        this.fileImage = img;
    }
    public String fileImage() {
        return fileImage;
    }
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFileImage() {
        return fileImage;
    }

    public void setFileImage(String fileImage) {
        this.fileImage = fileImage;
    }

    public boolean isFileSource() {
        return fileSource;
    }

    public void setFileSource(boolean fileSource) {
        this.fileSource = fileSource;
    }
}
