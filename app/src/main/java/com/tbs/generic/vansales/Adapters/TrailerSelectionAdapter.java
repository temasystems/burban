package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.LoanReturnDO;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.VRSelectionDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class TrailerSelectionAdapter extends RecyclerView.Adapter<TrailerSelectionAdapter.MyViewHolder> {

    private ArrayList<TrailerSelectionDO> trailerSelectionDOS;
    private Context context;


    public void refreshAdapter(@NotNull ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.trailerSelectionDOS = trailerSelectionDOS;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTrailer,tvDescription,tvType, tvAxle, tvMass, tvVolume;
        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvTrailer = itemView.findViewById(R.id.tvTrailer);
            tvAxle = itemView.findViewById(R.id.tvAxle);
            tvMass = itemView.findViewById(R.id.tvMass);
            tvVolume = itemView.findViewById(R.id.tvVolume);
            tvType = itemView.findViewById(R.id.tvType);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            cbSelected             =  itemView.findViewById(R.id.cbSelected);



        }
    }


    public TrailerSelectionAdapter(Context context, ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.context = context;
        this.trailerSelectionDOS = trailerSelectionDOS;

    }
    private ArrayList<TrailerSelectionDO> selectedDOs = new ArrayList<>();
    public ArrayList<TrailerSelectionDO> getSelectedTrailerDOs(){
        return selectedDOs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.trailer_selection_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TrailerSelectionDO trailerSelectionDO = trailerSelectionDOS.get(position);
//        final VRSelectionDO vrSelectionDO = new VRSelectionDO();
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lg));
        } else {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lsg));
        }
        holder.tvTrailer.setText(trailerSelectionDO.trailer);
        holder.tvDescription.setText(trailerSelectionDO.trailerDescription);

            holder.tvAxle.setText(""+trailerSelectionDO.axel);
        holder.tvType.setText(""+trailerSelectionDO.type);

        holder.tvMass.setText(""+trailerSelectionDO.mass+" "+trailerSelectionDO.volumeUnit);

        holder.tvVolume.setText("" + trailerSelectionDO.volume+" "+trailerSelectionDO.massUnit);
        holder.cbSelected.setOnCheckedChangeListener(null);

        if(trailerSelectionDO.isSelceted==2){
            if(!holder.cbSelected.isChecked()){
                holder.cbSelected.setChecked(true);
                trailerSelectionDO.isProductAdded = true;
                selectedDOs.add(trailerSelectionDO);
            }

        }else {
            if(holder.cbSelected.isChecked()){
                selectedDOs.remove(trailerSelectionDO);
                holder.cbSelected.setChecked(false);
                trailerSelectionDO.isProductAdded = false;

            }


        }

        holder.cbSelected.setChecked(trailerSelectionDO.isProductAdded);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                trailerSelectionDO.isProductAdded = isChecked;
                if (isChecked) {
//                    selectedDOs.add(trailerSelectionDO);
                    if(selectedDOs.size()<2){
                        selectedDOs.add(trailerSelectionDO);
                        trailerSelectionDO.isSelceted=2;
                    }else {

                        holder.cbSelected.setChecked(false);
//                        holder.cbSelected.setEnabled(false);
                        trailerSelectionDO.isSelceted=1;
                        ((BaseActivity)context).showToast(context.getResources().getString(R.string.max_2));
                    }
                } else {

                    selectedDOs.remove(trailerSelectionDO);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return trailerSelectionDOS != null ? trailerSelectionDOS.size() : 10;
//        return 10;

    }

}
