package com.tbs.generic.vansales.common;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.tbs.generic.vansales.utils.Constants.NAMESPACE;
import static com.tbs.generic.vansales.utils.Constants.SOAP_ACTION;

import org.json.XML;


public class WebServiceAcess {


    public WebServiceAcess() {
    }

    public String runRequest(Context context, String METHOD_NAME, String publicName, JSONObject jsonObject) {
        return serverRequest(context, METHOD_NAME, publicName, jsonObject);
    }


    public String serverRequest(Context context, String METHOD_NAME, String publicName, JSONObject jsonObject) {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);
        String username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        String password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        String ip   = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        String port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        String pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String http = preferenceUtils.getStringFromPreference(PreferenceUtils.HTTP, "");
        String URL = http + ip + ServiceURLS.COLON + port + ServiceURLS.APP_SUB_URL;
//        String URL = http + ip + ServiceURLS.APP_SUB_URL; //https

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        //  request.addProperty("publicName", "YMTRLOGIN");
        request.addProperty("publicName", publicName);
        request.addProperty("inputXml", jsonObject.toString());
        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        Locale locale= context.getResources().getConfiguration().locale;
        if(locale.equals(Locale.FRENCH)){
            callcontext.addProperty("codeLang",  "FRA");

        }else {
            callcontext.addProperty("codeLang",  Constants.ENG);
        }

        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("requestConfig", "");
        request.addSoapObject(callcontext);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
//        ((BaseActivity)context).allowAllSSL();
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL, 60000);
        androidHttpTransport.debug = true;
        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));
            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return resultXML;
            } else {
                String error = response.getProperty(0).toString();
                String[] messagee = error.split("message=");
                error = messagee[1].replace("]", "");
                error = error.replace("}", "");
                error = error.replace(";", "");
                error = error.replace(".", "");
                error = error.replace("[", " ");
                error = error.replace("|", "");
                error = error.replace("\\", "");
                error = error.replace("{", "");
                error = error.replace(",", "");
                error = error.replace("line", "");
                error = error.replace(":", "");
                error = error.replace("type", "");
                error = error.replace("1", "");
                error = error.replace("2", "");
                error = error.replace("3", "");
                error = error.replace("4", "");
                error = error.replace("5", "");
                error = error.replace("6", "");
                error = error.replace("7", "");
                error = error.replace("8", "");
                error = error.replace("9", "");
                error = error.replace("0", "");
                error = error.replace("$adx", "");
                error = error.replace("nextLong", "");
                if (error.equals(" ")){
                    error = error.replace(" ", "");

                }
                error = error.replace("Line Number", "");
                error = error.replace("CAdxMessagetype=3", "");
                error = error.replace("CAdxMessagetype=", "");
                error = error.replace("CAdxMessage=", "");
                ServiceURLS.message = "";

                if (error.contains("Nil price!")) {
                    ServiceURLS.message = "Product line does not have price";
                } else {
                    if (error.contains("No Transaction in Progress")) {
                        ServiceURLS.message = "Error In Service Response,Please try again";
                    } else {
                        ServiceURLS.message = error;
                    }
                }

                return "";
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            ServiceURLS.message="Socket Timeout Exception";
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            ServiceURLS.message = "";
            return "";
        }
    }


    public String getCurrentDate() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyyMMdd");
        Date myDate = new Date();
        String date = timeStampFormat.format(myDate);
        return date;
    }
}
