package com.tbs.generic.vansales.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.ConfigurationDo;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.Model.DashboardCaptureDO;
import com.tbs.generic.vansales.Model.FileDetails;
import com.tbs.generic.vansales.Model.InspectionDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.LoanReturnDO;
import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.Model.PodDo;
import com.tbs.generic.vansales.Model.SerialListDO;
import com.tbs.generic.vansales.Model.SiteDO;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.UpdateDocumentDO;
import com.tbs.generic.vansales.Model.VehicleCheckOutDo;
import com.tbs.generic.vansales.database.models.SaveStockDBModel;
import com.tbs.generic.vansales.Model.VehicleCheckInDo;
import com.tbs.generic.vansales.database.tables.LoadVehicleStockTable;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Kishore on 12/12/2018.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */

public class StorageManager {
    private static final String TAG = "StorageManager";

    private static TBSDatabaseHelper sDatabaseHelper;
    private static StorageManager sStorageManager;

    private StorageManager(Context context) {
        sDatabaseHelper = new TBSDatabaseHelper(context);
    }

    public static synchronized StorageManager getInstance(Context context) {
        if (sStorageManager == null) {
            sStorageManager = new StorageManager(context);
        }
        return sStorageManager;
    }

    //

    /**
     * Save Stock in Database
     */
    public long saveStockToDB(SaveStockDBModel saveStockDBModel) {
        SQLiteDatabase database = null;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(LoadVehicleStockTable.COLUMN_ITEM_ID, saveStockDBModel.getItem_id());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_DESC, saveStockDBModel.getItem_desc());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY, saveStockDBModel.getItem_quantity());
//            values.put(LoadVehicleStockTable.STOCK_UNIT, saveStockDBModel.getItem_quantity());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_WEIGHT, saveStockDBModel.getItem_weight());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_MASS, saveStockDBModel.getItem_mass());
            values.put(LoadVehicleStockTable.COLUMN_USER_ID, saveStockDBModel.getUser_id());
            values.put(LoadVehicleStockTable.COLUMN_STATUS, saveStockDBModel.getStatus());
            values.put(LoadVehicleStockTable.COLUMN_OTHER, saveStockDBModel.getOther());

            return database.insert(LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST, null, values);
        } catch (Exception e) {
            Log.e(TAG, "Exception at saving Stock Details");
        } finally {
            closeDatabase(database);
        }
        return 0;
    }


    /**
     * Update Stock based on selection from the list.
     */

    public long updateStockVehicleDetails(SaveStockDBModel saveStockDBModel) {
        SQLiteDatabase database = null;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(LoadVehicleStockTable.COLUMN_ITEM_ID, saveStockDBModel.getItem_id());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_DESC, saveStockDBModel.getItem_desc());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY, saveStockDBModel.getItem_quantity());
//            values.put(LoadVehicleStockTable.STOCK_UNIT, saveStockDBModel.getItem_quantity());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_WEIGHT, saveStockDBModel.getItem_weight());
            values.put(LoadVehicleStockTable.COLUMN_ITEM_MASS, saveStockDBModel.getItem_mass());
            values.put(LoadVehicleStockTable.COLUMN_USER_ID, "userid");
            values.put(LoadVehicleStockTable.COLUMN_STATUS, saveStockDBModel.getStatus());
            values.put(LoadVehicleStockTable.COLUMN_OTHER, saveStockDBModel.getOther());
            return database.update(LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST, values, LoadVehicleStockTable.COLUMN_ITEM_ID + "=" + saveStockDBModel.getItem_id(), null);
        } catch (Exception e) {
            Log.e(TAG, "Exception at updating stock");
        } finally {
            closeDatabase(database);
        }
        return 0;
    }

    /**
     * Update Stock based on selection from the list.
     */
    public long updateSingleStockDetails(String driverId, String itemID, int result, double massValue) {
        SQLiteDatabase database = null;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            // values.put(LoadVehicleStockTable.COLUMN_ITEM_ID, itemID);
            values.put(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY, "" + result);
            values.put(LoadVehicleStockTable.COLUMN_ITEM_MASS, "" + massValue);
            //  values.put(LoadVehicleStockTable.COLUMN_USER_ID, driverId);
            return database.update(LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST, values,
                    LoadVehicleStockTable.COLUMN_ITEM_ID
                            + " ='" + itemID + "'", null);
        } catch (Exception e) {
            Log.e(TAG, "Exception at updating stock");
        } finally {
            closeDatabase(database);
        }
        return 0;
    }
    /*TODO: Venu Create updatte totalQuantity query*/

    /**
     * Get all saved stock details
     */
    public ArrayList<SaveStockDBModel> getAllVechicleStockDetails(String driverID) {
        SQLiteDatabase database = null;
        Cursor cursor = null;
        try {
            database = sDatabaseHelper.getReadableDatabase();
            String selection = LoadVehicleStockTable.COLUMN_USER_ID + " = ? ";
            String[] selectionArgs = {driverID};
            cursor = database.query(LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST, null, selection, selectionArgs, null,
                    null, null);
            int colID = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ID);
            int itemID = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_ID);
            int itemDesc = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_DESC);
            int itemQuantity = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY);
            int stockUnit = cursor.getColumnIndex(LoadVehicleStockTable.STOCK_UNIT);
            int itemWeight = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_WEIGHT);
            int itemMass = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_MASS);
            int userID = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_USER_ID);
            int status = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_STATUS);
            int columnOther = cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_OTHER);

            ArrayList<SaveStockDBModel> searchItemArrayList = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    SaveStockDBModel saveStockDBModel = new SaveStockDBModel();
                    saveStockDBModel.setItem_id(cursor.getString(itemID));
                    saveStockDBModel.setItem_desc(cursor.getString(itemDesc));
                    saveStockDBModel.setItem_quantity(cursor.getString(itemQuantity));
                    saveStockDBModel.setItem_weight(cursor.getString(itemWeight));
                    saveStockDBModel.setItem_mass(cursor.getString(itemMass));
                    saveStockDBModel.setUser_id(cursor.getString(userID));
                    saveStockDBModel.setStatus(cursor.getString(status));
                    saveStockDBModel.setOther(cursor.getString(columnOther));
                    searchItemArrayList.add(saveStockDBModel);
                } while (cursor.moveToNext());
            }
            return searchItemArrayList;
        } catch (Exception e) {
            Log.e(TAG, "Exception at getting all stocks");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            closeDatabase(database);
        }
        return null;
    }

    /*Delete Selected record Stock Details*/
    public boolean deleteStockDetails(String userId, String itemId) {
        SQLiteDatabase database = null;
        Cursor cursor = null;
        try {

            database = sDatabaseHelper.getReadableDatabase();

            String Query = "DELETE FROM " + LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST + " where " + LoadVehicleStockTable.COLUMN_ITEM_ID
                    + " ='" + itemId + "'";
            cursor = database.rawQuery(Query, null);
            if (cursor.getCount() <= 0) {
                cursor.close();
                return false;
            } else {
                Log.d("Stock Count:: ", "" + cursor.getCount());
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return false;
    }

    /*Close database connection*/
    private void closeDatabase(SQLiteDatabase database) {
        if (database != null) {
            database.close();
        }
    }
    public ArrayList<SerialListDO> getSerialList(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "SerialList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<SerialListDO> skipShipmentList = (ArrayList<SerialListDO>) is.readObject();
                is.close();
                fis.close();
                return skipShipmentList;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getSkipShipmentList() : " + e.getMessage());
            return new ArrayList<SerialListDO>();
        }
        return new ArrayList<SerialListDO>();
    }

    public void saveSerialList(Activity activity, ArrayList<SerialListDO> skipShipmentList) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SerialList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(skipShipmentList);
            os.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "saveSkipShipmentList() : " + e.getMessage());
        }
    }

    public void deleteSerialList(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SerialList.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteSkipShipmentList() : " + e.getMessage());
        }
    }
    public boolean insertCheckInData(Context context, VehicleCheckInDo vehicleCheckInDo) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleCheckIn.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(vehicleCheckInDo);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "insertCheckInData() : " + e.getMessage());
            return false;
        }
    }

    public void deleteCheckInData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleCheckIn.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteCheckInData() : " + e.getMessage());
        }
    }

    public VehicleCheckInDo getVehicleCheckInData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleCheckIn.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                VehicleCheckInDo vehicleCheckInDo = (VehicleCheckInDo) is.readObject();
                is.close();
                fis.close();
                return vehicleCheckInDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVehicleCheckInData() : " + e.getMessage());
            return new VehicleCheckInDo();
        }
        return new VehicleCheckInDo();
    }




    public boolean insertDashboardData(Context context, DashboardCaptureDO vehicleCheckInDo) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "Dashboard.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(vehicleCheckInDo);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "insertCheckInData() : " + e.getMessage());
            return false;
        }
    }

    public void deleteDashboardData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "Dashboard.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteCheckInData() : " + e.getMessage());
        }
    }

    public DashboardCaptureDO getDashboardData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "Dashboard.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                DashboardCaptureDO vehicleCheckInDo = (DashboardCaptureDO) is.readObject();
                is.close();
                fis.close();
                return vehicleCheckInDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVehicleCheckInData() : " + e.getMessage());
            return new DashboardCaptureDO();
        }
        return new DashboardCaptureDO();
    }







    public boolean insertVehicleCheckOutData(Context context, VehicleCheckOutDo vehicleCheckInDo) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CheckOut.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(vehicleCheckInDo);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "insertCheckOutData() : " + e.getMessage());
            return false;
        }
    }

    public boolean insertCheckOutData(Context context, VehicleCheckInDo vehicleCheckInDo) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleCheckOut.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(vehicleCheckInDo);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "insertCheckOutData() : " + e.getMessage());
            return false;
        }
    }
    public void deleteVehCheckOutData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CheckOut.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteCheckOutData() : " + e.getMessage());
        }
    }
    public void deleteCheckOutData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleCheckOut.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteCheckOutData() : " + e.getMessage());
        }
    }
    public VehicleCheckOutDo getCheckOutData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CheckOut.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                VehicleCheckOutDo vehicleCheckInDo = (VehicleCheckOutDo) is.readObject();
                is.close();
                fis.close();
                return vehicleCheckInDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVehicleCheckOutData() : " + e.getMessage());
            return new VehicleCheckOutDo();
        }
        return new VehicleCheckOutDo();
    }
    public VehicleCheckInDo getVehicleCheckOutData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleCheckOut.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                VehicleCheckInDo vehicleCheckInDo = (VehicleCheckInDo) is.readObject();
                is.close();
                fis.close();
                return vehicleCheckInDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVehicleCheckOutData() : " + e.getMessage());
            return new VehicleCheckInDo();
        }
        return new VehicleCheckInDo();
    }

//    public ArrayList<SiteDO> getSiteListData(SiteActivity siteActivity) {
//        try {
//            File file = new File(siteActivity.getApplication().getFilesDir().toString() + "/", "SiteList.txt");
//            if (file.exists()) {
//                FileInputStream fis = new FileInputStream(file);
//                ObjectInputStream is = new ObjectInputStream(fis);
//                ArrayList<SiteDO> siteDOS = (ArrayList<SiteDO>) is.readObject();
//                is.close();
//                fis.close();
//                return siteDOS;
//            }
//        } catch (Exception e) {
//            Log.e(TAG, "getSiteListData() : " + e.getMessage());
//            return new ArrayList<SiteDO>();
//        }
//        return new ArrayList<SiteDO>();
//    }
//
//    public void saveSiteListData(SiteActivity siteActivity, ArrayList<SiteDO> siteDOS) {
//        try {
//            File file = new File(siteActivity.getApplication().getFilesDir().toString() + "/", "SiteList.txt");
//            FileOutputStream fos = new FileOutputStream(file);
//            ObjectOutputStream os = new ObjectOutputStream(fos);
//            os.writeObject(siteDOS);
//            os.close();
//            fos.close();
//        } catch (Exception e) {
//            Log.e(TAG, "saveSiteListData() : " + e.getMessage());
//        }
//    }

    public void deleteSiteListData(Activity siteActivity) {
        try {
            File file = new File(siteActivity.getApplication().getFilesDir().toString() + "/", "SiteList.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteSiteListData() : " + e.getMessage());
        }
    }

    public void saveDepartureData(Activity activity, PodDo podDo) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "podData.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(podDo);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveDepartureData() : " + e.getMessage());
        }
    }

    public void deleteDepartureData(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "podData.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteDepartureData() : " + e.getMessage());
        }
    }

    public PodDo getDepartureData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "podData.txt");
            if (file.exists()) {

                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                PodDo podDo = (PodDo) is.readObject();
                is.close();
                fis.close();
                return podDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getDepartureData() : " + e.getMessage());
            return new PodDo();
        }
        return new PodDo();
    }


    public void saveImagesData(Activity activity, ArrayList<FileDetails> fileDetails) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "fileDetails.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(fileDetails);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveDepartureData() : " + e.getMessage());
        }
    }
    public ArrayList<FileDetails> getImagesData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "fileDetails.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<FileDetails> loadStockDOS = (ArrayList<FileDetails>) is.readObject();
                is.close();
                fis.close();
                return loadStockDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVanScheduleProducts() : " + e.getMessage());
            return new ArrayList<FileDetails>();
        }
        return new ArrayList<FileDetails>();
    }
    public void deleteImagesData(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "fileDetails.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteDepartureData() : " + e.getMessage());
        }
    }


    public ArrayList<FileDetails> getLoanReturnImagesData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "fileLoanReturnDetails.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<FileDetails> loadStockDOS = (ArrayList<FileDetails>) is.readObject();
                is.close();
                fis.close();
                return loadStockDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVanScheduleProducts() : " + e.getMessage());
            return new ArrayList<FileDetails>();
        }
        return new ArrayList<FileDetails>();
    }
    public void deleteLoanReturnImagesData(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "fileLoanReturnDetails.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteDepartureData() : " + e.getMessage());
        }
    }
    public void saveLoanReturnImagesData(Activity activity, ArrayList<FileDetails> fileDetails) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "fileLoanReturnDetails.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(fileDetails);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveDepartureData() : " + e.getMessage());
        }
    }


    public void saveLoanDeliveryImagesData(Activity activity, ArrayList<FileDetails> fileDetails) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "fileLoanDeliveryDetails.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(fileDetails);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveDepartureData() : " + e.getMessage());
        }
    }
    public ArrayList<FileDetails> getLoanDeliveryImagesData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "fileLoanDeliveryDetails.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<FileDetails> loadStockDOS = (ArrayList<FileDetails>) is.readObject();
                is.close();
                fis.close();
                return loadStockDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVanScheduleProducts() : " + e.getMessage());
            return new ArrayList<FileDetails>();
        }
        return new ArrayList<FileDetails>();
    }
    public void deleteLoanDeliveryImagesData(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "fileLoanDeliveryDetails.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteDepartureData() : " + e.getMessage());
        }
    }

    public ArrayList<LoadStockDO> getVanScheduleProducts(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VanScheduleProducts.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<LoadStockDO> loadStockDOS = (ArrayList<LoadStockDO>) is.readObject();
                is.close();
                fis.close();
                return loadStockDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVanScheduleProducts() : " + e.getMessage());
            return new ArrayList<LoadStockDO>();
        }
        return new ArrayList<LoadStockDO>();
    }
    public void saveCompletedShipments(Activity activity, ArrayList<PickUpDo> pickUpDos) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "completedShipments.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(pickUpDos);
            os.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "saveCompletedShipments() : " + e.getMessage());
        }
    }

    public void deleteCompletedShipments(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "completedShipments.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteCompletedShipments() : " + e.getMessage());
        }
    }

    public ArrayList<PickUpDo> getCompletedShipments(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "completedShipments.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<PickUpDo> pickUpDos = (ArrayList<PickUpDo>) is.readObject();
                is.close();
                fis.close();
                return pickUpDos;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getDepartureData() : " + e.getMessage());
            return new ArrayList<PickUpDo>();
        }
        return new ArrayList<PickUpDo>();
    }
    public void saveActiveDeliveryMainDo(Activity activity, ActiveDeliveryMainDO activeDeliveryMainDO) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDelivery.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(activeDeliveryMainDO);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveActiveDeliveryMainDo() : " + e.getMessage());
        }
    }

    public void deleteUpdateActiveDeliveryMainDo(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDelivery.txt");
            if (file.exists())
                file.delete();
        } catch (Exception e) {
            Log.e(TAG, "saveActiveDeliveryMainDo() : " + e.getMessage());
        }
    }

    public ActiveDeliveryMainDO getUpdateActiveDeliveryMainDo(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ActiveDelivery.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) is.readObject();
                is.close();
                fis.close();
                return activeDeliveryMainDO;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getActiveDeliveryMainDo() : " + e.getMessage());
            return new ActiveDeliveryMainDO();
        }
        return new ActiveDeliveryMainDO();
    }
    public void saveUpdateActiveDeliveryMainDo(Activity activity, ActiveDeliveryMainDO activeDeliveryMainDO) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDelivery.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(activeDeliveryMainDO);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveActiveDeliveryMainDo() : " + e.getMessage());
        }
    }

    public void deleteActiveDeliveryMainDo(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDelivery.txt");
            if (file.exists())
                file.delete();
        } catch (Exception e) {
            Log.e(TAG, "saveActiveDeliveryMainDo() : " + e.getMessage());
        }
    }

    public ActiveDeliveryMainDO getActiveDeliveryMainDo(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ActiveDelivery.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) is.readObject();
                is.close();
                fis.close();
                return activeDeliveryMainDO;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getActiveDeliveryMainDo() : " + e.getMessage());
            return new ActiveDeliveryMainDO();
        }
        return new ActiveDeliveryMainDO();
    }

    public void saveShipmentListData(Activity activity, ArrayList<PickUpDo> pickUpDos) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ShipmentListData.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(pickUpDos);
            os.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "saveShipmentListData() : " + e.getMessage());
        }
    }

    public void deleteShipmentListData(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ShipmentListData.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteShipmentListData() : " + e.getMessage());
        }
    }

    public ArrayList<PickUpDo> getShipmentListData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ShipmentListData.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<PickUpDo> pickUpDos = (ArrayList<PickUpDo>) is.readObject();
                is.close();
                fis.close();
                return pickUpDos;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getShipmentListData() : " + e.getMessage());
            return new ArrayList<PickUpDo>();
        }
        return new ArrayList<PickUpDo>();
    }

    public ArrayList<String> getSkipShipmentList(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "SkipShipmentList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<String> skipShipmentList = (ArrayList<String>) is.readObject();
                is.close();
                fis.close();
                return skipShipmentList;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getSkipShipmentList() : " + e.getMessage());
            return new ArrayList<String>();
        }
        return new ArrayList<String>();
    }

    public void saveSkipShipmentList(Activity activity, ArrayList<String> skipShipmentList) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SkipShipmentList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(skipShipmentList);
            os.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "saveSkipShipmentList() : " + e.getMessage());
        }
    }

    public void deleteSkipShipmentList(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SkipShipmentList.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteSkipShipmentList() : " + e.getMessage());
        }
    }

    public void saveVanScheduleProducts(Activity activity, ArrayList<LoadStockDO> loadStockDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "VanScheduleProducts.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(loadStockDOS);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveVanScheduleProducts() : " + e.getMessage());
        }
    }

    public void deleteVanScheduleProducts(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "VanScheduleProducts.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteVanScheduleProducts() : " + e.getMessage());
        }
    }



    public void saveVanNonScheduleProducts(Activity activity, ArrayList<LoadStockDO> loadStockDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "VanNonScheduleProducts.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(loadStockDOS);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveVanNonScheduleProducts() : " + e.getMessage());
        }
    }

    public void deleteVanNonScheduleProducts(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "VanNonScheduleProducts.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteVanNonScheduleProducts() : " + e.getMessage());
        }
    }

    public ArrayList<LoadStockDO> getVanNonScheduleProducts(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VanNonScheduleProducts.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<LoadStockDO> loadStockDOS = (ArrayList<LoadStockDO>) is.readObject();
                is.close();
                fis.close();
                return loadStockDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getVanNonScheduleProducts() : " + e.getMessage());
            return new ArrayList<LoadStockDO>();
        }
        return new ArrayList<LoadStockDO>();
    }

    public long saveScheduledVehicleStockInLocal(@NotNull ArrayList<LoadStockDO> loadStockDOS) {
        SQLiteDatabase database = null;
        long insertedRecords = 0;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            for (int i = 0; i < loadStockDOS.size(); i++) {
                LoadStockDO loadStockDo = loadStockDOS.get(i);
                ContentValues values = new ContentValues();
                values.put(LoadVehicleStockTable.COLUMN_ITEM_ID, loadStockDo.product);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_DESC, loadStockDo.productDescription);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY, loadStockDo.quantity);
                values.put(LoadVehicleStockTable.STOCK_UNIT, loadStockDo.stockUnit);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_WEIGHT, loadStockDo.productWeight);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_MASS, loadStockDo.stockUnit);
                values.put(LoadVehicleStockTable.COLUMN_USER_ID, "");
                values.put(LoadVehicleStockTable.COLUMN_STATUS, 0);
                values.put(LoadVehicleStockTable.COLUMN_OTHER, "ScheduledVehicleStockInLocal");
                values.put(LoadVehicleStockTable.SHIPMENT_NUMBER, loadStockDo.shipmentNumber);
                insertedRecords = +database.insert(LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST, null, values);
            }
            return insertedRecords;
        } catch (Exception e) {
            Log.e(TAG, "saveScheduledVehicleStockInLocal() : " + e.getMessage());
        } finally {
            closeDatabase(database);
        }
        return 0;
    }

    public long saveNonScheduledVehicleStockInLocal(@NotNull ArrayList<LoadStockDO> loadStockDOS) {
        SQLiteDatabase database = null;
        long insertedRecords = 0;
        try {
            database = sDatabaseHelper.getWritableDatabase();

//            String query = "insert into "+LoadVehicleStockTable.TABLE_NON_SCHEDULE_PRODUCTS_LIST+" ("+LoadVehicleStockTable.COLUMN_ITEM_ID+","+LoadVehicleStockTable.COLUMN_ITEM_DESC+","
//                    +LoadVehicleStockTable.COLUMN_ITEM_QUANTITY+","+LoadVehicleStockTable.COLUMN_ITEM_WEIGHT+","+LoadVehicleStockTable.COLUMN_ITEM_MASS+","+LoadVehicleStockTable.COLUMN_USER_ID+","
//                    +LoadVehicleStockTable.COLUMN_STATUS+","+LoadVehicleStockTable.COLUMN_OTHER+") values "+"(?,?,?,?,?,?,?,?)";

            for (int i = 0; i < loadStockDOS.size(); i++) {
                LoadStockDO loadStockDo = loadStockDOS.get(i);
                ContentValues values = new ContentValues();
                values.put(LoadVehicleStockTable.COLUMN_ITEM_ID, loadStockDo.product);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_DESC, loadStockDo.productDescription);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY, loadStockDo.quantity);
                values.put(LoadVehicleStockTable.STOCK_UNIT, loadStockDo.stockUnit);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_WEIGHT, loadStockDo.productWeight);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_MASS, loadStockDo.stockUnit);
                values.put(LoadVehicleStockTable.COLUMN_USER_ID, "");
                values.put(LoadVehicleStockTable.COLUMN_STATUS, 0);
                values.put(LoadVehicleStockTable.COLUMN_OTHER, "NonScheduledVehicleStockInLocal");
                values.put(LoadVehicleStockTable.SHIPMENT_NUMBER, loadStockDo.shipmentNumber);
                insertedRecords = database.insert(LoadVehicleStockTable.TABLE_NON_SCHEDULE_PRODUCTS_LIST, null, values);
            }
            return insertedRecords;
        } catch (Exception e) {
            Log.e(TAG, "saveNonScheduledVehicleStockInLocal() : " + e.getMessage());
        } finally {
            closeDatabase(database);
        }
        return 0;
    }

    public ArrayList<LoadStockDO> getVehicleScheduleData() {
        ArrayList<LoadStockDO> loadStockDOS = null;
        SQLiteDatabase database = null;
        String query = "select *from " + LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST + " where " + LoadVehicleStockTable.COLUMN_ITEM_QUANTITY + " >= 0";
        try {
            database = sDatabaseHelper.getWritableDatabase();
            Cursor cursor = database.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                loadStockDOS = new ArrayList<>();
                do {
                    LoadStockDO loadStockDO = new LoadStockDO();
//                    loadStockDO.product = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ID));
//                    loadStockDO.userId = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_USER_ID));
                    loadStockDO.product = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_ID));
                    loadStockDO.productDescription = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_DESC));
                    loadStockDO.quantity = cursor.getInt(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY));
                    loadStockDO.stockUnit = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.STOCK_UNIT));
                    loadStockDO.productWeight = cursor.getInt(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_WEIGHT));
                    loadStockDO.productVolume = cursor.getInt(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_MASS));
                    loadStockDO.status = cursor.getInt(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_STATUS));
                    loadStockDO.other = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_OTHER));
                    loadStockDO.shipmentNumber = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.SHIPMENT_NUMBER));
                    loadStockDOS.add(loadStockDO);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getVehicleScheduleData() : " + e.getMessage());
        }
        return loadStockDOS;
    }

    public long updateTheProductsInLocal(ArrayList<ActiveDeliveryDO> activeDeliveryDos) {
        SQLiteDatabase database = null;
        long updatedRows = 0;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            SQLiteStatement updateProducts = null;
            String updateScheduleProductsQuery = "UPDATE " + LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST + " SET " + LoadVehicleStockTable.COLUMN_ITEM_QUANTITY + "=? WHERE " + LoadVehicleStockTable.COLUMN_ITEM_ID + "=? AND " + LoadVehicleStockTable.SHIPMENT_NUMBER + "=?";
            String updateNonScheduleProductsQuery = "UPDATE " + LoadVehicleStockTable.TABLE_NON_SCHEDULE_PRODUCTS_LIST + " SET " + LoadVehicleStockTable.COLUMN_ITEM_QUANTITY + "=? WHERE " + LoadVehicleStockTable.COLUMN_ITEM_ID + "=?";
            if (activeDeliveryDos != null && activeDeliveryDos.size() > 0) {
                for (int i = 0; i < activeDeliveryDos.size(); i++) {
                    ActiveDeliveryDO activeDeliveryDO = activeDeliveryDos.get(i);
                    int quantity = 0;
                    if (activeDeliveryDO.isScheduled) {
                        quantity = getProductQuantity(database, LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST, activeDeliveryDO.product, activeDeliveryDO.shipmentId) - activeDeliveryDO.orderedQuantity;
                        updateProducts = database.compileStatement(updateScheduleProductsQuery);
                        updateProducts.bindLong(1, quantity);
                        updateProducts.bindString(2, activeDeliveryDO.product);
                        updateProducts.bindString(3, activeDeliveryDO.shipmentId);
                        updatedRows = updatedRows + updateProducts.executeUpdateDelete();
                    } else {
                        quantity = getProductQuantity(database, LoadVehicleStockTable.TABLE_NON_SCHEDULE_PRODUCTS_LIST, activeDeliveryDO.product, "") - activeDeliveryDO.orderedQuantity;
                        updateProducts = database.compileStatement(updateNonScheduleProductsQuery);
                        updateProducts.bindLong(1, quantity);
                        updateProducts.bindString(2, activeDeliveryDO.product);
//                        updateProducts.bindString(3, activeDeliveryDO.shipmentId);
                        updatedRows = updatedRows + updateProducts.executeUpdateDelete();
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "updateTheProductsInLocal() : " + e.getMessage());
        } finally {
            closeDatabase(database);
        }
        return updatedRows;
    }

    private int getProductQuantity(SQLiteDatabase database, String tableName, String productId, String shipmentNumber) {
        int quantity = 0;
        String query = "";
        if (shipmentNumber.equalsIgnoreCase("")) {
            query = "select " + LoadVehicleStockTable.COLUMN_ITEM_QUANTITY + " from " + tableName + " where " + LoadVehicleStockTable.COLUMN_ITEM_ID + "='" + productId + "'";
        } else {
            query = "select " + LoadVehicleStockTable.COLUMN_ITEM_QUANTITY + " from " + tableName + " where " + LoadVehicleStockTable.COLUMN_ITEM_ID + "='" + productId + "' AND " + LoadVehicleStockTable.SHIPMENT_NUMBER + " = '" + shipmentNumber + "'";
        }
        try {
            Cursor cursor = database.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                quantity = cursor.getInt(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY));
            }
        } catch (Exception e) {
            Log.e(TAG, "getProductQuantity() : " + e.getMessage());
        }
        return quantity;
    }

    public void saveSpotSalesCustomerList(Activity activity, ArrayList<CustomerDo> customerDos) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SpotSalesCustomerList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(customerDos);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveSpotSalesCustomerList() : " + e.getMessage());
        }

    }

    public void deleteSpotSalesCustomerList(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SpotSalesCustomerList.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteSpotSalesCustomerList() : " + e.getMessage());
        }

    }

    public ArrayList<CustomerDo> getSpotSalesCustomerList(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "SpotSalesCustomerList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<CustomerDo> customerDos = (ArrayList<CustomerDo>) is.readObject();
                is.close();
                fis.close();
                return customerDos;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getSpotSalesCustomerList() : " + e.getMessage());
            return new ArrayList<CustomerDo>();
        }
        return new ArrayList<CustomerDo>();
    }

    public void saveConfigurationDetails(Activity activity, ConfigurationDo customerDo) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ConfigurationDetails.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(customerDo);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveConfigurationDetails() : " + e.getMessage());
        }

    }

    public ConfigurationDo getConfigurationDetails(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ConfigurationDetails.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ConfigurationDo configurationDo = (ConfigurationDo) is.readObject();
                is.close();
                fis.close();
                return configurationDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getConfigurationDetails() : " + e.getMessage());
            return new ConfigurationDo();
        }
        return new ConfigurationDo();
    }

    public long saveAllReturnStockValue(ArrayList<LoanReturnDO> loanReturnDOS) {
        SQLiteDatabase database = null;
        long insertedRecords = 0;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            for (int i = 0; i < loanReturnDOS.size(); i++) {
                LoanReturnDO loanReturnDO = loanReturnDOS.get(i);
                ContentValues values = new ContentValues();
                values.put(LoadVehicleStockTable.COLUMN_ITEM_ID, loanReturnDO.productName);
                values.put(LoadVehicleStockTable.SHIPMENT_NUMBER, loanReturnDO.shipmentNumber);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_DESC, loanReturnDO.productDescription);
                values.put(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY, loanReturnDO.qty);
                values.put(LoadVehicleStockTable.STOCK_UNIT, loanReturnDO.unit);
                insertedRecords = +database.insert(LoadVehicleStockTable.TABLE_RETURN_PRODUCTS_LIST, null, values);
            }
            return insertedRecords;
        } catch (Exception e) {
            Log.e(TAG, "saveAllReturnStockValue() : " + e.getMessage());
        } finally {
            closeDatabase(database);
        }
        return 0;
    }

    public ArrayList<LoanReturnDO> getAllReturnStockValue() {
        ArrayList<LoanReturnDO> loanReturnDOS = new ArrayList<>();
        SQLiteDatabase database = null;
        String query = "select *from " + LoadVehicleStockTable.TABLE_RETURN_PRODUCTS_LIST;
        try {
            database = sDatabaseHelper.getWritableDatabase();
            Cursor cursor = database.rawQuery(query, null);
            if (cursor != null && cursor.moveToFirst()) {
                loanReturnDOS = new ArrayList<>();
                do {
                    LoanReturnDO loanReturnDO = new LoanReturnDO();
                    loanReturnDO.productName = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_ID));
                    loanReturnDO.productDescription = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_DESC));
                    loanReturnDO.qty = cursor.getInt(cursor.getColumnIndex(LoadVehicleStockTable.COLUMN_ITEM_QUANTITY));
                    loanReturnDO.unit = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.STOCK_UNIT));
                    loanReturnDO.shipmentNumber = cursor.getString(cursor.getColumnIndex(LoadVehicleStockTable.SHIPMENT_NUMBER));
                    loanReturnDOS.add(loanReturnDO);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, "getAllReturnStockValue() : " + e.getMessage());
        }
        return loanReturnDOS;
    }

    public boolean saveCurrentSpotSalesCustomer(Activity activity, CustomerDo customerDo) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "CurrentSpotSalesCustomer.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(customerDo);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "saveCurrentSpotSalesCustomer() : " + e.getMessage());
            return false;
        }
    }

    public CustomerDo getCurrentSpotSalesCustomer(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CurrentSpotSalesCustomer.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                CustomerDo customerDo = (CustomerDo) is.readObject();
                is.close();
                fis.close();
                return customerDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getCurrentSpotSalesCustomer() : " + e.getMessage());
            return new CustomerDo();
        }
        return new CustomerDo();
    }

    public void deleteCurrentSpotSalesCustomer(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CurrentSpotSalesCustomer.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteCurrentSpotSalesCustomer() : " + e.getMessage());
        }
    }

    public void saveVehicleInspectionList(@Nullable Activity activity, @Nullable ArrayList<InspectionDO> inspectionDoS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "VehicleInspectionList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(inspectionDoS);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveVehicleInspectionList() : " + e.getMessage());
        }
    }

    public void deleteVehicleInspectionList(@Nullable Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "VehicleInspectionList.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteVehicleInspectionList() : " + e.getMessage());
        }
    }

    public ArrayList<InspectionDO> getVehicleInspectionList(@Nullable Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "VehicleInspectionList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<InspectionDO> inspectionDOS = (ArrayList<InspectionDO>) is.readObject();
                is.close();
                fis.close();
                return inspectionDOS;
            }
        } catch (Exception e) {
            Log.e(TAG, "getVehicleInspectionList() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public void saveEquipmentSelectionDO(@Nullable Activity activity, @Nullable ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "EquipmentSelectionDO.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(trailerSelectionDOS);
            os.close();
            fos.close();
        } catch (Exception e) {
        }
    }

    public void deleteEquipmentSelectionDO(@Nullable Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "EquipmentSelectionDO.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
        }
    }

    public ArrayList<TrailerSelectionDO> getEquipmentSelectionDO(@Nullable Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "EquipmentSelectionDO.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<TrailerSelectionDO> trailerSelectionDOS = (ArrayList<TrailerSelectionDO>) is.readObject();
                is.close();
                fis.close();
                return trailerSelectionDOS;
            }
        } catch (Exception e) {
            Log.e(TAG, "getVehicleInspectionList() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }


    public void saveSerialEquipmentSelectionDO(@Nullable Activity activity, @Nullable ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SERIALEquipmentSelectionDO.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(trailerSelectionDOS);
            os.close();
            fos.close();
        } catch (Exception e) {
        }
    }

    public void deleteSerialEquipmentSelectionDO(@Nullable Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "SERIALEquipmentSelectionDO.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
        }
    }

    public ArrayList<TrailerSelectionDO> getSerialEquipmentSelectionDO(@Nullable Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "SERIALEquipmentSelectionDO.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<TrailerSelectionDO> trailerSelectionDOS = (ArrayList<TrailerSelectionDO>) is.readObject();
                is.close();
                fis.close();
                return trailerSelectionDOS;
            }
        } catch (Exception e) {
            Log.e(TAG, "getVehicleInspectionList() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }
    public void saveTrailerSelectionDO(@Nullable Activity activity, @Nullable ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "TrailerSelectionDO.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(trailerSelectionDOS);
            os.close();
            fos.close();
        } catch (Exception e) {
        }
    }

    public void deleteTrailerSelectionDO(@Nullable Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "TrailerSelectionDO.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
        }
    }

    public ArrayList<TrailerSelectionDO> getTrailerSelectionDO(@Nullable Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "TrailerSelectionDO.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<TrailerSelectionDO> trailerSelectionDOS = (ArrayList<TrailerSelectionDO>) is.readObject();
                is.close();
                fis.close();
                return trailerSelectionDOS;
            }
        } catch (Exception e) {
            Log.e(TAG, "getVehicleInspectionList() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }



    public void saveGateInspectionList(@Nullable Activity activity, @Nullable ArrayList<InspectionDO> inspectionDoS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "GateInspectionList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(inspectionDoS);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveGateInspectionList() : " + e.getMessage());
        }
    }

    public void deleteGateInspectionList(@Nullable Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "GateInspectionList.txt");
            if (file != null && file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteGateInspectionList() : " + e.getMessage());
        }
    }

    public ArrayList<InspectionDO> getGateInspectionList(@Nullable Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "GateInspectionList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<InspectionDO> inspectionDOS = (ArrayList<InspectionDO>) is.readObject();
                is.close();
                fis.close();
                return inspectionDOS;
            }
        } catch (Exception e) {
            Log.e(TAG, "getGateInspectionList() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public void deleteScheduledNonScheduledReturnData() {//db.delete(TABLE_NAME, null, null);
        SQLiteDatabase database = null;
        try {
            String query1 = "DELETE FROM  " + LoadVehicleStockTable.TABLE_SCHEDULE_PRODUCTS_LIST;
            String query2 = "DELETE FROM  " + LoadVehicleStockTable.TABLE_NON_SCHEDULE_PRODUCTS_LIST;
            String query3 = "DELETE FROM  " + LoadVehicleStockTable.TABLE_RETURN_PRODUCTS_LIST;
            database = sDatabaseHelper.getWritableDatabase();
            database.execSQL(query1);
            database.execSQL(query2);
            database.execSQL(query3);
            Log.e(TAG, "deleteScheduledNonScheduledReturnData() : Deleted all the tables ");
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "deleteScheduledNonScheduledReturnData() : " + e.getMessage());
        }
    }

    public boolean saveReturnCylinders(Activity activity, ArrayList<LoanReturnDO> loanReturnDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ReturnCylinders.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(loanReturnDOS);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "saveReturnCylinders() : " + e.getMessage());
            return false;
        }
    }

    public ArrayList<LoanReturnDO> getReturnCylinders(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ReturnCylinders.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<LoanReturnDO> loanReturnDOS = (ArrayList<LoanReturnDO>) is.readObject();
                is.close();
                fis.close();
                return loanReturnDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getReturnCylinders() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public void deleteReturnCylinders(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "ReturnCylinders.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteReturnCylinders() : " + e.getMessage());
        }
    }

    public boolean saveCurrentDeliveryItems(Activity activity, ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "CurrentDeliveryItems.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(activeDeliveryDOS);
            os.close();
            fos.close();
            return true;
        } catch (Exception e) {
            Log.e(TAG, "saveCurrentDeliveryItems() : " + e.getMessage());
            return false;
        }
    }

    public ArrayList<ActiveDeliveryDO> getCurrentDeliveryItems(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CurrentDeliveryItems.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<ActiveDeliveryDO> activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) is.readObject();
                is.close();
                fis.close();
                return activeDeliveryDOS;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getCurrentDeliveryItems() : " + e.getMessage());
            return new ArrayList<>();
        }
        return new ArrayList<>();
    }

    public void deleteCurrentDeliveryItems(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "CurrentDeliveryItems.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteCurrentDeliveryItems() : " + e.getMessage());
        }
    }


    public void saveDamageDeliveryItems(Activity activity, ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDeliveryList.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(activeDeliveryDOS);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveDamageDeliveryItems() : " + e.getMessage());
        }
    }

    public ArrayList<ActiveDeliveryDO> getDamageDeliveryItems(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDeliveryList.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ArrayList<ActiveDeliveryDO> activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) is.readObject();
                is.close();
                fis.close();
                return activeDeliveryDOS;
            }
        } catch (Exception e) {
            Log.e(TAG, "getDamageDeliveryItems() : " + e.getMessage());
            return new ArrayList<ActiveDeliveryDO>();
        }
        return new ArrayList<ActiveDeliveryDO>();
    }

    public void deleteDamageDeliveryItems(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "ActiveDeliveryList.txt");
            if (file.exists())
                file.delete();
        } catch (Exception e) {
            Log.e(TAG, "deleteDamageDeliveryItems() : " + e.getMessage());
        }
    }

    public ActiveDeliveryMainDO getDamageDeliveryMainDo(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "DamageDelivery.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                ActiveDeliveryMainDO activeDeliveryMainDO = (ActiveDeliveryMainDO) is.readObject();
                is.close();
                fis.close();
                return activeDeliveryMainDO;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getDamageDeliveryMainDo() : " + e.getMessage());
            return new ActiveDeliveryMainDO();
        }
        return new ActiveDeliveryMainDO();
    }

    public void savePrinterMac(Activity activity, String macAddress) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "PB51PrinterMac.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(macAddress);
            os.close();
            fos.close();

        } catch (Exception e) {
            Log.e(TAG, "savePrinterMac() : " + e.getMessage());
        }
    }

    public void deletePrinterMac(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "PB51PrinterMac.txt");
            if (file.exists())
                file.delete();
        } catch (Exception e) {
            Log.e(TAG, "deletePrinterMac() : " + e.getMessage());
        }
    }

    public String getPrinterMac(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "PB51PrinterMac.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                String macAddress = (String) is.readObject();
                is.close();
                fis.close();
                return macAddress;
            }
        } catch (Exception e) {
            Log.e(TAG, "getPrinterMac() : " + e.getMessage());
            return "";
        }
        return "";
    }
    public void saveUpdateData(Activity activity, UpdateDocumentDO podDo) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "updateDocument.txt");
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(podDo);
            os.close();
            fos.close();
        } catch (Exception e) {
            Log.e(TAG, "saveDepartureData() : " + e.getMessage());
        }
    }

    public void deleteUpdateData(Activity activity) {
        try {
            File file = new File(activity.getApplication().getFilesDir().toString() + "/", "updateDocument.txt");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            Log.e(TAG, "deleteDepartureData() : " + e.getMessage());
        }
    }

    public UpdateDocumentDO getUpdateData(Context context) {
        try {
            File file = new File(((Activity) context).getApplication().getFilesDir().toString() + "/", "updateDocument.txt");
            if (file.exists()) {
                FileInputStream fis = new FileInputStream(file);
                ObjectInputStream is = new ObjectInputStream(fis);
                UpdateDocumentDO podDo = (UpdateDocumentDO) is.readObject();
                is.close();
                fis.close();
                return podDo;
            }
        } catch (Exception e) {
            Log.e("StorageManager", "getDepartureData() : " + e.getMessage());
            return new UpdateDocumentDO();
        }
        return new UpdateDocumentDO();
    }

}
