package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;


import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CashDO;
import com.tbs.generic.vansales.Model.ChequeDO;
import com.tbs.generic.vansales.Model.CustomerManagementDO;
import com.tbs.generic.vansales.Model.CustomerReturnDO;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.MiscStopDO;
import com.tbs.generic.vansales.Model.PickTicketDO;
import com.tbs.generic.vansales.Model.PurchaseReceiptDO;
import com.tbs.generic.vansales.Model.PurchaseReturnDO;
import com.tbs.generic.vansales.Model.ReportImagesDO;
import com.tbs.generic.vansales.Model.ReportMainDo;
import com.tbs.generic.vansales.Model.ReportQuestionsDO;
import com.tbs.generic.vansales.Model.SalesInvoiceDO;
import com.tbs.generic.vansales.Model.UserDeliveryDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;


public class ReportRequest extends AsyncTask<String, Void, Boolean> {
    private Context mContext;
    ReportMainDo reportMainDo;
    ReportImagesDO reportImagesDO;
    ReportQuestionsDO reportQuestionsDO;

    String contractNumber, preparationNumber;
    int type, lineNumber;

    public ReportRequest(Context mContext,
                         int type,
                         String contract,
                         String preparation,
                         int line) {
        this.mContext = mContext;
        this.contractNumber = contract;
        this.preparationNumber = preparation;
        this.lineNumber = line;
        this.type = type;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, ReportMainDo reportMainDo);
    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YDELNO", contractNumber);
            jsonObject.put("I_YLINNO", lineNumber);
            jsonObject.put("I_YSERNUM", preparationNumber);


        } catch (Exception e) {
            return false;
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.INSPECTION_REPORT, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressTask.getInstance().showProgress(mContext, false, "");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, reportMainDo);
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            reportMainDo = new ReportMainDo();


            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    attribute = xpp.getAttributeValue(null, "ID");
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP3")) {
                        ((BaseActivity)mContext). preferenceUtils.saveString(PreferenceUtils.GRP, "GRP3");
                        reportMainDo.questionList = new ArrayList<>();
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP4")) {
                        reportMainDo.imagesList = new ArrayList<>();
                        ((BaseActivity)mContext).  preferenceUtils.saveString(PreferenceUtils.GRP, "GRP4");
                    }else if (startTag.equalsIgnoreCase("LIN") && ((BaseActivity)mContext).
                            preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        reportQuestionsDO = new ReportQuestionsDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && ((BaseActivity)mContext).
                            preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP4")) {
                        reportImagesDO = new ReportImagesDO();
                    }
//
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XCOMMENTS")) {
                            if (text.length() > 0) {

                                reportMainDo.mainComment = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XSIGN")) {
                            if (text.length() > 0) {

                                reportMainDo.signature = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCONBY")) {
                            if (text.length() > 0) {

                                reportMainDo.conductedBy = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YITMREF")) {
                            if (text.length() > 0) {

                                reportMainDo.product = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YITMREF")) {
                            if (text.length() > 0) {

                                reportMainDo.productDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XIMG")) {
                            if (text.length() > 0) {

                                reportImagesDO.image = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YADDNOTES")) {
                            if (text.length() > 0) {

                                reportQuestionsDO.comment = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YQSTNCOD")) {
                            if (text.length() > 0) {

                                reportQuestionsDO.inspectionItem = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YRESPONSE")) {
                            if (text.length() > 0) {

                                reportQuestionsDO.reponse = text;
                            }

                        }  text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    } else if (endTag.equalsIgnoreCase("LIN") &&
                            ((BaseActivity)mContext). preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        reportMainDo.questionList.add(reportQuestionsDO);
                    } else if (endTag.equalsIgnoreCase("LIN")
                            &&    ((BaseActivity)mContext).preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP4")) {
                        reportMainDo.imagesList.add(reportImagesDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }

}