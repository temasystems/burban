package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.UnPaidInvoiceDO;
import com.tbs.generic.vansales.Model.UnPaidInvoiceMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class UnPaidInvoicesListRequest extends AsyncTask<String, Void, Boolean> {

    private UnPaidInvoiceMainDO unPaidInvoiceMainDO;
    private UnPaidInvoiceDO unPaidInvoiceDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String customer;


    public UnPaidInvoicesListRequest(String customer, Context mContext) {

        this.mContext = mContext;
        this.customer = customer;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, UnPaidInvoiceMainDO unPaidInvoiceMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_YBPC", customer);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.UNPAID_INVOICES, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("Unpaid xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            unPaidInvoiceMainDO = new UnPaidInvoiceMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        unPaidInvoiceMainDO.unPaidInvoiceDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        unPaidInvoiceDO = new UnPaidInvoiceDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YBPC")) {
                            unPaidInvoiceMainDO.customer = text;


                        } else if (attribute.equalsIgnoreCase("O_YOTOTAMT")) {
                            if (text.length() > 0) {

                                unPaidInvoiceMainDO.totalAmount = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPAYTYP")) {
                            if (text.length() > 0) {

                                unPaidInvoiceMainDO.type = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPCDES")) {
                            unPaidInvoiceDO.customerId = text;

                        } else if (attribute.equalsIgnoreCase("O_YSIH")) {
                            unPaidInvoiceDO.invoiceId = text;

                        } else if (attribute.equalsIgnoreCase("O_YDATE")) {
                            unPaidInvoiceDO.accountingDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YAMT")) {
                            unPaidInvoiceDO.amount = Double.valueOf(text);

                        } else if (attribute.equalsIgnoreCase("O_YDUDAYS")) {
                            unPaidInvoiceDO.dueDays = Integer.parseInt(text);

                        } else if (attribute.equalsIgnoreCase("O_YOAMT")) {
                            unPaidInvoiceDO.outstandingAmount = Double.parseDouble(text);

                        } else if (attribute.equalsIgnoreCase("O_YOCUR")) {
                            unPaidInvoiceDO.outstandingAmountCurrency = text;

                        } else if (attribute.equalsIgnoreCase("O_YPAMT")) {
                            unPaidInvoiceDO.paidAmount = Double.parseDouble(text);

                        } else if (attribute.equalsIgnoreCase("O_YPACUR")) {
                            unPaidInvoiceDO.paidAmountCurrency = text;

                        } else if (attribute.equalsIgnoreCase("O_YFCY")) {
                            unPaidInvoiceDO.site = text;

                        } else if (attribute.equalsIgnoreCase("O_YFCYDES")) {
                            unPaidInvoiceDO.siteId = text;

                        } else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            unPaidInvoiceDO.currency = text;

                        }

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        unPaidInvoiceMainDO.unPaidInvoiceDOS.add(unPaidInvoiceDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if (xpp.getText().length() > 0) {

                        text = xpp.getText();
                    } else {
                        text = "";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, unPaidInvoiceMainDO);
        }
        ((BaseActivity) mContext).hideLoader();

    }
}