package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class SpotPickUpDo implements Serializable {


    public String receiptNumber = "";

    public String customer = "";
    public String description = "";
    public String address = "";

    public String site = "";
    public Double lattitude = 0.0;
    public Double longitude = 0.0;
    public Double distance = 0.0;
    public Integer distanceMain = 0;

}
