package com.tbs.generic.vansales.utils;

import android.app.Application;

public class CustomFont extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        TypefaceUtil.overrideFont(getApplicationContext(), "serif", "fonts/montserrat_regular.ttf");
    }
}