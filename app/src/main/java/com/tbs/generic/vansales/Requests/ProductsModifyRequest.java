package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.CustomXmlPullParser;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;


public class ProductsModifyRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;
    private String shipmentID;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    private int workFlag;

    public ProductsModifyRequest(int flag,String shipment, ArrayList<ActiveDeliveryDO> activeDeliveryDoS, Context mContext) {

        this.mContext = mContext;

        this.activeDeliveryDOS = activeDeliveryDoS;
        this.shipmentID = shipment;
        this.workFlag=flag;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO successDO,String msg);

    }

    HttpURLConnection urlConnection;

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customer);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "modify";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String http = preferenceUtils.getStringFromPreference(PreferenceUtils.HTTP, "");
//        String URL = http + ip + ServiceURLS.COLON + port + ServiceURLS.APP_SUB_URL;
        String URL = http + ip + ServiceURLS.COLON + port + ServiceURLS.APP_SUB_URL;
//        String URL = http + ip + ServiceURLS.APP_SUB_URL; //https

        String loc = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");


        String inputXMLObject = "";
        if (activeDeliveryDOS != null && activeDeliveryDOS.size() > 0) {
            inputXMLObject = "<TAB DIM=\"300\" ID=\"SDH1_4\" SIZE=\"" + activeDeliveryDOS.size() + "\">\n";
            for (int i = 0; i < activeDeliveryDOS.size(); i++) {

                if(activeDeliveryDOS.get(i).location.length()>0){
                    inputXMLObject += "<LIN NUM=\"" + (i + 1) + "\">\n" +
                            "  <FLD NAME=\"ITMREF\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).product + "</FLD>\n" +
                            "  <FLD NAME=\"QTY\" TYPE=\"Decimal\">" + activeDeliveryDOS.get(i).orderedQuantity + "</FLD>\n" +
                            "  <FLD NAME=\"LOC\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).location+ "</FLD>\n" +
                            "  <FLD NAME=\"YORGNLQTY\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).totalQuantity+ "</FLD>\n" +

//                            "  <FLD NAME=\"XX10C_CMTSR\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).openingQuantity + "</FLD>\n" +
//                            "  <FLD NAME=\"XX10C_OMTSR\" TYPE=\"Decimal\">" + activeDeliveryDOS.get(i).endingQuantity + "</FLD>\n" +
//                            "  <FLD NAME=\"YORGNLMTR\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).totalQuantity+ "</FLD>\n" +
                            "</LIN>\n";
                }else {
                    inputXMLObject += "<LIN NUM=\"" + (i + 1) + "\">\n" +
                            "  <FLD NAME=\"ITMREF\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).product + "</FLD>\n" +
                            "  <FLD NAME=\"QTY\" TYPE=\"Decimal\">" + activeDeliveryDOS.get(i).orderedQuantity + "</FLD>\n" +
                            "  <FLD NAME=\"LOC\" TYPE=\"Char\">" + loc+ "</FLD>\n" +
                            "  <FLD NAME=\"YORGNLQTY\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).totalQuantity+ "</FLD>\n" +

//                            "  <FLD NAME=\"XX10C_CMTSR\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).openingQuantity + "</FLD>\n" +
//                            "  <FLD NAME=\"XX10C_OMTSR\" TYPE=\"Decimal\">" + activeDeliveryDOS.get(i).endingQuantity + "</FLD>\n" +
//                            "  <FLD NAME=\"YORGNLMTR\" TYPE=\"Char\">" + activeDeliveryDOS.get(i).totalQuantity+ "</FLD>\n" +
                            "</LIN>\n";
                }



            }

        }

        String xmlContentPrefix = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wss=\"http://www.adonix.com/WSS\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <wss:modify soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
                "         <callContext xsi:type=\"wss:CAdxCallContext\">\n" +
                "            <codeLang xsi:type=\"xsd:string\">"+ AppPrefs.getString(AppPrefs.SERVICE_LANAGUAGE, Constants.ENG)+"</codeLang>\n" +
                "            <poolAlias xsi:type=\"xsd:string\">" + pool + "</poolAlias>\n" +
                "            <poolId xsi:type=\"xsd:string\"></poolId>\n" +
                "            <requestConfig xsi:type=\"xsd:string\"></requestConfig>\n" +
                "         </callContext>\n" +
                "         <publicName xsi:type=\"xsd:string\">"+ WebServiceConstants.PRODUCT_MODIFY +"</publicName>\n" +
                "         <objectKeys xsi:type=\"wss:ArrayOfCAdxParamKeyValue\" soapenc:arrayType=\"wss:CAdxParamKeyValue[1]\">\n" +
                "         <keys><key>SDHNUM</key><value>" + shipmentID + "</value></keys>\n" +
                "         </objectKeys>\n" +
                "         <objectXml xsi:type=\"xsd:string\"><![CDATA[<PARAM>\n" +
                "  <FLD NAME=\"XWRKFLAG\" TYPE=\"Integer\">" + workFlag + "</FLD>\n" +
                inputXMLObject +
                "</TAB>\n" +
                "</PARAM>]]></objectXml>\n" +
                "      </wss:modify>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        try {
//            ((BaseActivity)mContext).allowAllSSL();
            java.net.URL url = new URL(URL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "text/plain");
            urlConnection.setRequestProperty("SOAPAction", "CAdxWebServiceXmlCC");
            urlConnection.setRequestProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes()));
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(xmlContentPrefix.getBytes());
            outputStream.flush();
            outputStream.close();
            urlConnection.connect();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            Log.e(getClass().getName(), String.valueOf(urlConnection.getResponseCode()));
            String result = null;
            if (urlConnection.getResponseCode() == 200) {
                BufferedInputStream bis = new BufferedInputStream(urlConnection.getInputStream());
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                int result2 = bis.read();
                while (result2 != -1) {
                    buf.write((byte) result2);
                    result2 = bis.read();
                }
                result = buf.toString();
            }
            //XML parsing is done in a second class called CustomXmlPullParser.
            //Please check that.

            String resultXml = string2SoapObject(result);

            if (resultXml != null && resultXml.length() > 0&&resultXml.contains("FLD")) {
                return parseXML(resultXml);
            } else {
                message=resultXml;

                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
//            message = "Socket Time Out Exception Please try again";

            return false;
        } catch (Exception e) {
            e.printStackTrace();
//            message = " " + e;

            return false;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

    }
    String message;

    public String string2SoapObject(String bytes) {
        String resultXML = null;

        try {

            resultXML = CustomXmlPullParser.parse(bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultXML;
    }

    public boolean parseXML(String xmlString) {
        System.out.println("Approval Request : xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("XWRKFLAG")) {
                            if (text.length() > 0) {

                                successDO.flag = Integer.parseInt(text);
                            }


                        }


                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);


        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO,message);
        }
    }
}