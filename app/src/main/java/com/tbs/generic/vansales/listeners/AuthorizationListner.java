package com.tbs.generic.vansales.listeners;
/*
 * Created by developer on 11/1/18.
 */

/**
 * The interface String listner.
 */
public interface AuthorizationListner {
    /**
     * Gets prase string.
     *
     * @param value the value
     */
    void getType(int value);
}
