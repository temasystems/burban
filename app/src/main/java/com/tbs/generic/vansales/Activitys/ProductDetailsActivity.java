//package com.tbs.generic.vansales.Activitys;
//
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.ScrollView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.tbs.generic.vansales.Model.ProductDetailMainDO;
//import com.tbs.generic.vansales.Model.ProductDetailsDO;
//import com.tbs.generic.vansales.R;
//import com.tbs.generic.vansales.Requests.ProductDetailsRequest;
//import com.tbs.generic.vansales.utils.PreferenceUtils;
//
//import java.util.ArrayList;
//
//public class ProductDetailsActivity extends BaseActivity implements AdapterView.OnItemClickListener {
//    private ScrollView llOrderDetails;
//
//    ArrayList<ProductDetailsDO> productDetailsDOS;
//
//
//    private TextView SalesUnit;
//    private TextView productStatus;
//    private TextView productWeight;
//    private TextView ProductDescription;
//    private TextView Productcategory;
//    private TextView StockUnit;
//    private TextView WeightUnit;
//    private TextView Accountingcode;
//    private TextView Taxlevel;
//    private TextView BasePrice;
//    PreferenceUtils preferenceUtils;
//    String orderCode;
//    ListView listView;
//    // CustomAdapter customAdapter;
//
//    @Override
//    public void initialize() {
//        llOrderDetails = (ScrollView) getLayoutInflater().inflate(R.layout.product_details, null);
//        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        changeLocale();
//        initializeControls();
//        toolbar.setNavigationIcon(R.drawable.back);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        // llshoppingCartLayout.setVisibility(View.GONE);
//
//        if (getIntent().hasExtra("Code")) {
//            orderCode = getIntent().getExtras().getString("Code");
//        }
//        listView = findViewById(R.id.lvItems);
//
//
//        preferenceUtils = new PreferenceUtils(ProductDetailsActivity.this);
//        ProductDetailsRequest driverListRequest = new ProductDetailsRequest(orderCode, ProductDetailsActivity.this);
//        driverListRequest.setOnResultListener(new ProductDetailsRequest.OnResultListener() {
//            @Override
//            public void onCompleted(boolean isError, ProductDetailMainDO productDetailMainDO) {
//                if (isError) {
//                    Toast.makeText(ProductDetailsActivity.this, R.string.error_product_list, Toast.LENGTH_SHORT).show();
//                } else {
//                    // productDetailsDOS = productDetailMainDO.productDetailsDOS;
//
//
//                    Productcategory.setText("" + productDetailMainDO.productName);
//                    ProductDescription.setText("" + productDetailMainDO.productDescription);
//                    productStatus.setText("" + productDetailMainDO.productStatus);
//                    SalesUnit.setText("" + productDetailMainDO.salesUnit);
//                    BasePrice.setText("" + productDetailMainDO.basePrice);
//                    Taxlevel.setText("" + productDetailMainDO.taxLevel);
//                    Accountingcode.setText("" + productDetailMainDO.accountingCode);
//                    StockUnit.setText(" " + productDetailMainDO.stockUnit);
//                    WeightUnit.setText("" + productDetailMainDO.weightUnit);
//                    productWeight.setText(" " + productDetailMainDO.productWeight);
////                    if (customerDos.size() > 0) {
////                        for (int k = 0; k < customerDos.size(); k++) {
////
////
////
//////                    CustomerAdapter driverAdapter = new CustomerAdapter(customerDos);
//////                    recycleview.setAdapter(driverAdapter);
////                        }
////                    }}
////                     customAdapter = new CustomAdapter(ProductDetailsActivity.this, customerDos);
////
////                    // Set a adapter object to the listview
////                    listView.setAdapter(customAdapter);
//
//                }
//
//            }
//        });
//        listView.setOnItemClickListener(this);
//
//        driverListRequest.execute();
//    }
//
////    class CustomAdapter extends BaseAdapter {
////        public Context context;
////        ArrayList<CustomerDetailsDo> listOrderHistoryDetailsDO;
////
////
////        public CustomAdapter(Context context, ArrayList<CustomerDetailsDo> customerDetailsDos) {
////
////            this.context = context;
////            this.listOrderHistoryDetailsDO = customerDetailsDos;
////        }
////
////        @Override
////        public int getCount() {
////            // return the all apps count
////            if (customerDos.size() > 0)
////                return customerDos.size();
////            else
////                return 0;
////        }
////
////        @Override
////        public Object getItem(int position) {
////            // TODO Auto-generated method stub
////            return null;
////        }
////
////        @Override
////        public long getItemId(int position) {
////            // TODO Auto-generated method stub
////            return position;
////        }
////
////        @Override
////        public View getView(int position, View convertView, ViewGroup parent) {
////            View view = getLayoutInflater().inflate(R.layout.details_list, null);
////
////            TextView addressLine        = (TextView) view.findViewById(R.id.addressLine);
////            TextView addressDescription = (TextView) view.findViewById(R.id.addressDescription);
////            TextView postalCode         = (TextView) view.findViewById(R.id.postalCode);
////            TextView city               = (TextView) view.findViewById(R.id.city);
////            TextView telephone          = (TextView) view.findViewById(R.id.telephone);
////            TextView mobile             = (TextView) view.findViewById(R.id.mobile);
////
////            addressLine.setText("" + customerDos.get(position).addressLine);
////            addressDescription.setText("" + customerDos.get(position).addressDescription);
////            postalCode.setText("" + customerDos.get(position).postalCode);
////            city.setText("" + customerDos.get(position).city);
////            telephone.setText("" + customerDos.get(position).telephone);
////            mobile.setText("" + customerDos.get(position).mobile);
////
////            return view;
////        }
//
//    //  }
//
//    @Override
//    public void initializeControls() {
//        tvScreenTitle.setText(getString(R.string.product_details));
//
//
//        Productcategory = findViewById(R.id.pCategory);
//        ProductDescription = findViewById(R.id.pDescription);
//        StockUnit = findViewById(R.id.stockUnit);
//        productWeight = findViewById(R.id.productWeight);
//        productStatus = findViewById(R.id.status);
//        SalesUnit = findViewById(R.id.salesUnit);
//        Accountingcode = findViewById(R.id.accountingCode);
//        BasePrice = findViewById(R.id.basePrice);
//        Taxlevel = findViewById(R.id.taxLevel);
//        WeightUnit = findViewById(R.id.weightUnit);
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//    }
//}
