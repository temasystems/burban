//package com.tbs.generic.vansales.Requests;
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.tbs.generic.vansales.Activitys.BaseActivity;
//import com.tbs.generic.vansales.Model.ProductDO;
//import com.tbs.generic.vansales.utils.AppPrefs;
//import com.tbs.generic.vansales.utils.Constants;
//import com.tbs.generic.vansales.utils.PreferenceUtils;
//import com.tbs.generic.vansales.utils.ServiceURLS;
//import com.tbs.generic.vansales.utils.WebServiceConstants;
//
//import org.ksoap2.HeaderProperty;
//import org.ksoap2.SoapEnvelope;
//import org.ksoap2.serialization.SoapObject;
//import org.ksoap2.serialization.SoapSerializationEnvelope;
//import org.ksoap2.transport.HttpTransportSE;
//import org.xmlpull.v1.XmlPullParser;
//import org.xmlpull.v1.XmlPullParserFactory;
//
//import java.io.StringReader;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by Vijay Dhas on 26/05/16.
// */
//public class ProductListRequest extends AsyncTask<String, Void, Boolean> {
//    List<ProductDO> productDOS;
//    String username,password,ip,pool,port;
//    PreferenceUtils preferenceUtils;
//    Context mContext;
//    public void setOnResultListener(OnResultListener onResultListener) {
//        this.onResultListener = onResultListener;
//    }
//
//    OnResultListener onResultListener;
//
//    public interface OnResultListener {
//        void onCompleted(boolean isError, List<ProductDO> productDOS);
//
//    }
//    public ProductListRequest(Context mContext) {
//
//        this.mContext = mContext;
//    }
//
//
//    public boolean runRequest() {
//        String NAMESPACE = "http://www.adonix.com/WSS";
//        String METHOD_NAME = "query";
//        String SOAP_ACTION = "CAdxWebServiceXmlCC";
////        String URL = "http://183.82.9.23:8124/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
//        preferenceUtils = new PreferenceUtils(mContext);
//        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
//        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
//        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
//        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
//        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
//        String http = preferenceUtils.getStringFromPreference(PreferenceUtils.HTTP, "");
//        String URL = http + ip + ServiceURLS.COLON + port + ServiceURLS.APP_SUB_URL;
//        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
//        // Set all input params
//        request.addProperty("publicName", WebServiceConstants.PRODUCT_LIST);
//        request.addProperty("listSize", 9999);
//
//        SoapObject callcontext = new SoapObject("", "callContext");
//        // Set all input params
//        callcontext.addProperty("codeLang", AppPrefs.getString(AppPrefs.SERVICE_LANAGUAGE, Constants.ENG));
//        callcontext.addProperty("poolAlias", pool);
//        callcontext.addProperty("poolId", "");
//        callcontext.addProperty("codeUser",username);
//        callcontext.addProperty("password", password);
//        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");
//
//        request.addSoapObject(callcontext);
//
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//
//        envelope.setOutputSoapObject(request);
//        ((BaseActivity)mContext).allowAllSSL();
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//        androidHttpTransport.debug = true;
//
//        try {
//            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
//            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));
//
//
//            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
//
//
//            SoapObject response = (SoapObject) envelope.getResponse();
//            String resultXML = (String) response.getProperty("resultXml");
//            if (resultXML != null && resultXML.length() > 0) {
//                return parseXML(resultXML);
//            } else {
//                return false;
//            }
//        } catch (Exception e) {
//            System.out.println("Exception " + e);
//            e.printStackTrace();
//            return false;
//        }
//
//    }
//
//    public boolean parseXML(String xmlString) {
//        System.out.println("xmlString "+xmlString);
//        try {
//            String text = "", attribute = "", startTag = "", endTag = "";
//            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
//            factory.setNamespaceAware(true);
//            XmlPullParser xpp = factory.newPullParser();
//
//            xpp.setInput(new StringReader(xmlString));
//            int eventType = xpp.getEventType();
//            ProductDO productDO = null;
//            productDOS = new ArrayList<>();
//            while (eventType != XmlPullParser.END_DOCUMENT) {
//                if (eventType == XmlPullParser.START_TAG) {
//
////
//                    startTag = xpp.getName();
//                    if (startTag.equalsIgnoreCase("FLD")) {
//                        attribute = xpp.getAttributeValue(null, "NAME");
//                    } else if (startTag.equalsIgnoreCase("LIN")) {
//                        productDO = new ProductDO();
//                    }
//                } else if (eventType == XmlPullParser.END_TAG) {
//                    endTag = xpp.getName();
//                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
//                        if (attribute.equalsIgnoreCase("ITMREF")) {
//                            productDO.productId= text;
//                        } else if (attribute.equalsIgnoreCase("C2")) {
//                            productDO.productName=text;
//                        }
//                        //else if (attribute.equalsIgnoreCase("XDRVID")) {
////                            customer.setcustomerID(text);
////                        }else if (attribute.equalsIgnoreCase("XDRVNAME")) {
////                            customer.setUserName(text);
////                        } else if (attribute.equalsIgnoreCase("XDRVPWD")) {
////                            customer.setPassword(text);
////                        } else if (attribute.equalsIgnoreCase("XDRVMAIL")) {
////                            customer.setEmailID(text);
////                        }
//
//                    }
//                    if (endTag.equalsIgnoreCase("LIN")) {
//                        productDOS.add(productDO);
//                    }
//                } else if (eventType == XmlPullParser.TEXT) {
//                    text = xpp.getText();
//                }
//                eventType = xpp.next();
//            }
//            return true;
//        } catch (Exception e) {
//            System.out.println("Exception " + e);
//
//            return false;
//        }
//    }
//
//    @Override
//    protected Boolean doInBackground(String... param) {
//
//        return runRequest();
//    }
//
//    @Override
//    protected void onPostExecute(Boolean result) {
//        super.onPostExecute(result);
//        if (onResultListener != null) {
//            onResultListener.onCompleted(!result, productDOS);
//        }
//    }
//}