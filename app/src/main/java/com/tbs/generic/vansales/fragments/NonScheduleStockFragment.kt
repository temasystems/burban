package com.tbs.generic.vansales.fragments

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Adapters.StockItemAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoadStockMainDO
import com.tbs.generic.vansales.Model.StockItemDo
import com.tbs.generic.vansales.Model.VanStockDetailsDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.NonScheduledLoadVanSaleRequest
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.fragment_non_schedule_stock.*
import kotlinx.android.synthetic.main.fragment_open_stock.no_data_found
import kotlinx.android.synthetic.main.fragment_open_stock.prgress_bar
import kotlinx.android.synthetic.main.fragment_open_stock.recycleview
import kotlinx.android.synthetic.main.include_stock_caluclute.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [NonScheduleStockFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [NonScheduleStockFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NonScheduleStockFragment : Fragment() {

    private lateinit var preferenceUtils: PreferenceUtils
    private var listener: OnFragmentInteractionListener? = null
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()

    private lateinit var resultListner: ResultListner
    private lateinit var vanStockDetailsDO: VanStockDetailsDO

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment OpenStockFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(vanStockDetailsDO: VanStockDetailsDO, resultListner: ResultListner): NonScheduleStockFragment {
            val nonScheduleStockFragment = NonScheduleStockFragment()
            nonScheduleStockFragment.vanStockDetailsDO = vanStockDetailsDO
            nonScheduleStockFragment.resultListner = resultListner
            return nonScheduleStockFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_non_schedule_stock, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferenceUtils = PreferenceUtils(context)
        try {
            if (vanStockDetailsDO != null) {
                setData()
            } else {
                showHideViews(false, getString(R.string.no_vehicle_data_found))
            }
            // loadNonScheduledVehicleStockData()
        } catch (e: Exception) {
            e.localizedMessage
        }

        //Listners
        btn_schedule_stock.setOnClickListener({
            listener?.onFragmentScheduleStockInteraction()

        })
    }

    private fun setData() {

        calculate()
        val list = getSchedueNonScheduleData(vanStockDetailsDO)
        if (list.size > 0) {
            showHideViews(true, "")
            val stockAdapter = StockItemAdapter(activity, list, "Shipments")
            recycleview.adapter = stockAdapter
        } else {
            showHideViews(false, getString(R.string.no_vehicle_data_found))
        }
    }


    private fun calculate() {
        if (activity != null) {
            tvCapacity.text = "" + vanStockDetailsDO.vehicleCapacity + " " + vanStockDetailsDO.vehicleCapacityUnit
            tvScheduledStock.text = "" + vanStockDetailsDO.totalScheduledStockQty + " " + vanStockDetailsDO.totalScheduledStockUnits
            tvNonScheduledStock.text = "" + vanStockDetailsDO.totalNonScheduledStockQty + " " + vanStockDetailsDO.totalNonScheduledStockUnits

            if (!TextUtils.isEmpty(vanStockDetailsDO.pickUpStock)) {
                llPickUp.visibility = View.VISIBLE
                tvPickUpStock.text = "" + vanStockDetailsDO.pickUpStock + " " + vanStockDetailsDO.vehicleCapacityUnit

            } else {
                llPickUp.visibility = View.GONE
            }
            var scheduleStock = 0.0;

            if (!TextUtils.isEmpty(vanStockDetailsDO.dropStock)) {
                llDrop.visibility = View.VISIBLE
                tvDropStock.text = "" + vanStockDetailsDO.dropStock + " " + vanStockDetailsDO.vehicleCapacityUnit
                scheduleStock = vanStockDetailsDO.dropStock.toDouble()

            } else {
                llDrop.visibility = View.GONE
            }

            var scheduleTotal = 0
            val nonScheduleTotal = vanStockDetailsDO.totalNonScheduledStockQty.toInt()

            //  var nonScheduleStock =loadStockMainDo.totalNonScheduledStock

            var total = 0.0.toBigDecimal()

            total = vanStockDetailsDO.vehicleCapacity.toBigDecimal()
            scheduleTotal = scheduleStock.toInt()
            var baltotal=(total - (scheduleTotal + nonScheduleTotal).toBigDecimal()).toString()
            tvBalanceStock.text = "" + baltotal + " " + vanStockDetailsDO.vehicleCapacityUnit

        }
    }

    /*private fun loadNonScheduledVehicleStockData() {
        if (activity == null) {
            return
        }
        if (Util.isNetworkAvailable(activity)) {
            val nonScheduledRootId = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (!nonScheduledRootId.equals("", true)) {
                val loadVanSaleRequest = NonScheduledLoadVanSaleRequest(nonScheduledRootId, activity)
                Util.showLoader(prgress_bar)
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    Util.hideLoader(prgress_bar)
                    if (isError) {
                        //Util.showToast(activity, "No vehicle data found.")
                        showHideViews(false, "No vehicle data found.")
                    } else {
                        nonScheduleDos = loadStockMainDo.loadStockDOS;

                        if (nonScheduleDos.size > 0) {
                            calculate(loadStockMainDo)
                            showHideViews(true, "")
                            val loadStockAdapter = LoadStockAdapter(activity, nonScheduleDos, "Shipments")
                            recycleview.setAdapter(loadStockAdapter)
//
                        } else {
                            showHideViews(false, "No Data Found")
                        }
                    }
                }
                loadVanSaleRequest.execute()
            } else {
                showHideViews(false, "No vehicle data found.")
            }
        } else {
            Util.showToast(activity, "No Internet connection, please try again later")
        }
    }*/


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }


    /* private fun calculate(loadStockMainDo: LoadStockMainDO) {
         try {
             llScheduledStock.visibility = View.GONE
             var unit = preferenceUtils.getStringFromPreference(PreferenceUtils.UNIT_CAPACITY, "")
             tvCapacity.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "" + " " + unit))

             //  tvNonScheduledStock.setText("Non Scheduled Stock : "+loadStockMainDo.totalNonScheduledStock +" "+loadStockMainDo.vehicleCapacityUnit)
             var scheduleTotal = 0;
             var nonScheduleTotal = 0;

             var scheduleStock = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_SCHEDULED_STOCK, "")
             var nonScheduleStock = loadStockMainDo.totalNonScheduledStock
             tvScheduledStock.setText("" + scheduleStock + " " + loadStockMainDo.vehicleCapacityUnit)
             tvNonScheduledStock.setText("" + nonScheduleStock + " " + loadStockMainDo.vehicleCapacityUnit)

             var total = 0
             var capacity = preferenceUtils.getStringFromPreference(PreferenceUtils.TOTAL_VEHICLE_CAPACITY, "")
             total = capacity.toInt()
             scheduleTotal = scheduleStock.toInt()

             nonScheduleTotal = nonScheduleStock.toInt()
             tvBalanceStock.setText("" + (total - scheduleTotal - nonScheduleTotal) + " " + loadStockMainDo.vehicleCapacityUnit)
         } catch (e: java.lang.Exception) {
             e.printStackTrace()
         }

     }*/

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentScheduleStockInteraction()
    }

    private fun showHideViews(hasData: Boolean, message: String) {
        try {
            if (hasData) {
                no_data_found.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
                ll_cal.visibility = View.VISIBLE
            } else {
                no_data_found.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
                no_data_found.text = message
                ll_cal.visibility = View.GONE
            }
            resultListner.onResultListner(hasData, hasData)
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    fun getSchedueNonScheduleData(vanStockDetails: VanStockDetailsDO): ArrayList<StockItemDo> {

        val list = ArrayList<StockItemDo>()

        val stockList = vanStockDetails.stockItemList
        val map = HashMap<String, StockItemDo>()
        for (item in stockList) {
            if (item.stockType == 2) {
                if (map.containsKey(item.productName)) {
                    val count = item.quantity.toDouble() + map.getValue(item.productName).updateQty
                    item.updateQty = count
                    item.quantity = count
                    map.put(item.productName, item)
                } else {
                    item.updateQty = item.quantity.toDouble()
                    map.put(item.productName, item)
                }
            }

        }

        for (mapItem in map) {
            list.add(mapItem.value)
        }

        return list
    }

}
