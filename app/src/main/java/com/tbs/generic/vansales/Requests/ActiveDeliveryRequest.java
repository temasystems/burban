package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.ImageDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressDialogTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class ActiveDeliveryRequest extends AsyncTask<String, Void, Boolean> {

    private ActiveDeliveryMainDO activeDeliveryMainDO;
    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    ActiveDeliveryDO activeDeliveryDO;
    ImageDO imageDO;
    int docType;

    public ActiveDeliveryRequest(int doctype,String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
        this.docType = doctype;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ActiveDeliveryMainDO activeDeliveryMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
//        int docType = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
        String shipmntType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YSDHNUM", id);
            if (shipmntType.equals(mContext.getResources().getString(R.string.checkin_non_scheduled))) {
                jsonObject.put("I_XDOCTYP", 1);

            } else {
                jsonObject.put("I_XDOCTYP", docType);

            }


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.ACTIVEDELIVERY, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            activeDeliveryMainDO = new ActiveDeliveryMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    attribute = xpp.getAttributeValue(null, "ID");
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP2")) {
                        activeDeliveryMainDO.activeDeliveryDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP2");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP3")) {
                        activeDeliveryMainDO.imageDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP3");
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        activeDeliveryDO = new ActiveDeliveryDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        imageDO = new ImageDO();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("I_YSDHNUM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.shipmentNumber = text;
                            }


                        } else if (attribute.equalsIgnoreCase("O_YSDHFLG")) {
                            activeDeliveryMainDO.validatedFlag = Integer.parseInt(text);

                        }
                        else if (attribute.equalsIgnoreCase("O_YBPC")) {
                            activeDeliveryMainDO.customer = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YSTOFCY")) {
                            activeDeliveryMainDO.site = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YSERUPDFLG")) {
                            activeDeliveryDO.serialupdate = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHVALFLG")) {
                            activeDeliveryDO.validatedFlag = Integer.parseInt(text);

                        }else if (attribute.equalsIgnoreCase("O_YTOTWEIGHT")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.totalWeight = Double.parseDouble(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YTOTVOLUME")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.totalVolume = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YWEIGHTUN")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.weightUnit = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YTOTLDUN")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.ldUnit = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTPTUN")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.unit = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YVOLUMEUN")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.volumeUnit = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YDOCTYP")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.doctype = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_LINKEDDOC")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.loandelivery = text;
                            }
                        }else if (attribute.equalsIgnoreCase("O_YTOTDOCQTY")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.totalQuantity = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YTOTLDQTY")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.totalLDQuantity = Integer.parseInt(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPCSHO")) {
                            activeDeliveryMainDO.customerDescription = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YPAYTERM")) {
                            activeDeliveryMainDO.paymentTerm = text;

                        } else if (attribute.equalsIgnoreCase("O_YARTIM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.arrivalTime = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.logo = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEDES")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteDescription = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteAddress1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG1")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteAddress2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG2")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteAddress3 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCRY")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteCountry = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCTY")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteCity = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XDELEM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.deliveryEmail = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XLINTEX")) {
                            if (text.length() > 0) {

                                activeDeliveryDO.lineText = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XSERNUM")) {
                            if (text.length() > 0) {

                                activeDeliveryDO.serialNumber = text;
                            }
                        }else if (attribute.equalsIgnoreCase("O_YSIGNATURE")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.signature = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCREUSR")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.createUserID = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCREUSRNAM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.createUserName = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCRETIM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.createdTime = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XINVEM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.invoiceEmail = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XNOTES")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.notes = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XPAYEM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.paymentEmail = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XCYLEM")) {
                            if (text.length() > 0) {

                                activeDeliveryMainDO.cylinderIssueEmail = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITPOS")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.sitePostalCode = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITLND")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteLandLine = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITMOB")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteMobile = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITFAX")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteFax = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG")) {


                            activeDeliveryMainDO.customerStreet = text;
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG1")) {


                            activeDeliveryMainDO.customerLandMark = text;
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG2")) {


                            activeDeliveryMainDO.customerTown = text;
                        } else if (attribute.equalsIgnoreCase("O_YCITY")) {


                            activeDeliveryMainDO.customerCity = text;
                        } else if (attribute.equalsIgnoreCase("O_YPRVLAT")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.prevLattitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPRVLON")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.prevLongitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YAFTERLAT")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.afterLattitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YAFTERLON")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.afterLongitude = Double.parseDouble(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPOS")) {


                            activeDeliveryMainDO.customerPostalCode = text;
                        } else if (attribute.equalsIgnoreCase("O_XCPYEML1")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteEmail1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML2")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteEmail2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.siteWebEmail = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_YARDAT")) {
                            activeDeliveryMainDO.arraivalDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDPTIM")) {
                            activeDeliveryMainDO.departureTime = text;

                        } else if (attribute.equalsIgnoreCase("O_YDPDAT")) {
                            activeDeliveryMainDO.departureDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YDLVDAT")) {
                            activeDeliveryMainDO.deliveryDate = text;

                        } else if (attribute.equalsIgnoreCase("O_XCREDAT")) {
                            activeDeliveryMainDO.createdDate = text;

                        } else if (attribute.equalsIgnoreCase("O_YCRYNAME")) {
                            activeDeliveryMainDO.countryName = text;

                        } else if (attribute.equalsIgnoreCase("O_YWEB")) {
                            activeDeliveryMainDO.webSite = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YPOHNUM")) {
                            activeDeliveryMainDO.poNumber = text;

                        } else if (attribute.equalsIgnoreCase("O_YLD")) {
                            activeDeliveryMainDO.landLine = text;

                        } else if (attribute.equalsIgnoreCase("O_YQTYCNGAPR")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.qtyApproval = Integer.parseInt(text);

                            }

                        } else if (attribute.equalsIgnoreCase("O_YPRICNGAPR")) {
                            if (text.length() > 0) {
                                activeDeliveryMainDO.priceApproval = Integer.parseInt(text);

                            }
                        } else if (attribute.equalsIgnoreCase("O_YEM")) {
                            activeDeliveryMainDO.email = text;

                        } else if (attribute.equalsIgnoreCase("O_YFAX")) {
                            activeDeliveryMainDO.fax = text;

                        } else if (attribute.equalsIgnoreCase("O_YMOB")) {
                            activeDeliveryMainDO.mobile = text;

                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            activeDeliveryMainDO.lattitude = text;

                        } else if (attribute.equalsIgnoreCase("O_LON")) {
                            activeDeliveryMainDO.longitude = text;

                        } else if (attribute.equalsIgnoreCase("O_YSHIDAT")) {
                            activeDeliveryMainDO.shipmentDate = text;

                        } else if (attribute.equalsIgnoreCase("O_XCPYNAM")) {
                            activeDeliveryMainDO.companyCode = text;

                        } else if (attribute.equalsIgnoreCase("O_XCPYDES")) {
                            activeDeliveryMainDO.companyDescription = text;

                        } else if (attribute.equalsIgnoreCase("O_YITM")) {
                            activeDeliveryDO.product = text;
                            activeDeliveryDO.pType = "SCHEDULED";


                        } else if (attribute.equalsIgnoreCase("O_YAVLVEHMASS")) {
                            activeDeliveryMainDO.vehicleCapacity = Double.valueOf(text);


                        } else if (attribute.equalsIgnoreCase("O_YITMWET")) {
                            if (!text.isEmpty()) {
                                activeDeliveryDO.productWeight = Double.parseDouble(text);
                            }


                        } else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                            activeDeliveryDO.productDescription = text;

                        } else if (attribute.equalsIgnoreCase("O_YITMLOC")) {
                            activeDeliveryDO.location = text;

                        } else if (attribute.equalsIgnoreCase("O_XWEU")) {
                            activeDeliveryDO.unit = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YSDHCAT")) {

                            activeDeliveryMainDO.deliveryType = Integer.parseInt(text);


                        }else if (attribute.equalsIgnoreCase("O_YDQTY")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.totalQuantity = Integer.parseInt(text);
                                activeDeliveryDO.orderedQuantity = Integer.parseInt(text);// to fix issue at added products showing full qty instead selected qty in add productlistactivity
                            }

                        } else if (attribute.equalsIgnoreCase("O_YREMARKS")) {
                            if (!text.isEmpty()) {

                                activeDeliveryMainDO.remarks = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSELFLG")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.serialLotFlag = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDDLIN")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.linenumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YCMTR")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.openingQuantity = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YOMTR")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.endingQuantity = Double.valueOf(text);
                            }

                        }
//
                        else if (attribute.equalsIgnoreCase("O_YGRI")) {
                            if (!text.isEmpty()) {

                                activeDeliveryDO.price = Double.parseDouble(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_YPODIMAGES")) {
                            if (!text.isEmpty()) {

                                imageDO.photoImage = text;
                            }

                        }
                        text = "";


                    }


                    if (endTag.equalsIgnoreCase("GRP3")) {
                        //   customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    } else if (endTag.equalsIgnoreCase("LIN") &&
                            preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        activeDeliveryMainDO.activeDeliveryDOS.add(activeDeliveryDO);
                        if (activeDeliveryDO.productType.length() > 0) {

                            activeDeliveryDO.productType = AppConstants.ShipmentListView;
                        }
                    } else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        activeDeliveryMainDO.imageDOS.add(imageDO);

                    }
//                    if (endTag.equalsIgnoreCase("LIN")) {
//                        activeDeliveryMainDO.activeDeliveryDOS.add(activeDeliveryDO);
//                        if (activeDeliveryDO.productType.length() > 0) {
//
//                            activeDeliveryDO.productType = AppConstants.ShipmentListView;
//                        }
//
//                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {

        super.onPreExecute();
        /*((BaseActivity) mContext).showLoader();*/

        ProgressDialogTask.getInstance().showProgress(mContext, false, "");

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        /* ((BaseActivity) mContext).hideLoader();*/
        ProgressDialogTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, activeDeliveryMainDO);
        }
    }
}