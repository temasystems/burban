package com.tbs.generic.vansales.Activitys

//import com.tbs.generic.vansales.here.HEREActivity
import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tbs.generic.vansales.Adapters.ReasonListAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.here.Constants
import com.tbs.generic.vansales.here.TurnByTurnNavigationActivity
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.android.synthetic.main.include_active_delivery_steps.*
import kotlinx.android.synthetic.main.include_toolbar.*
import kotlinx.android.synthetic.main.new_active_delivery.*
import java.util.*

//
class ActiveDeliveryActivity : BaseActivity(), OnMapReadyCallback {

    lateinit var googleMap: GoogleMap
    lateinit var podDo: PodDo
    private var shipmentId: String = ""
    private var shipmentType: String = ""
    lateinit var tvDateTime: TextView
    lateinit var tvEmail: TextView
    lateinit var tvName: TextView
    lateinit var tvPhoneNumber: TextView
    lateinit var activeDeliveryDo: ActiveDeliveryMainDO
    lateinit var btnReturnDetails: Button
    lateinit var btnPod: LinearLayout
    lateinit var tvShipmentList: TextView
    lateinit var btnStartRoute: LinearLayout
    lateinit var btnCancelRoute: LinearLayout
    lateinit var ivNavigate: ImageView
    lateinit var dialog: Dialog
    lateinit var btnSkip: LinearLayout
    var distance: Double = 0.0

    override fun initialize() {
        val llCategories = layoutInflater.inflate(R.layout.new_active_delivery, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        changeLocale()

        initializeControls()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
        act_toolbar.setNavigationOnClickListener {
            val intent = Intent()
            intent.putExtra(resources.getString(R.string.captured_returns), AppConstants.CapturedReturns)
            setResult(19, intent)
            finish()
        }

        activeDeliveryDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        var dropFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0)
//        if(dropFlag==1){
//
//        }
        tv_title.setText(resources.getString(R.string.active_delivery)+"\n"+activeDeliveryDo.shipmentNumber)

        tvName.text = activeDeliveryDo.customerDescription

        tvDateTime.text = activeDeliveryDo.customerStreet + activeDeliveryDo.customerLandMark + activeDeliveryDo.customerTown + " " + activeDeliveryDo.customerCity + " " + activeDeliveryDo.countryName

        var podDo = StorageManager.getInstance(this).getDepartureData(this)
        ll_phone.setOnClickListener {
            isPermissionGranted()
            var intent = Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + activeDeliveryDo.mobile));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return@setOnClickListener
            }
            startActivity(intent);
//            if(isPermissionGranted()){
//
//            }else{
//                val builder = AlertDialog.Builder(this)
//                builder.setMessage("Your Call Permission seems to be disabled, do you want to enable it?")
//                        .setCancelable(false)
//                        .setPositiveButton("Yes", object: DialogInterface.OnClickListener {
//                            override fun onClick(dialog: DialogInterface, id:Int) {
//                                startInstalledAppDetailsActivity(this@ActiveDeliveryActivity)
//                            }
//                        })
//                        .setNegativeButton("No", object: DialogInterface.OnClickListener {
//                            override fun onClick(dialog: DialogInterface, id:Int) {
//                                dialog.cancel()
//                            }
//                        })
//                val alert = builder.create()
//                alert.show()
//            }

        }
        ll_email.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:${activeDeliveryDo.email}"))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "body")
            startActivity(Intent.createChooser(emailIntent, "Chooser Title"))
        }
        shipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "")
        var rcpt = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_RECEIPT, "")

        if (rcpt.isEmpty()) {
            btnCancelRoute.visibility = View.GONE
            btnSkip.visibility = View.VISIBLE
            rl_detail_document.visibility = View.VISIBLE

        } else {
            rl_detail_document.visibility = View.VISIBLE
            btnCancelRoute.visibility = View.VISIBLE
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.white))
            btnCancelRoute.isClickable = true
            btnCancelRoute.isEnabled = true
            btnSkip.visibility = View.GONE
        }
        if (podDo != null && !podDo.podTimeCaptureCaptureDeliveryTime.equals("")) {
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnCancelRoute.isClickable = false
            btnCancelRoute.isEnabled = false
        }

        if (!activeDeliveryDo.podTime.equals("")) {
            btnPod.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnPod.isClickable = false
            btnPod.isEnabled = false
        }
        if (!activeDeliveryDo.CaptureReturnsTime.equals("")) {
            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnReturnDetails.isClickable = false
            btnReturnDetails.isEnabled = false

        }
        var pickupFlag = preferenceUtils.getIntFromPreference(PreferenceUtils.PICKUP_DROP_FLAG, 0)
        val locale: Locale = resources.configuration.locale

        if (pickupFlag == 1) {
            if(locale== Locale.FRENCH){
                tvPOD.setText("SAISIE ENLEVEMENT")

                ivPOD.setImageDrawable(
                        ContextCompat.getDrawable(
                                applicationContext, // Context
                                R.drawable.fr_poc // Drawable
                        )
                )
            }else{

                ivPOD.setImageDrawable(
                        ContextCompat.getDrawable(
                                applicationContext, // Context
                                R.drawable.poc // Drawable
                        )
                )
            }


        } else {
            if(locale== Locale.FRENCH){
                tvPOD.setText("SAISIE LIVRAISON")

                ivPOD.setImageDrawable(
                        ContextCompat.getDrawable(
                                applicationContext, // Context
                                R.drawable.fr_pod // Drawable
                        )
                )
            }else{
                ivPOD.setImageDrawable(
                        ContextCompat.getDrawable(
                                applicationContext, // Context
                                R.drawable.pod_48 // Drawable
                        )
                )
            }
        }
        if (intent.hasExtra("SHIPMENT_ID")) {
            shipmentId = intent.extras?.getString("SHIPMENT_ID").toString()
        }

        if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
            if (Util.isNetworkAvailable(this)) {
                val driverListRequest = ActiveDeliveryRequest(dropFlag,shipmentId, this@ActiveDeliveryActivity)
                driverListRequest.setOnResultListener { isError, activeDeliveryMainDo ->
                    if (isError) {
                        Toast.makeText(this@ActiveDeliveryActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
                    } else {
                        if (intent.hasExtra(resources.getString(R.string.scheduled_customer))) {
                            intent.extras?.getString(resources.getString(R.string.scheduled_customer))
                        }
                        activeDeliveryDo = activeDeliveryMainDo
                        StorageManager.getInstance(this).saveActiveDeliveryMainDo(this, activeDeliveryDo)

                        tvName.text = activeDeliveryDo.customerDescription
                        tvPhoneNumber.text = activeDeliveryDo.mobile
                        tvEmail.text = activeDeliveryDo.email
                        tvDateTime.text = activeDeliveryDo.customerStreet + activeDeliveryDo.customerLandMark + activeDeliveryDo.customerTown + " " + activeDeliveryDo.customerCity + " " + activeDeliveryDo.countryName
                        if( activeDeliveryDo.mobile.isNotEmpty()){
                            tvPhoneNumber.text = activeDeliveryDo.mobile
                            ll_phone.visibility=View.VISIBLE

                        }else{
                            ll_phone.visibility=View.GONE
                        }
                        if( activeDeliveryDo.email.isNotEmpty()){
                            tvEmail.text = activeDeliveryDo.email
                            ll_email.visibility=View.VISIBLE

                        }else{
                            ll_email.visibility=View.GONE
                        }
                        val arrivalTime = activeDeliveryDo.arrivalTime.substring(Math.max(activeDeliveryDo.arrivalTime.length - 2, 0))
                        val departureTime = activeDeliveryDo.departureTime.substring(Math.max(activeDeliveryDo.departureTime.length - 2, 0))
                        val arrivalTime2: String
                        val departureTime2: String
                        val aMonth: String
                        val ayear: String
                        val aDate: String
                        val dMonth: String
                        val dyear: String
                        val dDate: String
                        try {
                            arrivalTime2 = activeDeliveryDo.arrivalTime.substring(0, 2)
                            departureTime2 = activeDeliveryDo.departureTime.substring(0, 2)
                            aMonth = activeDeliveryDo.arraivalDate.substring(4, 6)
                            ayear = activeDeliveryDo.arraivalDate.substring(0, 4)
                            aDate = activeDeliveryDo.arraivalDate.substring(Math.max(activeDeliveryDo.arraivalDate.length - 2, 0))
                            dMonth = activeDeliveryDo.departureDate.substring(4, 6)
                            dyear = activeDeliveryDo.departureDate.substring(0, 4)
                            dDate = activeDeliveryDo.departureDate.substring(Math.max(activeDeliveryDo.departureDate.length - 2, 0))
                            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_DT, dDate + "-" + dMonth + "-" + dyear + "  " + departureTime2 + ":" + departureTime)
                            preferenceUtils.saveString(PreferenceUtils.SHIPMENT_AT, aDate + "-" + aMonth + "-" + ayear + "  " + arrivalTime2 + ":" + arrivalTime)
                        } catch (e: Exception) {
                        }
                        if (!activeDeliveryDo.podTime.equals("")) {
                            btnPod.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnPod.isClickable = false
                            btnPod.isEnabled = false
                        }
                        if (!activeDeliveryDo.CaptureReturnsTime.equals("")) {
                            btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                            btnReturnDetails.isClickable = false
                            btnReturnDetails.isEnabled = false
                        }
                    }
                }
                driverListRequest.execute()
            } else {
                showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

            }


        } else {
            val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
            tvName.text = "" + customerDo.customer
            tvEmail.text = "" + customerDo.email
            tvPhoneNumber.text = "" + customerDo.mobile
            tvDateTime.text = customerDo.city + customerDo.landmark + customerDo.town + " " + customerDo.businessLine + " " + customerDo.countryName
            if( activeDeliveryDo.mobile.isNotEmpty()){
                tvPhoneNumber.text = activeDeliveryDo.mobile
                ll_phone.visibility=View.VISIBLE

            }else{
                ll_phone.visibility=View.GONE
            }
            if( activeDeliveryDo.email.isNotEmpty()){
                tvEmail.text = activeDeliveryDo.email
                ll_email.visibility=View.VISIBLE

            }else{
                ll_email.visibility=View.GONE
            }
        }

        btnCancelRoute.setOnClickListener {
            Util.preventTwoClick(it)

            var podDo = StorageManager.getInstance(this).getDepartureData(this)
            if (podDo != null && !podDo.arrivalTime.equals("")) {
                showToast(resources.getString(R.string.pod_validation_arrival))
            } else {
                preferenceUtils.removeFromPreference(PreferenceUtils.Reason_OTH)
                preferenceUtils.removeFromPreference(PreferenceUtils.SCHEDULE_DOCUMENT)
                preferenceUtils.removeFromPreference(PreferenceUtils.ETA_ETD)
                preferenceUtils.removeFromPreference(PreferenceUtils.STOP)
                preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_CODE);

                val skipShipmentList = StorageManager.getInstance(this).getSkipShipmentList(this)
                skipShipmentList.add(shipmentId)
                StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
                StorageManager.getInstance(this).deleteDepartureData(this)
                StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
                StorageManager.getInstance(this).deleteReturnCylinders(this)
                preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
                preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_RECEIPT)

                preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
                preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
                preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL)
                preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL)
                preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
                preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID)
                val intent = Intent()
                intent.putExtra(resources.getString(R.string.skip_shipment), resources.getString(R.string.skip))
                setResult(52, intent)
                finish()
            }


        }

    }

    fun isPermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if ((checkSelfPermission(android.Manifest.permission.CALL_PHONE) === PackageManager.PERMISSION_GRANTED)) {
                Log.v("TAG", "Permission is granted")
                return true
            } else {
                Log.v("TAG", "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.CALL_PHONE), 1)
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted")
            return true
        }
    }

    fun startInstalledAppDetailsActivity(context: Activity) {
        if (context == null) {
            return
        }
        val i = Intent()
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        i.addCategory(Intent.CATEGORY_DEFAULT)
        i.setData(Uri.parse("package:" + context.getPackageName()))
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        context.startActivity(i)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 11) {//CaptureReturnDetailsActivity
            activeDeliveryDo.CaptureReturnsTime = CalendarUtils.getCurrentDate(this)

        } else if (requestCode == 13 && resultCode == 13) {

        } else if (requestCode == 12 && resultCode == 12) {
            if (data != null && data.hasExtra(resources.getString(R.string.captured_returns)) && data.getBooleanExtra(resources.getString(R.string.captured_returns), false)) {
                AppConstants.CapturedReturns = true
                btnPod.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnPod.isClickable = false
                btnPod.isEnabled = false
                btnStartRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnStartRoute.isClickable = false
                btnStartRoute.isEnabled = false
                btnReturnDetails.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnReturnDetails.isClickable = false
                btnReturnDetails.isEnabled = false
                btnCancelRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
                btnCancelRoute.isClickable = false
                btnCancelRoute.isEnabled = false
                val intent = Intent()
                intent.putExtra(resources.getString(R.string.captured_returns), AppConstants.CapturedReturns)
                setResult(19, intent)
                finish()
            }
        } else if (requestCode == 12 && resultCode == 52) {
            val intent = Intent()
            intent.putExtra(resources.getString(R.string.skip_shipment), resources.getString(R.string.skip))
            setResult(resultCode, intent)
            finish()
        } else if (requestCode == 12 && resultCode == 53) {
            val intent = Intent()
            intent.putExtra(resources.getString(R.string.skip_shipment), resources.getString(R.string.hint_cancel))
            setResult(resultCode, intent)
            finish()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun reasonDialog() {
        dialog = Dialog(this, R.style.NewDialog)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.simple_list_dialog)
        val window = dialog.window
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        }
        var recyclerView = dialog.findViewById(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        if (reasonMainDO != null && reasonMainDO.reasonDOS != null && reasonMainDO.reasonDOS.size > 0) {
//            val siteAdapter = ReasonListAdapter(this@Cancel_RescheduleActivity, reasonMainDO.reasonDOS)
//            recyclerView.setAdapter(siteAdapter)
//        } else {
        if (Util.isNetworkAvailable(this)) {
            val siteListRequest = ReasonsListRequest(5, this)
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null) {
                    if (isError) {
                        showAppCompatAlert("", getString(R.string.no_reasong_found_at_this_movment), getString(R.string.ok), getString(R.string.cancel), "", false)
                    } else {

                        val siteAdapter = ReasonListAdapter(5, this@ActiveDeliveryActivity, unPaidInvoiceMainDO.reasonDOS)
                        recyclerView.adapter = siteAdapter
                    }
                } else {
                    hideLoader()
                    showAppCompatAlert("", getString(R.string.no_reasong_found_at_this_movment), getString(R.string.ok), getString(R.string.cancel), "", false)

                }
            }
            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }





        dialog.show()


    }

    override fun initializeControls() {
        toolbar.setNavigationIcon(R.drawable.back)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this@ActiveDeliveryActivity)
        tvName = findViewById<TextView>(R.id.tvName)
        tvEmail = findViewById<TextView>(R.id.tvEmail)
        tvPhoneNumber = findViewById<TextView>(R.id.tvPhoneNumber)
        tvDateTime = findViewById<TextView>(R.id.tvDateTime)
        tvShipmentList = findViewById<TextView>(R.id.tvShipmentList)
        ivNavigate = findViewById<ImageView>(R.id.ivNavigate)
        btnCancelRoute = findViewById<LinearLayout>(R.id.btnCancelRoute)
        btnStartRoute = findViewById<LinearLayout>(R.id.btnStartRoute)
        btnSkip = findViewById<LinearLayout>(R.id.btnSkip)
        btnPod = findViewById<LinearLayout>(R.id.btnPod)
        btnReturnDetails = findViewById<Button>(R.id.btnReturnDetails)
        podDo = StorageManager.getInstance(this).getDepartureData(this)
        if (podDo != null && !podDo.podTimeCaptureCaptureDeliveryTime.equals("")) {
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.md_gray_light))
            btnCancelRoute.isClickable = false
            btnCancelRoute.isEnabled = false
        } else {
            btnCancelRoute.setBackgroundColor(resources.getColor(R.color.white))
            btnCancelRoute.isClickable = true
            btnCancelRoute.isEnabled = true
        }
        btnSkip.setOnClickListener {
            Util.preventTwoClick(it)


            podDo = StorageManager.getInstance(this).getDepartureData(this)
            if (podDo == null || podDo.arrivalTime.equals("")) {
                reasonDialog()
            } else {
                showToast(resources.getString(R.string.cannot_use_pod))

            }
        }
        btnStartRoute.setOnClickListener {
            Util.preventTwoClick(it)

            var customDialog = Dialog(this);

            customDialog.setTitle(resources.getString(R.string.select));

            customDialog.setContentView(R.layout.custom_dialog);

            customDialog.show();
            customDialog.ivGmaps.setOnClickListener {
                customDialog.dismiss()
                val latitude = activeDeliveryDo.lattitude//"28.7041"
                val longitude = activeDeliveryDo.longitude//"77.1025"
//                val latitude = activeDeliveryDo.lattitude//"28.7041"
//                val longitude = activeDeliveryDo.longitude//"77.1025"
                val gmmIntentUri = resources.getString(R.string.gmap_harcode1) + latitude + "," + longitude + "&mode=d"
                val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
                mapIntent.setPackage(resources.getString(R.string.gmap_pkg))
                try {
                    if (mapIntent.resolveActivity(packageManager) != null) {
                        deliveryStatus(2)
                        podDo.starttime = CalendarUtils.getTime()
                        StorageManager.getInstance(this).saveDepartureData(this, podDo)
//                        startRoute(2)
                        val depatureDate = preferenceUtils.getStringFromPreference(PreferenceUtils.CD_DATE, "")
                        val departureTime = preferenceUtils.getStringFromPreference(PreferenceUtils.CD_TIME, "")
                        var time = CalendarUtils.timeDifference(depatureDate, departureTime, this)
                        startRoute(time)

                        startActivityForResult(mapIntent, 1542)
                        onUserLeaveHint()
                    }
                } catch (e: Exception) {
                    Toast.makeText(this, resources.getString(R.string.map_error), Toast.LENGTH_SHORT).show()
                }
            }
            customDialog.ivHere.setOnClickListener {
                customDialog.dismiss()
                if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
                    activeDeliveryDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

                    if (activeDeliveryDo.lattitude.isNotEmpty()) {
                        deliveryStatus(2)

                        Constants.Des_Lattitude = activeDeliveryDo.lattitude.toDouble()
                        Constants.Des_Longitude = activeDeliveryDo.longitude.toDouble()
                        val intent = Intent(this@ActiveDeliveryActivity, TurnByTurnNavigationActivity::class.java)
                        startActivity(intent)
                    } else {
                        showToast(resources.getString(R.string.pass_co_ordinates))
                    }

                } else {
                    val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                    customerDo.lattitude = Constants.Des_Lattitude.toString()
                    val intent = Intent(this@ActiveDeliveryActivity, TurnByTurnNavigationActivity::class.java)
                    startActivity(intent)
                    if (customerDo.lattitude.isNotEmpty()) {
                        deliveryStatus(2)

                        Constants.Des_Lattitude = customerDo.lattitude.toDouble()
                        Constants.Des_Longitude = customerDo.longitude.toDouble()
                        val intent = Intent(this@ActiveDeliveryActivity, TurnByTurnNavigationActivity::class.java)
                        startActivity(intent)
                    } else {
                        showToast(resources.getString(R.string.pass_co_ordinates))
                    }

                }
            }
        }
        ivNavigate.setOnClickListener {
            Util.preventTwoClick(it)

            val latitude = activeDeliveryDo.lattitude//"28.7041"
            val longitude = activeDeliveryDo.longitude//"77.1025"

            val gmmIntentUri = resources.getString(R.string.gmap_harcode1) + latitude + "," + longitude + "&mode=d"
            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
            mapIntent.setPackage(resources.getString(R.string.gmap_pkg))
            try {
                if (mapIntent.resolveActivity(packageManager) != null) {
                    startActivityForResult(mapIntent, 1542)
                    onUserLeaveHint()
                }
            } catch (e: Exception) {
                Toast.makeText(this, resources.getString(R.string.map_error), Toast.LENGTH_SHORT).show()
            }
        }
        btnPod.setOnClickListener {
//            if (podDo.starttime.equals("")) {
//                showToast(resources.getString(R.string.start_route_validation))
//            } else {
                Util.preventTwoClick(it)
                if(activeDeliveryDo.doctype==2){
                    var ld = preferenceUtils.getStringFromPreference(PreferenceUtils.LOAN_DELIVERY, "")
                    if(ld.isNullOrEmpty()){
                        val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java)
                        intent.putExtra(resources.getString(R.string.distance), distance)
                        startActivityForResult(intent, 12)
                    }else{
                        val intent = Intent(this@ActiveDeliveryActivity, POEActivity::class.java)
                        intent.putExtra(resources.getString(R.string.distance), distance)
                        startActivityForResult(intent, 12)
                    }

                }else{
                    val intent = Intent(this@ActiveDeliveryActivity, PODActivity::class.java)
                    intent.putExtra(resources.getString(R.string.distance), distance)
                    startActivityForResult(intent, 12)
                }


//            }
        }

        rl_detail_document.setOnClickListener {
            Util.preventTwoClick(it)
            var shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            if (!shipmentId.equals("")) {
                val intent = Intent(this@ActiveDeliveryActivity, ShipmentDetailsActivity::class.java)
                startActivity(intent)
            }
        }

//        btnReturnDetails.setOnClickListener {
//            Util.preventTwoClick(it)
//            if (activeDeliveryDo.startRouteTime.equals("")) {
//                showToast(resources.getString(R.string.start_route_validation))
//            } else {
//                val intent = Intent(this@ActiveDeliveryActivity, CaptureReturnDetailsActivity::class.java)
//                startActivityForResult(intent, 11)
//            }
//        }
    }


    fun skipShipment(reason: String) {
        if (Util.isNetworkAvailable(this)) {

            var distance = CalculationByDistance(LatLng(activeDeliveryDo.prevLattitude, activeDeliveryDo.prevLongitude), LatLng(activeDeliveryDo.afterLattitude, activeDeliveryDo.afterLongitude))
            val siteListRequest = SkipShipmentRequest(distance, reason, this@ActiveDeliveryActivity)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()

                if (isError) {
                    hideLoader()
                    Toast.makeText(this@ActiveDeliveryActivity, resources.getString(R.string.server_error), Toast.LENGTH_SHORT).show()
                } else {
                    hideLoader()
                    if (loginDo.flag == 2) {
                        preferenceUtils.removeFromPreference(PreferenceUtils.Reason_OTH)
                        preferenceUtils.removeFromPreference(PreferenceUtils.SCHEDULE_DOCUMENT)
                        preferenceUtils.removeFromPreference(PreferenceUtils.ETA_ETD)
                        preferenceUtils.removeFromPreference(PreferenceUtils.STOP)
                        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_CODE);

                        val skipShipmentList = StorageManager.getInstance(this).getSkipShipmentList(this)
                        skipShipmentList.add(shipmentId)
                        StorageManager.getInstance(this).saveSkipShipmentList(this, skipShipmentList)
                        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
                        StorageManager.getInstance(this).deleteDepartureData(this)
                        StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
                        StorageManager.getInstance(this).deleteReturnCylinders(this)
                        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
                        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
                        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
                        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL)
                        preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL)
                        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
                        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID)
                        showAppCompatAlert("Info!", resources.getString(R.string.doc_skip), "OK", "   ", "SKIP", false)


                    } else {
                        showToast(resources.getString(R.string.doc_not_skip))
                    }

                }


            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", "", false)

        }


    }

    override fun onMapReady(p0: GoogleMap?) {
        googleMap = p0!!
        if (googleMap == null) {
            Toast.makeText(applicationContext, R.string.map_error, Toast.LENGTH_SHORT).show()
        } else {
            googleMap.uiSettings.isZoomControlsEnabled = true

        }
        showLocation()
        try {

        } catch (e: Exception) {

        }


    }

    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if ("SKIP".equals(from, ignoreCase = true)) {
            val intent = Intent()
            intent.putExtra(resources.getString(R.string.skip_shipment), resources.getString(R.string.skip))
            setResult(52, intent)
            finish()
        }

    }

    private fun showLocation() {
        try {

            val markerOptions = MarkerOptions()
            preferenceUtils = PreferenceUtils(this@ActiveDeliveryActivity)
            if (shipmentType.equals(resources.getString(R.string.checkin_scheduled), true) && shipmentId.length > 0) {
                val lat = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LATTITUDE, 0.0)
                val lng = preferenceUtils.getDoubleFromPreference(PreferenceUtils.LONGITUDE, 0.0)
                markerOptions.position(LatLng(lng, lat))
                googleMap.addMarker(markerOptions)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lng, lat), 16f))
            } else {
                var customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)
                var lat = java.lang.Double.parseDouble(customerDo.lattitude)
                var lng = java.lang.Double.parseDouble(customerDo.longitude)
                markerOptions.position(LatLng(lng, lat))
                googleMap.addMarker(markerOptions)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lng, lat), 16f))
            }


        } catch (e: Exception) {
        }

    }

    override fun onResume() {
        super.onResume()
    }

    fun deliveryStatus(status: Int) {
        if (Util.isNetworkAvailable(this)) {
            var vrId = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

            val siteListRequest = DeliveryRoutingIdRequest(this@ActiveDeliveryActivity, vrId, deliveryId, status,lattitudeFused,longitudeFused)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 2) {
                            //  showToast(resources.getString(R.string.del_on_way))

                        } else {
                        }
                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

        }
    }

    fun startRoute(time: String) {
        if (Util.isNetworkAvailable(this)) {
            var vrId = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            var deliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
            val doc_number = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

            val siteListRequest = StartRoutingIdRequest(this@ActiveDeliveryActivity, vrId, deliveryId, time, doc_number)
            siteListRequest.setOnResultListener { isError, loginDo ->
                hideLoader()
                if (loginDo != null) {
                    if (isError) {
                        hideLoader()
                    } else {
                        hideLoader()
                        if (loginDo.flag == 2) {
                            //  showToast(resources.getString(R.string.del_on_way))

                        } else {
                        }
                    }
                } else {
                    hideLoader()
                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(resources.getString(R.string.alert_message), resources.getString(R.string.internet_connection), resources.getString(R.string.ok), "", resources.getString(R.string.from_failure), false)

        }
    }

}