package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.TrailerSelectionMainDO;
import com.tbs.generic.vansales.Model.VRSelectionDO;
import com.tbs.generic.vansales.Model.VRSelectionMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class TrailerSelectionRequest extends AsyncTask<String, Void, Boolean> {

    private TrailerSelectionMainDO trailerSelectionMainDO;
    private TrailerSelectionDO trailerSelectionDO;
    private Context mContext;
    PreferenceUtils preferenceUtils;


    public TrailerSelectionRequest(Context mContext) {

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, TrailerSelectionMainDO trailerSelectionMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "");
        String vr = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
        String transaction = preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "");

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_YNUMPC", vr);
            jsonObject.put("I_YVCRNUM", transaction);
            jsonObject.put("I_XFCY", id);
            jsonObject.put("I_YCODEYVE", preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, ""));


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.TRAILOR_SELECTION, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("VR selection xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            trailerSelectionMainDO = new TrailerSelectionMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        trailerSelectionMainDO.trailerSelectionDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        trailerSelectionDO = new TrailerSelectionDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XTRAILER")) {
                            trailerSelectionDO.trailer = text;


                        } else if (attribute.equalsIgnoreCase("O_XTRAILERDES")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.trailerDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XLINK")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.link = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XTRATYPE")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.type = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XNOOFAXLE")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.axel = Double.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_XMAXLODMASS")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.mass = Integer.valueOf(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XMAXLODMASS")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.mass = Integer.valueOf(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XSELECT")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.isSelceted = Integer.parseInt(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_XMAXLODVOL")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.volume = Integer.valueOf(text);
                            }
                        } else if (attribute.equalsIgnoreCase("O_XVOLUN")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.volumeUnit = text;
                            }
                        }


                        text = "";                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        trailerSelectionMainDO.trailerSelectionDOS.add(trailerSelectionDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if (xpp.getText().length() > 0) {

                        text = xpp.getText();
                    } else {
                        text = "";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, trailerSelectionMainDO);
        }
        ((BaseActivity) mContext).hideLoader();

    }
}