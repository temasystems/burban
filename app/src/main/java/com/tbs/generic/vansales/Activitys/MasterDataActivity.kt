package com.tbs.generic.vansales.Activitys

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.SpotSalesCustomerAdapter
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.MasterDataRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*

class MasterDataActivity : BaseActivity() {
    lateinit var  userId: String
    lateinit var orderCode: String
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var customerAdapter: SpotSalesCustomerAdapter
    lateinit var llOrderHistory: View
//    lateinit var customerDos: ArrayList<CustomerDo>
    private var customerDos = java.util.ArrayList<CustomerDo>()
    lateinit var llSearch : LinearLayout
    lateinit var etSearch : EditText
    lateinit var ivClearSearch : ImageView
    lateinit var ivGoBack : ImageView
    lateinit var ivSearchs : ImageView
    lateinit var tvNoOrders : TextView
    private lateinit var tvScreenTitles : TextView
    var type=0

    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.customer_screen, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        if (intent.hasExtra("TYPE")) {
            type= intent.extras?.getInt("TYPE")!!
        }


        disableMenuWithBackButton()
        changeLocale()

        initializeControls()
        ivGoBack.setOnClickListener {
            Util.preventTwoClick(it)
            finish()
        }
        if(type==1){
            tvScreenTitles.text = getString(R.string.driver_list)
        }else if(type==2){
            tvScreenTitles.text = getString(R.string.customer_listt)
        }else{
            tvScreenTitles.text = getString(R.string.supplier_list)
        }
        ivSearchs.setOnClickListener{
            Util.preventTwoClick(it)
//            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            tvScreenTitles.visibility= View.GONE
            llSearch.visibility = View.VISIBLE
    }
        ivClearSearch.setOnClickListener{
            Util.preventTwoClick(it)
            etSearch.setText("")
            if(customerDos!=null &&customerDos.size>0){
                customerAdapter = SpotSalesCustomerAdapter(this@MasterDataActivity, customerDos,"MASTER",type)
                recycleview.adapter = customerAdapter
                tvNoOrders.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            }
            else{
                tvNoOrders.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if(etSearch.text.toString().equals("", true)){
                    if(customerDos!=null &&customerDos.size>0){
                        customerAdapter = SpotSalesCustomerAdapter(this@MasterDataActivity, customerDos,"MASTER",type)
                        recycleview.adapter = customerAdapter
                        tvNoOrders.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    }
                    else{
                        tvNoOrders.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                }
                else if(etSearch.text.toString().length>2){
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
//        customerDos = StorageManager.getInstance(this).getSpotSalesCustomerList(this)
//        if(customerDos!=null &&customerDos.size>0){
//            showLoader()
//            customerAdapter = SpotSalesCustomerAdapter(this@MasterDataCustomerActivity, customerDos,"MASTER")
//            recycleview.adapter = customerAdapter
//            recycleview.visibility = View.VISIBLE
//            tvNoOrders.visibility = View.GONE
//            hideLoader()
//        }
//        else {
        if (Util.isNetworkAvailable(this)) {

            val driverListRequest = MasterDataRequest(this@MasterDataActivity, type)

            driverListRequest.setOnResultListener { isError, customerDOs ->
                hideLoader()
                if (isError) {
                    tvNoOrders.visibility = View.VISIBLE

                    Toast.makeText(this@MasterDataActivity, R.string.server_error, Toast.LENGTH_SHORT).show()
                } else {
                    customerDos = customerDOs
                    if (customerDos != null && customerDos.size > 0) {
                        StorageManager.getInstance(this).saveSpotSalesCustomerList(this, customerDos)

                        customerAdapter = SpotSalesCustomerAdapter(this@MasterDataActivity, customerDos, "MASTER",type)
                        recycleview.adapter = customerAdapter
                        tvNoOrders.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    } else {
                        tvNoOrders.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                        showToast(getString(R.string.no_data_found))
                    }
                }
            }
            driverListRequest.execute()
        } else {
            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)

        }

//        }
    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        llSearch             = findViewById<View>(R.id.llSearch) as LinearLayout
        etSearch             = findViewById<View>(R.id.etSearch) as EditText
        ivClearSearch        = findViewById<View>(R.id.ivClearSearch) as ImageView
        ivGoBack             = findViewById<View>(R.id.ivGoBack) as ImageView
        ivSearchs            = findViewById<View>(R.id.ivSearchs) as ImageView
        tvNoOrders           = findViewById<View>(R.id.tvNoOrders) as TextView
        tvScreenTitles       = findViewById<View>(R.id.tvScreenTitles) as TextView


        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@MasterDataActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
    }

    private fun filter(filtered : String) : ArrayList<CustomerDo>{
        val customerDOs = ArrayList<CustomerDo>()
        for (i in customerDos.indices){
            if(customerDos.get(i).customerName.contains(filtered, true)
                    ||customerDos.get(i).customer.contains(filtered, true)){
                customerDOs.add(customerDos.get(i))
            }
        }
        if(customerDOs.size>0){
            customerAdapter.refreshAdapter(customerDOs)
            tvNoOrders.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else{
            tvNoOrders.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun onBackPressed() {
        if (!svSearch.isIconified) {
            svSearch.isIconified = true
            return
        }
        super.onBackPressed()
    }

}
