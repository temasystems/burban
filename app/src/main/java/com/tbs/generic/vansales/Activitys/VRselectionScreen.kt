package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.alert.rajdialogs.ProgressDialog
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Adapters.VehicleRouteAdapter
import com.tbs.generic.vansales.Adapters.VrSelectionAdapter
import com.tbs.generic.vansales.Model.DriverIdMainDO
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.VRSelectionDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.DriverIdRequest
import com.tbs.generic.vansales.Requests.VRSelectionRequest
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.vr_selection_screen.*
import java.util.ArrayList


class VRselectionScreen : BaseActivity() {
    lateinit var adapter: VrSelectionAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: RecyclerView
    private var progressDialog: ProgressDialog? = null
    var selectDOS: ArrayList<VRSelectionDO>? = null

    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.vr_selection_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        disableMenuWithBackButton()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }
        initializeControls()
    }

    override fun initializeControls() {
        tv_title.text = getString(R.string.select_vr)
        recycleview = findViewById<RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview.layoutManager = linearLayoutManager
        VR()
    }

    fun VR() {
        if (Util.isNetworkAvailable(this)) {
            progressDialog?.show()
            val driverListRequest = VRSelectionRequest(this)
            driverListRequest.setOnResultListener { isError, modelDO ->
                //hideLoader()
                if (isError) {
                    progressDialog?.hide()

                    showAlert(getString(R.string.server_error))
                } else {
                    progressDialog?.hide()


                    selectDOS = modelDO.vrSelectionDOS
                    if (selectDOS != null && selectDOS!!.size > 0) {
                        adapter = VrSelectionAdapter(this@VRselectionScreen, selectDOS)
                        recycleview.adapter = adapter
                        tvNoData.visibility = View.GONE

                    } else {
                        showToast(getString(R.string.no_data_found))
                        tvNoData.visibility = View.VISIBLE
                    }

                }
            }
            driverListRequest.execute()
        } else {
            YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.internet_connection), ResultListner { `object`, isSuccess -> }, false).show((this as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
        }
    }
}