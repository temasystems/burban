package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.tbs.generic.vansales.Adapters.VehicleRouteAdapter
import com.tbs.generic.vansales.Model.PickUpDo
import com.tbs.generic.vansales.Model.RouteDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.DisplayPickupListRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*
import kotlinx.android.synthetic.main.vehicle_screen.*
import java.util.*


class TransactionList : BaseActivity() {

    private var recycleview: androidx.recyclerview.widget.RecyclerView? = null
    private var llOrderHistory: RelativeLayout? = null
    private lateinit var driverAdapter: VehicleRouteAdapter
    private var tvName: TextView? = null
    private var tvSiteId: TextView? = null
    private var llDetails: LinearLayout? = null
    private var tvNoData: TextView? = null
    private var tvlName: TextView? = null
    private var tvlSiteId: TextView? = null
    private val customerDos: List<RouteDO>? = null
    private var lllDetails: LinearLayout? = null

    lateinit var pickUpDos: ArrayList<PickUpDo>


    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.vehicle_screen, null) as RelativeLayout
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }


        //  pickUpDos = (this as VehicleRoutingList).getLoadData()
        tvName!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, ""))
        tvSiteId!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, ""))
        tvlName!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_NAME, ""))
        tvlSiteId!!.setText(preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, ""))

        // new CommonBL(OrderHistoryActivity.this, OrderHistoryActivity.this).orderHistory(userId);
        activeDocumentView()
    }

    private fun activeDocumentView() {

        ll_active_documents.setOnClickListener {
            Util.preventTwoClick(it)
            val vehicleCheckOutDo = StorageManager.getInstance(this).getVehicleCheckOutData(this)
            if (vehicleCheckOutDo.confirmCheckOutArrival.equals("", true)) {
                val vehicleCheckInDo = StorageManager.getInstance(this).getVehicleCheckInData(this);
                if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {


                    val spotRecpt = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_RECEIPT, "")
                    if (spotRecpt.isEmpty()) {
                        val shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
                        if (!shipmentId.isEmpty()) {
                            val intent = Intent(this, ActiveDeliveryActivity::class.java)
                            intent.putExtra("SHIPMENT_ID", shipmentId);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)

                        } else {
                            showToast(getString(R.string.active_doc_not_found))
                        }

                    } else {
                        showToast(getString(R.string.active_doc_not_found))
                    }
                } else {
                    YesOrNoDialogFragment().newInstance("", getString(R.string.please_finish_check_in_process), ResultListner { `object`, isSuccess -> }, false)
                }
            } else {
                YesOrNoDialogFragment().newInstance("", getString(R.string.you_have_arrived), ResultListner { `object`, isSuccess -> }, false)

            }
        }
    }

    private fun loadData() {
        if (Util.isNetworkAvailable(this)) {

            val vehicleRoutId = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            showLoader()
            if (vehicleRoutId.length > 0) {
                val driverListRequest = DisplayPickupListRequest(this, vehicleRoutId)
                driverListRequest.setOnResultListener { isError, pods ->
                    if (isError) {
                        hideLoader()
                        showToast(getString(R.string.server_erro))
                    } else {
                        pickUpDos = pods
                        if (pickUpDos.size > 0) {

//                            pickUpDos = saveShipmentListData(pods)
                            recycleview!!.setVisibility(View.VISIBLE)
                            tvNoData!!.setVisibility(View.GONE)
                            lllDetails!!.setVisibility(View.VISIBLE)

                            llDetails!!.setVisibility(View.VISIBLE)
                            driverAdapter = VehicleRouteAdapter(this@TransactionList, pods)
                            recycleview!!.adapter = driverAdapter

                        } else {
                            lllDetails!!.setVisibility(View.GONE)
                            hideLoader()

                            llDetails!!.setVisibility(View.GONE)
                            recycleview!!.setVisibility(View.GONE)
                            tvNoData!!.setVisibility(View.VISIBLE)
                        }

                    }
                }
                driverListRequest.execute()
            } else {
                showToast(getString(R.string.no_transactions_found))

            }

        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), "OK", "", "", false)

        }

    }

    fun getLoadData(): ArrayList<PickUpDo> {

        return pickUpDos;
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 19 && resultCode == 19) {
            if (AppConstants.CapturedReturns) {

                AppConstants.CapturedReturns = false
            }
        } else if (requestCode == 19 && resultCode == 52) {

        } else if (requestCode == 511 && resultCode == 511) {
            deletePOD()

        } else {

        }

    }

    override fun onResume() {
        if (Util.isNetworkAvailable(this)) {
            loadData()
        } else {
            showAlert(getString(R.string.please_check_internet_connection))
        }
        super.onResume()
    }
    fun deletePOD() {
        StorageManager.getInstance(this).deleteActiveDeliveryMainDo(this)
        StorageManager.getInstance(this).deleteDepartureData(this)
        StorageManager.getInstance(this).deleteCurrentSpotSalesCustomer(this)
        StorageManager.getInstance(this).deleteCurrentDeliveryItems(this)
        StorageManager.getInstance(this).deleteReturnCylinders(this)
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER);
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.DELIVERY_NOTE_RESHEDULE)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_SHIPMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.VALIDATED_FLAG);
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_AMOUNT);
        preferenceUtils.removeFromPreference(PreferenceUtils.NONBG);
        preferenceUtils.removeFromPreference(PreferenceUtils.PICKUP_DROP_FLAG)
        preferenceUtils.removeFromPreference(PreferenceUtils.STOP)
        preferenceUtils.removeFromPreference(PreferenceUtils.DOC_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PREVIEW_INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.EMIRATES_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.COMMENT_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.DAMAGE);
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_APPROVAL);
        preferenceUtils.removeFromPreference(PreferenceUtils.RESCHEDULE_APPROVAL);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.PAYMENT_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CustomerId);
        preferenceUtils.removeFromPreference(PreferenceUtils.ETA_ETD)


    }

    override fun initializeControls() {
        var vehicleCode = preferenceUtils!!.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
        tvScreenTitle.maxLines = 2
        tvScreenTitle.setSingleLine(false)
        tv_title.setText(getString(R.string.transaction_list) + " :" + " " + vehicleCode);
        recycleview = findViewById(R.id.recycleview)
        recycleview!!.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@TransactionList)
        tvName = findViewById(R.id.tvName) as TextView
        tvSiteId = findViewById(R.id.tvSiteId) as TextView
        tvlName = findViewById(R.id.tvlName) as TextView
        tvlSiteId = findViewById(R.id.tvlSiteId) as TextView
        llDetails = findViewById(R.id.llDetails) as LinearLayout
        lllDetails = findViewById(R.id.lllDetails) as LinearLayout

        lllDetails!!.setOnClickListener {
            Util.preventTwoClick(it)

            val latitude = 25.379465//"28.7041"
            val longitude = 55.460388//"77.1025"
            val gmmIntentUri = "google.navigation:q=" + latitude + "," + longitude + "&mode=d"
            val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse(gmmIntentUri))
            mapIntent.setPackage("com.google.android.apps.maps")
            try {
                if (mapIntent.resolveActivity(packageManager) != null) {
                    startActivityForResult(mapIntent, 1542)
                    onUserLeaveHint()
                }
            } catch (e: Exception) {
                Log.e("Map Loading", "onClick: NullPointerException: Couldn't open map." + e.message)
                Toast.makeText(this, "Couldn't open map", Toast.LENGTH_SHORT).show()
            }
        }

        tvNoData = findViewById(R.id.tvNoData) as TextView

    }



    override fun onButtonYesClick(from: String) {
        super.onButtonYesClick(from)
        if (from.equals("PLAYSTORE", true)) {
            val appPackageName = packageName // getPackageName() from Context or Activity object
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }
    }
}
