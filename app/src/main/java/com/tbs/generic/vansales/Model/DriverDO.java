package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class DriverDO implements Serializable {


    public String vehicleRouteId = "";
    public String vehicleCode    = "";
    public String vehicleCarrier = "";
    public String site = "";
    public String siteId = "";
    public String aDate = "";
    public String dDate = "";
    public String plate = "";
    public String dTime = "";
    public String aTime    = "";
    public String nShipments = "";
    public String tripNumber = "";
    public String driver = "";
    public String reference = "";
    public String driverName = "";
    public String notes          = "";
    public String location          = "";
    public int checkinFlag          = 0;
    public String srcNumber          = "";


}
