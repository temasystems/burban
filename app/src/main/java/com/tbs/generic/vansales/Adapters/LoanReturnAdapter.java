package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.InspectionDetailsActivity;
import com.tbs.generic.vansales.Activitys.PurchaseRecieptInfoActivity;
import com.tbs.generic.vansales.Activitys.UpdateSerializationActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.LoanReturnDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class LoanReturnAdapter extends RecyclerView.Adapter<LoanReturnAdapter.MyViewHolder> {
    private LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDosMap;
    private String from;
    private Context context;
    ArrayList<LoanReturnDO> selectedLoanReturnDOs;

    public void refreshAdapter(@NotNull LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDosMap, @NotNull String from) {
        this.loanReturnDosMap = loanReturnDosMap;
        this.from = from;

        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvShipmentId;
        private RecyclerView rvReturnProducts;

        public MyViewHolder(View view) {
            super(view);
            tvShipmentId = view.findViewById(R.id.tvShipmentId);
            rvReturnProducts = view.findViewById(R.id.rvReturnProducts);

        }
    }


    public LoanReturnAdapter(Context context, LinkedHashMap<String, ArrayList<LoanReturnDO>> loanReturnDosMap, String from) {
        this.context = context;
        this.loanReturnDosMap = loanReturnDosMap;
        this.from = from;
    }


    public ArrayList<LoanReturnDO> getSelectedLoadStockDOs() {
        return selectedLoanReturnDOs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.capture_return_list_cell, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ArrayList<String> keySet = new ArrayList<>(loanReturnDosMap.keySet());
        ArrayList<LoanReturnDO> loanReturnDos = loanReturnDosMap.get(keySet.get(position));
        holder.tvShipmentId.setText(context.getString(R.string.document) +" : "+ keySet.get(position));
        holder.rvReturnProducts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        holder.rvReturnProducts.setAdapter(new ReturnProductsListAdapter(context, loanReturnDos));


//        if (loanReturnDos != null && loanReturnDos.size() > 0 && loanReturnDos.get(0).type == 1) {
//            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_gray));
//        } else {
//            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_secnd_grey));
//        }
    }

    @Override
    public int getItemCount() {
        return loanReturnDosMap != null ? loanReturnDosMap.keySet().size() : 0;
    }

    private class ReturnProductsListAdapter extends RecyclerView.Adapter<ReturnListHolder> {
        private ArrayList<LoanReturnDO> loanReturnDos;
        private Context context;

        ReturnProductsListAdapter(Context context, ArrayList<LoanReturnDO> loanReturnDos) {
            this.context = context;
            this.loanReturnDos = loanReturnDos;
        }

        @NonNull
        @Override
        public ReturnListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.loan_return_data, viewGroup, false);
            return new ReturnListHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ReturnListHolder holder, int position) {
            if (selectedLoanReturnDOs == null) {
                selectedLoanReturnDOs = new ArrayList<>();
            }
            final LoanReturnDO loanReturnDO = loanReturnDos.get(position);
            holder.tvProductName.setText(loanReturnDO.productName);
            holder.tvSerial.setText(loanReturnDO.serialDO);


            holder.tvProductDescription.setText(loanReturnDO.productDescription);
            int qty = (int) loanReturnDO.qty;
            holder.etNumberET.setText(qty + "");

            if (from.equalsIgnoreCase("Products")) {
                holder.cbSelected.setVisibility(View.VISIBLE);
                holder.ivAdd.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.GONE);
                holder.etNumberET.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.GONE);
                holder.btnInspection.setVisibility(View.GONE);

                holder.tvNumber.setVisibility(View.GONE);
                holder.tvNumber.setText("" + qty + " " + loanReturnDO.unit);
                holder.tvNumber.setEnabled(false);
            }else {
                holder.cbSelected.setVisibility(View.GONE);
                holder.ivAdd.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.GONE);
                holder.etNumberET.setVisibility(View.GONE);
                holder.ivRemove.setVisibility(View.GONE);
            }
            if (selectedLoanReturnDOs != null && selectedLoanReturnDOs.contains(loanReturnDO)) {
                holder.cbSelected.setChecked(true);
            } else {
                holder.cbSelected.setChecked(false);
            }
            if (loanReturnDO.state ==2) {
                holder.cbSelected.setChecked(true);
                boolean state=false;
                for(int i=0;i<selectedLoanReturnDOs.size();i++){
                    if(selectedLoanReturnDOs.get(i).serialDO.equals(loanReturnDO.serialDO)){
                        state=true;
                        break;
                    }
                }
                if(state==false){

                    selectedLoanReturnDOs.add(loanReturnDO);
                }
                loanReturnDO.isChecked = true;
                holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background2));


            } else {
                holder.cbSelected.setChecked(false);

            }
            holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        selectedLoanReturnDOs.add(loanReturnDO);
                    } else {
                        selectedLoanReturnDOs.remove(loanReturnDO);
                    }
                }
            });

            holder.btnInspection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent= new Intent(context, InspectionDetailsActivity.class);
                    intent.putExtra("PRODUCT_CODE",loanReturnDO.productName);
                    intent.putExtra("INPUT_FLAG",loanReturnDO.serialFlag);
//                    intent.putExtra("INPUT_FLAG",2);
                    intent.putExtra("LINE_NUMBER",loanReturnDO.lineNumber);

                    intent.putExtra("PRODUCT",loanReturnDO.productDescription);
                    intent.putExtra("PRODUCTDO", loanReturnDO);
                    intent.putExtra("SHIPMENT_NUMBER", loanReturnDO.shipmentNumber);


                    ((BaseActivity) context).startActivityForResult(intent,10);
                }
            });
            int maxCount = (int) loanReturnDO.qty;//Integer.parseInt(holder.tvNumberET.getText().toString());
            final int[] quantity = {(int) loanReturnDO.qty};
            holder.ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (quantity[0] < maxCount) {
                        quantity[0] = quantity[0] + 1;
                        holder.etNumberET.setText("" + (quantity[0]++));
                        loanReturnDO.qty = quantity[0];
                    }
                }
            });
            holder.ivRemove.setOnClickListener(view -> {
                if (quantity[0] > 1) {
                    quantity[0] = quantity[0] - 1;
                    holder.etNumberET.setText("" + quantity[0]);
                    loanReturnDO.qty = quantity[0];
                }
            });
            holder.etNumberET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equalsIgnoreCase("")) {
                        if (Integer.parseInt(s.toString()) <= maxCount && Integer.parseInt(s.toString()) > 0) {
                            quantity[0] = Integer.parseInt(s.toString());
                            loanReturnDO.qty = quantity[0];
                        } else {
                            final String newText = s.toString().substring(0, s.length() - 1) + "";
                            holder.etNumberET.setText("" + newText);
                        }
                    }
                }
            });


        }

        @Override
        public int getItemCount() {
            return loanReturnDos != null ? loanReturnDos.size() : 0;
        }
    }

    private class ReturnListHolder extends RecyclerView.ViewHolder {
        private TextView tvShipmentId, tvProductName, tvSerial,tvProductDescription, tvLineNumber, tvNumber;
        private CheckBox cbSelected;
        private EditText etNumberET;
        Button btnInspection,btnSerialLot;
        private ImageView ivAdd, ivRemove;

        public ReturnListHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);
            etNumberET = itemView.findViewById(R.id.tvNumberET);
            cbSelected = itemView.findViewById(R.id.cbSelected);
            tvNumber = itemView.findViewById(R.id.tvNumber);
            tvLineNumber = itemView.findViewById(R.id.tvLineNumber);
            btnInspection = itemView.findViewById(R.id.btnInspection);
            btnSerialLot = itemView.findViewById(R.id.btnSerialLot);
            ivAdd = itemView.findViewById(R.id.ivAdd);
            ivRemove = itemView.findViewById(R.id.ivRemove);
            tvSerial = itemView.findViewById(R.id.tvSerial);

        }
    }
}
