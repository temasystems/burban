package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.NonSheduledProductDO;
import com.tbs.generic.vansales.Model.CustomProductDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class CreateDeliveryProductAdapter extends RecyclerView.Adapter<CreateDeliveryProductAdapter.MyViewHolder> {

    private List<NonSheduledProductDO> productDOS;
    private Context context;
    private ArrayList<String> customProductDOS;

    List<CustomProductDO> list;
    CustomProductDO aa;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvNumber;
        private LinearLayout llDetails;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvProductName = view.findViewById(R.id.tvProductName);
            ivRemove = view.findViewById(R.id.ivRemove);
            ivAdd = view.findViewById(R.id.ivAdd);
            tvNumber = view.findViewById(R.id.tvNumber);

        }
    }


    public CreateDeliveryProductAdapter(Context context, List<NonSheduledProductDO> productDOS) {
        this.context = context;
        this.productDOS = productDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_product_data, parent, false);
        list = new ArrayList<CustomProductDO>();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final NonSheduledProductDO productDO = productDOS.get(position);
        holder.tvProductName.setText(productDO.item+"\n"+productDO.itemDescription);
        holder.tvNumber.setText("" + productDO.quantity);

        PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productDO.quantity = productDO.quantity + 1;
                holder.tvNumber.setText("" + productDO.quantity);

                if (list.size() > 0) {
                    ListIterator<CustomProductDO> iterator = list.listIterator();

                    if (iterator.hasNext()) {


                        CustomProductDO customProductDO = iterator.next();
                        if (customProductDO.productName.equals(productDOS.get(position).item)) {
                            aa.productName = productDO.item;
                            aa.itemCount = productDO.quantity;
                            aa.productId = productDO.itemDescription;                       //     iterator.set("New");
                            iterator.set(aa);
                        }

//
//
//                        if (list.contains(productDOS.get(position).productName)) {
//                            aa.itemCount = productDO.itemCount;
//                            list.add(aa);
//                            preferenceUtils.saveString(PreferenceUtils.WEIGHT, String.valueOf(list));
//                            Log.d("Check1", "" + list);
//                        }
                        else {
                            aa             = new CustomProductDO();
                            aa.productName = productDO.item;
                            aa.itemCount   = productDO.quantity;
                            aa.productId   = productDO.itemDescription;
                            list.add(aa);
                            Log.d("Check2", "" + list);

                        }


                    }
                } else {
                    aa = new CustomProductDO();

                    aa.productName = productDO.item;
                    aa.itemCount = productDO.quantity;
                    aa.productId = productDO.itemDescription;
                    list.add(aa);
                    Log.d("Check2", "" + list);
                    //lap,sweater,design

                }


//                list.add(arr2);
//                list.add(arr3);
            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (productDO.quantity > 0) {
                    productDO.quantity = productDO.quantity - 1;

                    holder.tvNumber.setText("" + productDO.quantity);
                    if (list.size() > 0) {
                        ListIterator<CustomProductDO> iterator = list.listIterator();
                        while (iterator.hasNext()) {
                            CustomProductDO next = iterator.next();
                            if (next.productName.equals(productDOS.get(position).item)) {
                                aa.productName = productDO.item;
                                aa.itemCount = productDO.quantity;
                                aa.productId = productDO.itemDescription;                       //     iterator.set("New");
                                iterator.set(aa);
                            }

//
//
//                        if (list.contains(productDOS.get(position).productName)) {
//                            aa.itemCount = productDO.itemCount;
//                            list.add(aa);
//                            preferenceUtils.saveString(PreferenceUtils.WEIGHT, String.valueOf(list));
//                            Log.d("Check1", "" + list);
//                        }
                            else {
                                aa = new CustomProductDO();
                                aa.productName = productDO.item;
                                aa.itemCount = productDO.quantity;
                                aa.productId = productDO.itemDescription;
                                list.remove(aa);
                                Log.d("Check2", "" + list);

                            }

                        }
                    } else {
                        aa = new CustomProductDO();
                        aa.productName = productDO.item;
                        aa.itemCount = productDO.quantity;
                        aa.productId = productDO.itemDescription;
                        list.remove(aa);
                        Log.d("Check2", "" + list);

                    }
                }
            }
        });
//        ((ScheduledCaptureDeliveryActivity) context).btnCompleted.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Bundle args = new Bundle();
////                args.putSerializable("ARRAYLIST", (Serializable) list);
////                Intent i = new Intent(context, LoadVanSalesActivity.class);
////                i.putExtra("ProductObject", (Serializable) list);
////                context.startActivity(i);
//
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return productDOS.size();
    }

}
