package com.tbs.generic.vansales.Requests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.alert.rajdialogs.ProgressDialog;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ConfigurationDo;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.ProgressDialogTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.Util;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


public class ConfigurationRequest extends AsyncTask<String, Void, Boolean> {
    boolean isValid = false;
    ConfigurationDo configurationDo;
    private Context context;

    public ConfigurationRequest(Context context) {
        this.context = context;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void OnCompleted(boolean isError, boolean isSuccess, ConfigurationDo configurationDo);
    }

    public boolean runRequest(String address, String port, String pool, String username, String Password, String company) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        String URL = ServiceURLS.HTTP + address  + ServiceURLS.APP_SUB_URL;//https
        String URL = ServiceURLS.HTTP + address + ServiceURLS.COLON + port + ServiceURLS.APP_SUB_URL;

//// Load CAs from an InputStream
//// (could be from a resource or ByteArrayInputStream or ...)
//        CertificateFactory cf = CertificateFactory.getInstance("X.509");
//// From https://www.washington.edu/itconnect/security/ca/load-der.crt
//        InputStream caInput = null;
//        try {
//            caInput = context.getAssets().open("demo.crt");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        Certificate ca;
//        try {
//            ca = cf.generateCertificate(caInput);
//            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//// Create a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//// Create an SSLContext that uses our TrustManager
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, tmf.getTrustManagers(), null);
//            java.net.URL url = new URL(URL);
//            HttpsURLConnection urlConnection =
//                    (HttpsURLConnection)url.openConnection();
//            urlConnection.setSSLSocketFactory(context.getSocketFactory());
//            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
////            InputStream in = urlConnection.getInputStream();
//        } finally {
//            caInput.close();
//        }


        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.CONFIGURATION);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XCPY", company);
            jsonObject.put("I_XIP", address);
            jsonObject.put("I_XPORT", port);
            jsonObject.put("I_XFOLDER", pool);
            jsonObject.put("I_XUSR", username);
            jsonObject.put("I_PWD", Password);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", AppPrefs.getString(AppPrefs.SERVICE_LANAGUAGE, Constants.ENG));
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", Password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

//        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

//        ((BaseActivity)context).allowAllSSL();

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + Password).getBytes())));
            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (Exception e) {
//            ((BaseActivity)context).showToast(""+e.getMessage());
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            configurationDo = new ConfigurationDo();
            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.equalsIgnoreCase("1")) {
                                isValid = true;
                            }
                        }
                        if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if (text.length() > 0) {
                                configurationDo.companyImg = text;
                            }
                        }
                        if (attribute.equalsIgnoreCase("O_XLDES")) {
                            if (text.length() > 0) {
                                configurationDo.companyDescription = text;
                            }
                        }
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }
                eventType = xpp.next();
            }

            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);
            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressDialogTask.getInstance().showProgress(context, false, "");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        try {
            return runRequest(param[0], param[1], param[2], param[3], param[4], param[5]);
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ProgressDialogTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.OnCompleted(!result, isValid, configurationDo);
        }
    }
}