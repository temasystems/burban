package com.tbs.generic.vansales.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.util.ArrayList

//
class NewAvailableStockActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()

    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.available_stock, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }
    }

    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.available_stock_title)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

       loadNonVehicleStockData()


    }

    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

//            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            if (scheduledRootId.length > 0) {
                val loadVanSaleRequest = CurrentScheduledStockRequest(scheduledRootId, this@NewAvailableStockActivity)
                showLoader()

                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast(getString(R.string.no_vehicle_data_found))
                    } else {
                        hideLoader()

                        scheduleDos = loadStockMainDo.loadStockDOS

                    }
                    loadNonVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonVehicleStockData()

            }


        } else {
            showToast(getString(R.string.no_internet))
        }
    }

    private fun loadNonVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
                val loadVanSaleRequest = CurrentVehicleStockRequest("", this@NewAvailableStockActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast(getString(R.string.no_vehicle_data_found))
                    } else {
                        hideLoader()
                        nonScheduleDos = loadStockMainDo.loadStockDOS
                        var isProductExisted = false
                        availableStockDos = nonScheduleDos
                        var loadStockAdapter = LoadStockAdapter(this@NewAvailableStockActivity, availableStockDos, "Shipments")
                        recycleview.adapter = loadStockAdapter
                    }
                }
                loadVanSaleRequest.execute()
        } else {
            showToast(getString(R.string.no_internet))
        }
    }
}