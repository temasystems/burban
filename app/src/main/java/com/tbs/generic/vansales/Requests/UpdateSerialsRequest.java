package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.SerialListDO;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;


public class UpdateSerialsRequest extends AsyncTask<String, Void, Boolean> {
    private Context mContext;
    SuccessDO successDO;
    int flag;
    ActiveDeliveryDO trailerSelectionDO;
    public UpdateSerialsRequest(Context mContext,int flag,
                                ArrayList<SerialListDO> serialListDOArrayList, ActiveDeliveryDO trailerSelectionDO) {
        this.mContext = mContext;
        this.flag = flag;

        this.serialListDOS = serialListDOArrayList;
        this.trailerSelectionDO = trailerSelectionDO;
    }

    private ArrayList<SerialListDO>  serialListDOS;

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, SuccessDO contractMainDo);
    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        String id = ((BaseActivity)mContext).preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "");
        String shipmentId = ((BaseActivity)mContext).preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
        String vr = ((BaseActivity)mContext).preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
        int type = ((BaseActivity)mContext).preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
        String loan = ((BaseActivity)mContext).preferenceUtils.getStringFromPreference(PreferenceUtils.LOAN_DELIVERY, "");

        try {
            if(type==2){
                jsonObject.put("I_YSDHNUM",loan);

            }else {
                jsonObject.put("I_YSDHNUM",shipmentId);

            }
            jsonObject.put("I_YSERUPDFLG",flag);

            jsonObject.put("I_YCODEYVE",id);
            jsonObject.put("I_YSDDLIN",trailerSelectionDO.linenumber);
            jsonObject.put("I_YQTY",trailerSelectionDO.orderedQuantity);
            jsonObject.put("I_YVEHROU",vr);

            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i <serialListDOS.size(); i++) {

                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("I_YSERNUM", serialListDOS.get(i).serialDO);
                jsonArray.put(i, jsonObject1);
            }
            jsonObject.put("GRP2", jsonArray);

        } catch (Exception e) {
            return false;
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.FIN_SAVE_SERIAL, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }
    public boolean parseXML(String xmlString) {
        System.out.println("create delivery xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YSTATUS")) {
                            if(text.length()>0){
                                successDO.flag = Integer.parseInt(text);

                            }


                        }
                        if (attribute.equalsIgnoreCase("O_MESSAGE")) {
                            if(text.length()>0){
                                successDO.successFlag = text;

                            }


                        }

                        text="";

                    }



                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressTask.getInstance().showProgress(mContext, false, "");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}