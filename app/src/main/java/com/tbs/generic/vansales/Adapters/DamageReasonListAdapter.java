package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.DamageReasonsActivity;
import com.tbs.generic.vansales.Model.ReasonDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class DamageReasonListAdapter extends RecyclerView.Adapter<DamageReasonListAdapter.MyViewHolder>  {

    private ArrayList<ReasonDO> reasonDoS;
    private Context context;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);



        }
    }


    public DamageReasonListAdapter(Context context, ArrayList<ReasonDO> reasonDOS) {
        this.context = context;
        this.reasonDoS = reasonDOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.simple_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ReasonDO reasonDO = reasonDoS.get(position);

        holder.tvName.setText(""+reasonDO.reason);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = position + 1;

                ((DamageReasonsActivity)context).preferenceUtils.saveInt(PreferenceUtils.DAMAGE_REASON_ID,pos);

                ((DamageReasonsActivity)context).finish();
            }
        });


    }

    @Override
    public int getItemCount() {
        return reasonDoS.size();
    }

}
