package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class InspectionMainDO implements Serializable {

    public String name = "";
    public int flag = 0;
    public String message = "";

    public int type=0;
    public String contractNumber = "";
    public String preparationNumber = "";
    public String lineNumber = "";
    public String equipment = "";
    public String user = "";
    public String date = "";
    public String time = "";
    public String inspectionStatus = "";
    public String productDescription = "";
    public String productImage = "";
    public int bookingQty = 0;
    public String serialNumber = "";
    public String customer = "";
    public ArrayList<InspectionDO> inspectionDOS = new ArrayList<>();



}
