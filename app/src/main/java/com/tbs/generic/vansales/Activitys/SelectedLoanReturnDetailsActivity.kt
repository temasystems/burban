package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Adapters.LoanReturnAdapter
import com.tbs.generic.vansales.Model.FileDetails
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoanReturnDO
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import com.tbs.generic.vansales.utils.WebServiceConstants.LOAN_RETURN
import kotlinx.android.synthetic.main.include_image_caputure.*
import kotlinx.android.synthetic.main.include_signature.*
import kotlinx.android.synthetic.main.include_signature.btnSignature
import kotlinx.android.synthetic.main.include_signature.imv_signature
import kotlinx.android.synthetic.main.include_toolbar.*
import kotlinx.android.synthetic.main.reading_dialog.*
import kotlinx.android.synthetic.main.scheduled_capture_delivery.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap


class SelectedLoanReturnDetailsActivity : BaseActivity() {
    lateinit var invoiceAdapter: LoanReturnAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView

    private var type : Int = 1

    private lateinit var btnConfirm: Button
    private lateinit var btnComment: Button
    lateinit var podDo: PodDo
    private var signature: String? = null

    private lateinit var rbEmptyCylinder: RadioButton
    private lateinit var rbSalesReturn: RadioButton
    private var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoanReturnDO>> = LinkedHashMap()
    private var loanReturnDos: ArrayList<LoanReturnDO> = ArrayList()
    lateinit var fileDetailsDo: ArrayList<FileDetails>
    private var fileDetailsList: java.util.ArrayList<FileDetails>? = null
    private lateinit var filesPreviewAdapter: FilesPreviewAdapter
    override fun initialize() {
      val  llCategories = layoutInflater.inflate(R.layout.selected_salesreturn_details, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            finish()
        }

        initializeControls()
        if (intent.hasExtra("Products")) {
            loanReturnDos = intent.getSerializableExtra("Products") as ArrayList<LoanReturnDO>
        }


        loanReturnDOsMap = LinkedHashMap<String, ArrayList<LoanReturnDO>>()

        bindData(loanReturnDos)
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        fileDetailsDo = StorageManager.getInstance(this).getLoanReturnImagesData(this)
        llnoteFab.setOnClickListener {
            var intent = Intent(this@SelectedLoanReturnDetailsActivity, PODNotesActivity::class.java);
            intent.putExtra("TYPE",2)
            startActivityForResult(intent, 140);
        }

        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            setUpImagesAs64Bit()
            if (!lattitudeFused.isNullOrEmpty()) {
                stopLocationUpdates()
                hideLoader()
              creation()
            } else {
                showLoader()
                stopLocationUpdates()
                location(ResultListner { `object`, isSuccess ->
                    if (isSuccess) {
                        hideLoader()
                        stopLocationUpdates()
                      creation()

                    }
                })
            }

        }
        btnSignature.setOnClickListener {
            val intent = Intent(this, CommonSignatureActivity::class.java)
            intent.putExtra("LR",1)
            startActivityForResult(intent, 9907)
        }
        imagesSetUp()
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4 - fileDetailsList!!.size)
            } else {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4)
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }

    }
    fun creation(){

        if(loanReturnDOsMap!=null && loanReturnDOsMap.size>0){
            StorageManager.getInstance(this).saveReturnCylinders(this, loanReturnDos)
            val returnCount = StorageManager.getInstance(this).saveAllReturnStockValue(loanReturnDos)
            if(returnCount>0){
                var recievedSite = preferenceUtils.getStringFromPreference(PreferenceUtils.B_SITE_ID, "")
                var vehicleCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")

                val customer = preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER, "")
                if (Util.isNetworkAvailable(this)) {
                    val siteListRequest = CreateLoanReturnRequest( recievedSite,customer,vehicleCode,type,loanReturnDOsMap,this@SelectedLoanReturnDetailsActivity)
                    showLoader()
                    siteListRequest.setOnResultListener { isError, loanReturnMainDo ->
                        hideLoader()
                        if (loanReturnMainDo != null) {
                            if (isError) {
                                showToast(getString(R.string.server_error))
                            } else {
                                if(loanReturnMainDo.flag==2){
                                    podDo!!.podTimeCaptureLRCaptureDeliveryTime= CalendarUtils.getTime()
                                    podDo!!.podTimeCaptureLRCaptureDeliveryDate = CalendarUtils.getDate()
                                    StorageManager.getInstance(this).saveDepartureData(this, podDo)
                                    StorageManager.getInstance(this).deleteLoanDeliveryImagesData(this)
                                    val intent = Intent()
                                    intent.putExtra("CapturedReturns", true)

                                    intent.putExtra("CRELAT", lattitudeFused)
                                    intent.putExtra("CRELON", longitudeFused)
                                    intent.putExtra("RATING", podDo!!.loanReturnRating)
                                    intent.putExtra("NOTES", podDo!!.loanReturnnotes)
                                    intent.putExtra("NAME", podDo!!.loanReturnname)
                                    setResult(11, intent)
                                    showToast(loanReturnMainDo.message)
                                    preferenceUtils.saveString(PreferenceUtils.LOAN_RETURN, loanReturnMainDo.number)
                                    finish()
                                }else{
                                    showAlert(loanReturnMainDo.message)
                                }

                            }
                        } else {
                            showToast(getString(R.string.server_error))
                        }

                    }
                    siteListRequest.execute()

                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.no_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }



            }
            else{
                showToast(getString(R.string.unbale_to_create_loan))
            }
        }
    }
    private fun setUpImagesAs64Bit() {
        podDo!!.capturedImagesLRListBulk = ArrayList()
        for (list in this.fileDetailsList!!) {
            try {
                val fis = FileInputStream(File(list.filePath))
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                LogUtils.debug("PIC-->", encImage)
                podDo!!.capturedImagesLRListBulk!!.add(encImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        StorageManager.getInstance(this).saveDepartureData(this@SelectedLoanReturnDetailsActivity, podDo)

    }



    override fun initializeControls() {
        tv_title.text = getString(R.string.loan_return)
        recycleview    = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvNoDataFound  = findViewById<TextView>(R.id.tvNoDataFound)

        recycleview.layoutManager = linearLayoutManager
        btnConfirm     = findViewById<Button>(R.id.btnConfirm)
        rbEmptyCylinder  = findViewById<RadioButton>(R.id.rbEmptyCylinder)
        rbSalesReturn    = findViewById<RadioButton>(R.id.rbSalesReturn)
        btnComment     = findViewById<Button>(R.id.btnComment)
        btnComment.setOnClickListener {
            Util.preventTwoClick(it)
            showAddItemDialog()

        }


    }

    private fun bindData(loanReturnDos: ArrayList<LoanReturnDO>){

        val shipmentIds = ArrayList<String>()
        if(loanReturnDos!=null && loanReturnDos.size>0){
            for (i in loanReturnDos.indices){
                if(!shipmentIds.contains(loanReturnDos.get(i).shipmentNumber)){
                    shipmentIds.add(loanReturnDos.get(i).shipmentNumber)
                }
            }
        }
        if(shipmentIds.size>0){
            for (j in shipmentIds.indices){
                val returnList = ArrayList<LoanReturnDO>()
                for (i in loanReturnDos.indices) {
                    if (loanReturnDos.get(i).shipmentNumber.equals(shipmentIds.get(j))) {
                        returnList.add(loanReturnDos.get(i))
                    }
                }
                loanReturnDOsMap.put(shipmentIds.get(j), returnList)
            }
        }
        if(loanReturnDOsMap.size>0){
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
            invoiceAdapter = LoanReturnAdapter(this@SelectedLoanReturnDetailsActivity, loanReturnDOsMap,"salesreturn")
            recycleview.adapter = invoiceAdapter
        }else{
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
            btnConfirm.visibility = View.GONE
        }

    }

    private fun imagesSetUp() {

        fileDetailsList = fileDetailsDo
        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = View.VISIBLE

            } else {
                btnAddImages.visibility = View.GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = View.VISIBLE
            } else {
                recycleviewImages.visibility = View.GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter
        if (fileDetailsList!!.size > 0) {
            recycleviewImages.visibility = View.VISIBLE

        }
    }
    private fun showAddItemDialog() {
        try {
            var dialog = AppCompatDialog(this, R.style.AppCompatAlertDialogStyle)
            dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(this).inflate(R.layout.comment_custom, null)
//            applyFont(view, AppConstants.MONTSERRAT_REGULAR_TYPE_FACE)
            var etAdd = view.findViewById<View>(R.id.etAdd) as EditText
            var comment =preferenceUtils.getStringFromPreference(PreferenceUtils.COMMENT_ID,"")
            if(comment.isNotEmpty()){
                etAdd.setText(comment)
                val pos: Int = etAdd.getText().length
                etAdd.setSelection(pos)
            }

            dialog.setCancelable(true)
            dialog.setCanceledOnTouchOutside(true)
            val btnSubmit = view.findViewById<View>(R.id.btnSubmit) as Button




            btnSubmit.setOnClickListener {
                Util.preventTwoClick(it)
                val itemName = etAdd.text.toString().trim()

                if (itemName.equals("", ignoreCase = true)) {
                    showToast(getString(R.string.please_enter_comment))
                } else {

                    preferenceUtils.saveString(PreferenceUtils.COMMENT_ID, itemName)
                    dialog.dismiss()
                }
            }
            dialog.setContentView(view)
            if (!dialog.isShowing)
                dialog.show()
        } catch (e: Exception) {
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 9907) {
            signature = data!!.getStringExtra("Signature")
            Log.d("Signature----->", signature)
            var rating = data!!.getStringExtra("Rating")

            podDo!!.loanReturnRating = rating
            podDo!!.loanReturnsignature = signature
            var name = data!!.getStringExtra("Name")

            podDo!!.loanReturnname = name

            StorageManager.getInstance(this).saveDepartureData(this@SelectedLoanReturnDetailsActivity, podDo)
            val decodedString = android.util.Base64.decode(signature, android.util.Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            imv_signature.visibility = View.VISIBLE
            imv_signature?.setImageBitmap(decodedByte)

        }
        if (resultCode == RESULT_OK && requestCode == 888) {
            val images = data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {
                var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    fileDetailsList?.add(0, fileDetails)
                    StorageManager.getInstance(this).saveLoanReturnImagesData(this,fileDetailsList)

                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }

        if (requestCode == 140 && resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra(resources.getString(R.string.notes_remarks))
            LogUtils.debug(resources.getString(R.string.notes_remarks), returnString)
            podDo!!.loanReturnnotes = returnString;
            StorageManager.getInstance(this).saveDepartureData(this@SelectedLoanReturnDetailsActivity, podDo)
        }
    }

}