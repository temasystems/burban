package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.app.Dialog;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.ScheduledCaptureDeliveryActivity;
import com.tbs.generic.vansales.Activitys.SerializationActivity;
import com.tbs.generic.vansales.Activitys.UpdateSerializationActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.ReasonDO;
import com.tbs.generic.vansales.Model.ReasonMainDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.listeners.LoadStockListener;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class ScheduledProductsAdapter extends RecyclerView.Adapter<ScheduledProductsAdapter.MyViewHolder> implements Filterable {
    public ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    private String imageURL;
    private double weight = 0;
    private double volume = 0;
    ValueFilter valueFilter;
    private Context context;
    private ReasonMainDO reasonMainDo;
    private LoadStockListener loadStockListener;
    double mass = 0.0;

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<ActiveDeliveryDO> filterList = new ArrayList<>();
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    if ((activeDeliveryDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(activeDeliveryDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = activeDeliveryDOS.size();
                results.values = activeDeliveryDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            activeDeliveryDOS = (ArrayList<ActiveDeliveryDO>) results.values;
            notifyDataSetChanged();
        }

    }

    public void refreshAdapter(ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
        this.activeDeliveryDOS = activeDeliveryDOS;
        notifyDataSetChanged();
    }

    public ArrayList<ActiveDeliveryDO> selectedactiveDeliveryDOs = new ArrayList<>();
    private String from = "";

    public ArrayList<ActiveDeliveryDO> getSelectedactiveDeliveryDOs() {
        return selectedactiveDeliveryDOs;
    }

    public ScheduledProductsAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS, String from) {
        this.context = context;
        this.activeDeliveryDOS = activeDeliveryDOS;
        this.from = from;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_stock_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        } else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_secnd_grey));
        }


        final ActiveDeliveryDO activeDeliveryDO = activeDeliveryDOS.get(position);
        holder.tvProductName.setText(activeDeliveryDO.product);
        holder.tvDescription.setText(activeDeliveryDO.productDescription);
        if(activeDeliveryDO.lineText.isEmpty()){
            holder.tvText.setVisibility(View.GONE);
        }else {
            holder.tvText.setText(activeDeliveryDO.lineText);

        }

        if (from.equalsIgnoreCase("AddProducts")) {
            if (activeDeliveryDO.totalQuantity > 0) {
                activeDeliveryDO.orderedQuantity = 1;
                holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
            } else {
                activeDeliveryDO.orderedQuantity = 0;
                holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
            }
        } else {
            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);

        }
        holder.tvAvailableQty.setText(context.getString(R.string.stock) + " : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
        int pos = position + 1;
        int num = pos * 1000;
        activeDeliveryDO.line = num;

        int reasonId = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.DAMAGE_REASON_ID, 0);
        if (reasonId > 0) {
            String reason = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.DAMAGE_REASON, "");

            holder.tvSelection.setText("" + reason);
        }
        holder.tvSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.llSelectReason.performClick();
            }
        });
        ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(context).getActiveDeliveryMainDo(context);
        if (activeDeliverySavedDo.deliveryType == 2 || !activeDeliverySavedDo.loandelivery.isEmpty()) {
            if (!from.equalsIgnoreCase("Shipments")&&activeDeliveryDO.validatedFlag != 2) {
                holder.btnSerialLot.setVisibility(View.VISIBLE);

            }
            else {
                holder.btnSerialLot.setVisibility(View.GONE);

            }
        } else {
            holder.btnSerialLot.setVisibility(View.GONE);

        }
        holder.btnSerialLot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UpdateSerializationActivity.class);
                intent.putExtra("PRODUCTDO", activeDeliveryDO);
                ((BaseActivity) context).startActivityForResult(intent, 10);


            }
        });

        //need to get it from non-schedule product's qty
        holder.llSelectReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog quantityDialog = new Dialog(context);
                quantityDialog.setCancelable(true);
                quantityDialog.setCanceledOnTouchOutside(true);
                quantityDialog.setContentView(R.layout.simple_list);
                ListView list = quantityDialog.findViewById(R.id.lvItems);
                ReasonAdapter reasonAdapter = new ReasonAdapter();
                if (reasonMainDo != null && reasonMainDo.reasonDOS.size() > 0) {
                    list.setAdapter(reasonAdapter);
                } else {
                    ((BaseActivity) context).showToast(context.getString(R.string.no_reason_found));
                }
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        quantityDialog.dismiss();
                        holder.tvSelection.setText(reasonMainDo.reasonDOS.get(i).reason);

                    }
                });
                quantityDialog.show();
            }
        });

        String shipmentProductsType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "");
        activeDeliveryDO.shipmentProductType = shipmentProductsType;
        String shipmentType = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, "");
        if (from.equalsIgnoreCase("AddProducts")) {
            holder.cbSelected.setVisibility(View.VISIBLE);
        } else if (from.equalsIgnoreCase("Shipments")) {
            holder.tvNumberET.setVisibility(View.GONE);
            holder.ivRemove.setVisibility(View.GONE);
            holder.ivAdd.setVisibility(View.GONE);
            holder.tvAvailableQty.setVisibility(View.GONE);
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.cbSelected.setVisibility(View.GONE);
            holder.tvNumber.setText("Qty : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);

        }
//        else {
//            if(shipmentType.equalsIgnoreCase(context.getResources().getString(R.string.checkin_scheduled))){
//                holder.cbSelected.setVisibility(View.GONE);
//            }
//            else {
//                holder.cbSelected.setVisibility(View.VISIBLE);
//            }
//        }

        if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.FixedQuantityProduct)
                && !from.equalsIgnoreCase("Shipments")) {
            holder.tvNumberET.setVisibility(View.VISIBLE);
            holder.ivRemove.setVisibility(View.VISIBLE);
            holder.ivAdd.setVisibility(View.VISIBLE);
            holder.tvNumber.setVisibility(View.GONE);
            holder.llMeterReadings.setVisibility(View.GONE);
            holder.llKGMeterReadings.setVisibility(View.GONE);

            holder.llAddRemove.setVisibility(View.VISIBLE);
            holder.tvAvailableQty.setText(context.getString(R.string.stock) + " : " + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
            if (from.equalsIgnoreCase("AddProducts")) {
                holder.tvAvailableQty.setVisibility(View.VISIBLE);
            } else {
                holder.tvAvailableQty.setVisibility(View.VISIBLE);

            }
//            activeDeliveryDO.orderedQuantity = activeDeliveryDO.totalQuantity;
//            holder.tvNumberET.setText("" + activeDeliveryDO.totalQuantity);// to fix issue at added products showing full qty instead selected qty in add productlistactivity
            holder.tvNumberET.setText("" + activeDeliveryDO.orderedQuantity);
        } else if (activeDeliveryDO.shipmentProductType.equalsIgnoreCase(AppConstants.MeterReadingProduct)
                && !from.equalsIgnoreCase("Shipments") && !from.equalsIgnoreCase("AddProducts")) {
            holder.tvNumberET.setVisibility(View.VISIBLE);
            holder.ivRemove.setVisibility(View.VISIBLE);
            holder.ivAdd.setVisibility(View.VISIBLE);
            holder.tvNumber.setVisibility(View.GONE);
            holder.etTotalMeterQty.setText("" + activeDeliveryDO.orderedQuantity);
            holder.etKGTotalMeterQty.setText("" + activeDeliveryDO.orderedQuantity);

            holder.tvNumberET.setText("");
            if (activeDeliveryDO.productUnit.length() > 0) {
                holder.tvProductUnit.setText("Qty1" + (activeDeliveryDO.productUnit));
                if (activeDeliveryDO.productUnit.equalsIgnoreCase("KG")) {
                    holder.llKGMeterReadings.setVisibility(View.VISIBLE);
                    holder.llMeterReadings.setVisibility(View.GONE);
                    holder.tvKGProductUnit.setText("Qty1 (" + (activeDeliveryDO.productUnit) + ")");

                } else {
                    holder.llMeterReadings.setVisibility(View.VISIBLE);
                    holder.llKGMeterReadings.setVisibility(View.GONE);
                    holder.tvProductUnit.setText("Qty1 (" + (activeDeliveryDO.productUnit) + ")");

                }

            } else {
                holder.llMeterReadings.setVisibility(View.VISIBLE);
                holder.llKGMeterReadings.setVisibility(View.GONE);

            }
            holder.llAddRemove.setVisibility(View.GONE);
//            holder.etTotalMeterQty.setText("");
//            holder.etTotalMeterQty.append("" + activeDeliveryDO.totalQuantity);
//            holder.etKGTotalMeterQty.setText("");
//            holder.etKGTotalMeterQty.append("" + activeDeliveryDO.totalQuantity);

            holder.tvAvailableQty.setVisibility(View.GONE);
        } else if (activeDeliveryDO.productType.equalsIgnoreCase(AppConstants.ShipmentListView)) {
            holder.tvNumberET.setVisibility(View.GONE);
            holder.ivRemove.setVisibility(View.GONE);
            holder.ivAdd.setVisibility(View.GONE);
            holder.tvNumber.setVisibility(View.VISIBLE);
            holder.tvAvailableQty.setVisibility(View.GONE);
            holder.tvNumber.setText("" + activeDeliveryDO.totalQuantity + " " + activeDeliveryDO.unit);
        }

        holder.cbSelected.setOnCheckedChangeListener(null);
        holder.cbSelected.setChecked(activeDeliveryDO.isProductAdded);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (activeDeliveryDO.totalQuantity > 0) {
                    activeDeliveryDO.isProductAdded = isChecked;
                    if (isChecked) {
                        selectedactiveDeliveryDOs.add(activeDeliveryDO);
                    } else {
                        selectedactiveDeliveryDOs.remove(activeDeliveryDO);
                    }
                } else {
                    holder.cbSelected.setChecked(false);
                }

            }
        });
        int maxCount = activeDeliveryDO.totalQuantity;//Integer.parseInt(holder.tvNumberET.getText().toString());
        final int[] quantity = {activeDeliveryDO.orderedQuantity};//{Integer.parseInt(holder.tvNumberET.getText().toString())};
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quantity[0] < maxCount) {
                    int docType = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
                    if (docType == 2) {
                        quantity[0] = quantity[0] + 1;
                        ActiveDeliveryMainDO activeDeliverySavedDo = StorageManager.getInstance(context).getActiveDeliveryMainDo(context);
                        double proW8 = activeDeliveryDO.productWeight * quantity[0];
                        if (proW8 > activeDeliverySavedDo.vehicleCapacity) {

                            ((BaseActivity) context).showToast(context.getString(R.string.exceed_capacity));

                        } else {
                            holder.tvNumberET.setText("" + quantity[0]++);
                            activeDeliveryDO.orderedQuantity = quantity[0];
                            if (context instanceof ScheduledCaptureDeliveryActivity) {

                                ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);


                            }
                        }
                    } else {
                        quantity[0] = quantity[0] + 1;
                        holder.tvNumberET.setText("" + quantity[0]++);
                        activeDeliveryDO.orderedQuantity = quantity[0];
                        if (context instanceof ScheduledCaptureDeliveryActivity) {

                            ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);


                        }
                    }


                }
            }
        });
        holder.ivRemove.setOnClickListener(view -> {
            if (quantity[0] > 1) {
                int docType = ((BaseActivity) context).preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);
                if (docType == 2) {

                } else {
                    quantity[0] = quantity[0] - 1;
                    holder.tvNumberET.setText("" + quantity[0]);
                    activeDeliveryDO.orderedQuantity = quantity[0];
                    if (context instanceof ScheduledCaptureDeliveryActivity) {
                        ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);
                    }
                }


            }
        });
        holder.tvNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase("")) {
                    if (Integer.parseInt(s.toString()) <= maxCount && Integer.parseInt(s.toString()) > 0) {
                        quantity[0] = Integer.parseInt(s.toString());
                        activeDeliveryDO.orderedQuantity = quantity[0];
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).updateConfirmRequestButtons(activeDeliveryDOS);
                        }
                    } else {
                        holder.tvNumberET.setText("1");
                        if (s.length() > 1) {
                            final String newText = s.toString().substring(0, s.length() - 1) + "";
                            holder.tvNumberET.setText("" + newText);
                        }
                    }
                } else {
                    holder.tvNumberET.setText("1");
                }
            }
        });
        if (context instanceof ScheduledCaptureDeliveryActivity) {
            holder.ivDelete.setVisibility(View.GONE);
        } else {
            holder.ivDelete.setVisibility(View.GONE);
        }
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof ScheduledCaptureDeliveryActivity) {
                    ((ScheduledCaptureDeliveryActivity) context).deleteShipmentProducts(activeDeliveryDO);
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from.equalsIgnoreCase("PICKUP")) {

                }
            }
        });

        final int orderedQuantity = activeDeliveryDO.orderedQuantity;

        TextWatcher textWatcher1 = new TextWatcher() {// should be less than read 2
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
                        holder.tvInfo.setText("Info:  Please enter Read 1 value");
                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                    } else {
                        holder.tvInfo.setText("");
                        double reading1 = Integer.parseInt(holder.etMeterReading1.getText().toString().trim());
                        activeDeliveryDO.orderedQuantity = 0;
                        holder.etMeterReading2.setText("");
                        holder.etMeterReading3.setText("");
                        activeDeliveryDO.openingQuantity = reading1;
                        activeDeliveryDO.orderedQuantity = maxCount;
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        };

        TextWatcher textWatcher2 = new TextWatcher() {//should be less
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {// wherever no need to display toast you can remove

                if (holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
                    holder.tvInfo.setText("Info:  Please enter Read 1 value");
                    if (context instanceof ScheduledCaptureDeliveryActivity) {
                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                    }
                    holder.etMeterReading2.removeTextChangedListener(this);
                    holder.etMeterReading2.setText("");
                    holder.etMeterReading2.addTextChangedListener(this);
                } else {
                    if (holder.etMeterReading2.getText().toString().trim().equalsIgnoreCase("")) {
//                        if (holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase("")) {
//                        }
                        holder.tvInfo.setText("Info : Please enter Read 2 value");
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                        }
                    } else {
                        int reading1 = Integer.parseInt(holder.etMeterReading1.getText().toString().trim());
                        int reading2 = Integer.parseInt(holder.etMeterReading2.getText().toString().trim());
                        int qty2 = reading2 - reading1;
                        if (qty2 > 0) {
                            holder.etMeterReading3.setText("" + qty2);
                        } else {
                            holder.etMeterReading3.setText("");
                        }
                        if (reading2 > reading1) {
                            if (context instanceof ScheduledCaptureDeliveryActivity) {
                                ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
                            }
                            activeDeliveryDO.openingQuantity = Double.valueOf(holder.etMeterReading1.getText().toString().trim());
                            activeDeliveryDO.endingQuantity = Double.valueOf(holder.etMeterReading2.getText().toString().trim());
                            activeDeliveryDO.orderedQuantity = Integer.parseInt(holder.etMeterReading3.getText().toString().trim());
                        } else {
                            holder.tvInfo.setText("Info : Please enter higher value than Read 1");
                            if (context instanceof ScheduledCaptureDeliveryActivity) {
                                ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                            }
                        }
                    }
                }
            }
        };

        TextWatcher textWatcher3 = new TextWatcher() {//should be less
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {// wherever no need to display toast you can remove

                if (holder.etMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
                    holder.tvInfo.setText("Info:  Please enter Read 1 value");
                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                    holder.etMeterReading3.removeTextChangedListener(this);
                    holder.etMeterReading3.setText("");
                    holder.etMeterReading3.addTextChangedListener(this);
                } else {
                    if (holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase("")) {
//                        holder.tvInfo.setText("Info:  Please enter quantity2 value");
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                        }
                    } else {
                        holder.tvInfo.setText("");
                        if (!holder.etMeterReading2.hasFocus()) {
                            int read3 = Integer.parseInt(holder.etMeterReading3.getText().toString().trim());
                            int read1 = Integer.parseInt(holder.etMeterReading1.getText().toString().trim());
                            int read2 = read1 + read3;
                            if (read3 > 0) {
                                holder.etMeterReading2.removeTextChangedListener(textWatcher2);
                                holder.etMeterReading2.setText("" + read2);
                                holder.etMeterReading2.addTextChangedListener(textWatcher2);
                                if (context instanceof ScheduledCaptureDeliveryActivity) {
                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
                                }
                                activeDeliveryDO.openingQuantity = Double.valueOf(holder.etMeterReading1.getText().toString().trim());
                                activeDeliveryDO.endingQuantity = Double.valueOf(holder.etMeterReading2.getText().toString().trim());
                                activeDeliveryDO.orderedQuantity = Integer.parseInt(holder.etMeterReading3.getText().toString().trim());
                            } else {
                                holder.etMeterReading3.removeTextChangedListener(this);
                                holder.etMeterReading3.setText("");
                                holder.etMeterReading3.addTextChangedListener(this);
                                if (context instanceof ScheduledCaptureDeliveryActivity) {
                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                                }
                            }
                        }
                    }
                }
            }
        };

        holder.etMeterReading1.addTextChangedListener(textWatcher1);

        holder.etMeterReading2.addTextChangedListener(textWatcher2);

        holder.etMeterReading3.addTextChangedListener(textWatcher3);

        TextWatcher textWatcher4 = new TextWatcher() {// should be less than read 2
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (holder.etKGMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
                        holder.tvInfo.setText("Info:  Please enter Read 1 value");
                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                    } else {
                        holder.tvInfo.setText("");
                        double reading1 = Integer.parseInt(holder.etKGMeterReading1.getText().toString().trim());
                        activeDeliveryDO.orderedQuantity = 0;
                        holder.etKGMeterReading2.setText("");
                        holder.etKGMeterReading3.setText("");
                        activeDeliveryDO.openingQuantity = reading1;
                        activeDeliveryDO.orderedQuantity = maxCount;
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        };

        TextWatcher textWatcher5 = new TextWatcher() {//should be less
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {// wherever no need to display toast you can remove

                if (holder.etKGMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
                    holder.tvInfo.setText("Info:  Please enter Read 1 value");
                    if (context instanceof ScheduledCaptureDeliveryActivity) {
                        ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                    }
                    holder.etKGMeterReading2.removeTextChangedListener(this);
                    holder.etKGMeterReading2.setText("");
                    holder.etKGMeterReading2.addTextChangedListener(this);
                } else {
                    if (holder.etKGMeterReading2.getText().toString().trim().equalsIgnoreCase("")) {
//                        if (holder.etMeterReading3.getText().toString().trim().equalsIgnoreCase("")) {
//                        }
                        holder.tvInfo.setText("Info : Please enter Read 2 value");
                        holder.etKGMeterReading3.removeTextChangedListener(this);
                        holder.etKGMeterReading3.setText("");
                        holder.etKGMeterReading3.addTextChangedListener(this);
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                        }
                    } else {
                        int reading1 = Integer.parseInt(holder.etKGMeterReading1.getText().toString().trim());
                        int reading2 = Integer.parseInt(holder.etKGMeterReading2.getText().toString().trim());
                        int qty2 = reading1 - reading2;
                        if (qty2 > 0) {
                            holder.etKGMeterReading3.removeTextChangedListener(this);
                            holder.etKGMeterReading3.setText("" + qty2);
                            holder.etKGMeterReading3.addTextChangedListener(this);
                        } else {
                            holder.etKGMeterReading3.removeTextChangedListener(this);
                            holder.etKGMeterReading3.setText("");
                            holder.etKGMeterReading3.addTextChangedListener(this);
                        }
                        if (reading1 > reading2) {
                            if (context instanceof ScheduledCaptureDeliveryActivity) {
                                ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
                            }
                            activeDeliveryDO.openingQuantity = Double.valueOf(holder.etKGMeterReading1.getText().toString().trim());
                            activeDeliveryDO.endingQuantity = Double.valueOf(holder.etKGMeterReading2.getText().toString().trim());
                            activeDeliveryDO.orderedQuantity = Integer.parseInt(holder.etKGMeterReading3.getText().toString().trim());
                        } else {
                            holder.tvInfo.setText("Info : Please enter lower value than Read 1");
                            if (context instanceof ScheduledCaptureDeliveryActivity) {
                                ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                            }
                        }
                    }
                }
            }
        };

        TextWatcher textWatcher6 = new TextWatcher() {//should be less
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {// wherever no need to display toast you can remove

                if (holder.etKGMeterReading1.getText().toString().trim().equalsIgnoreCase("")) {
                    holder.tvInfo.setText("Info:  Please enter Read 1 value");
                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                    holder.etKGMeterReading3.removeTextChangedListener(this);
                    holder.etKGMeterReading3.setText("");
                    holder.etKGMeterReading3.addTextChangedListener(this);
                } else {
                    if (holder.etKGMeterReading3.getText().toString().trim().equalsIgnoreCase("")) {
//                        holder.tvInfo.setText("Info:  Please enter quantity2 value");
                        if (context instanceof ScheduledCaptureDeliveryActivity) {
                            ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                        }
                    } else {
                        holder.tvInfo.setText("");
                        int read3 = 0;
                        int read2 = 0;
                        int read1 = 0;

                        if (!holder.etKGMeterReading2.hasFocus()) {
                            String kgRead3 = holder.etKGMeterReading3.getText().toString().trim();
                            String kgRead2 = holder.etKGMeterReading2.getText().toString().trim();
                            String kgRead1 = holder.etKGMeterReading1.getText().toString().trim();

                            if (!kgRead3.isEmpty()) {
                                read3 = Integer.parseInt(kgRead3);
                            }
                            if (!kgRead2.isEmpty()) {
                                read2 = Integer.parseInt(kgRead2);
                            }
                            if (!kgRead1.isEmpty()) {
                                read1 = Integer.parseInt(kgRead1);
                            }
                            read2 = read1 - read3;
                            if (read2 > 0) {
                                if (context instanceof ScheduledCaptureDeliveryActivity) {
                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(true);
                                }
                                holder.etKGMeterReading2.removeTextChangedListener(textWatcher5);
                                holder.etKGMeterReading2.setText("" + read2);
                                holder.etKGMeterReading2.addTextChangedListener(textWatcher5);


                                activeDeliveryDO.openingQuantity = Double.valueOf(kgRead1);
                                activeDeliveryDO.endingQuantity = Double.valueOf(read2);
                                activeDeliveryDO.orderedQuantity = read3;
                            } else {
                                holder.etKGMeterReading3.removeTextChangedListener(this);
                                holder.etKGMeterReading3.setText("");
                                holder.etKGMeterReading3.addTextChangedListener(this);
                                if (context instanceof ScheduledCaptureDeliveryActivity) {
                                    ((ScheduledCaptureDeliveryActivity) context).enableDisableConfirm(false);
                                }
                            }
                        }
                    }
                }
            }
        };
        holder.etKGMeterReading1.addTextChangedListener(textWatcher4);

        holder.etKGMeterReading2.addTextChangedListener(textWatcher5);

        holder.etKGMeterReading3.addTextChangedListener(textWatcher6);

    }

    @Override
    public int getItemCount() {
        return activeDeliveryDOS != null ? activeDeliveryDOS.size() : 0;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription, tvText,tvAvailableQty, tvInfo, tvNumber, etTotalMeterQty, etKGTotalMeterQty, tvSelection, tvProductUnit, tvKGProductUnit;
        public RelativeLayout rlRemove, rlAdd;
        public ImageView ivRemove, ivAdd, ivDelete;
        public Button btnSerialLot;
        public LinearLayout llAddRemove, llMeterReadings, llSelectReason, llKGMeterReadings;
        public EditText tvNumberET, etMeterReading1, etMeterReading2, etMeterReading3, etKGMeterReading1, etKGMeterReading2, etKGMeterReading3;

        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = view.findViewById(R.id.tvName);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvNumber = view.findViewById(R.id.tvNumber);
            ivRemove = view.findViewById(R.id.ivRemove);
            btnSerialLot = view.findViewById(R.id.btnSerialLot);
            tvText = view.findViewById(R.id.tvText);
            ivAdd = view.findViewById(R.id.ivAdd);
            tvNumberET = view.findViewById(R.id.tvNumberET);
            tvAvailableQty = view.findViewById(R.id.tvAvailableQty);
            tvInfo = view.findViewById(R.id.tvInfo);
            tvProductUnit = view.findViewById(R.id.tvProductUnit);
            etMeterReading1 = view.findViewById(R.id.etMeterReading1);
            etMeterReading2 = view.findViewById(R.id.etMeterReading2);
            etTotalMeterQty = view.findViewById(R.id.etTotalMeterQty);
            etMeterReading3 = view.findViewById(R.id.etMeterReading3);
            llMeterReadings = view.findViewById(R.id.llMeterReadings);
            tvKGProductUnit = view.findViewById(R.id.tvKGProductUnit);
            etKGMeterReading1 = view.findViewById(R.id.etKGMeterReading1);
            etKGMeterReading2 = view.findViewById(R.id.etKGMeterReading2);
            etKGTotalMeterQty = view.findViewById(R.id.etKGTotalMeterQty);
            etKGMeterReading3 = view.findViewById(R.id.etKGMeterReading3);
            llKGMeterReadings = view.findViewById(R.id.llKGMeterReadings);
            ivDelete = view.findViewById(R.id.ivDelete);
            cbSelected = view.findViewById(R.id.cbSelected);
            llAddRemove = view.findViewById(R.id.llAddRemove);
            llSelectReason = view.findViewById(R.id.llSelectReason);
            tvSelection = view.findViewById(R.id.tvSelection);

        }
    }
//
//    @Override
//    public void onResponseReceived(int reasonId, String reason) {
//        Log.d("REason", "Selected"+reason);
//        ((ScheduledCaptureDeliveryActivity)context).preferenceUtils.saveString(PreferenceUtils.DAMAGE_REASON,String.valueOf(reason));
//        ((ScheduledCaptureDeliveryActivity)context).preferenceUtils.saveInt(PreferenceUtils.DAMAGE_REASON_ID,reasonId);
//
//        notifyDataSetChanged();
//
//
//    }

    public void reasons(ArrayList<ReasonDO> reasonDoS) {

    }

    class ReasonAdapter extends BaseAdapter {
        private TextView tvQuantity;

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.reason_text, null);


            tvQuantity = v.findViewById(R.id.tvQuantity);
            tvQuantity.setText(reasonMainDo.reasonDOS.get(i).reason);

            return v;
        }

        @Override
        public int getCount() {
            if (reasonMainDo.reasonDOS.size() > 0)
                return reasonMainDo.reasonDOS.size();
            else
                return 0;
        }

    }
}
