package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Environment
import android.os.Looper
import androidx.core.app.ActivityCompat
import android.util.AttributeSet
import android.util.Base64
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.PermissionRequest
import android.widget.*
import androidx.core.widget.NestedScrollView
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.single.PermissionListener
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.Model.CustomerManagementDO
import com.tbs.generic.vansales.Model.FileDetails
import com.tbs.generic.vansales.Model.PodDo
import com.tbs.generic.vansales.Model.ValidateDeliveryDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest
//import com.tbs.generic.vansales.Requests.CustomerDeliveryAuthorizationsRequest
import com.tbs.generic.vansales.Requests.CylinderIssueRequest
import com.tbs.generic.vansales.Requests.ValidateDeliveryRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.pdfs.BulkDeliveryNotePDF
import com.tbs.generic.vansales.pdfs.CYLDeliveryNotePDF
import com.tbs.generic.vansales.pdfs.CylinderIssRecPdfBuilder
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.LogUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_digital_signature.*
import kotlinx.android.synthetic.main.include_image_caputure.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.DateFormat
import java.util.*

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class DocSignatureScreen : BaseActivity(), View.OnClickListener {
    private lateinit var filesPreviewAdapter: FilesPreviewAdapter
    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    lateinit var savedPath: String
    lateinit var encodedImage: String
    lateinit var validateDO: ValidateDeliveryDO
    lateinit var podDo: PodDo
    var docType=0
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvShipmentNumber: TextView
    private var mLastUpdateTime: String? = null
    private val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
    private val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    private val REQUEST_CHECK_SETTINGS = 100
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var mLocationSettingsRequest: LocationSettingsRequest? = null
    private var mLocationCallback: LocationCallback? = null
    private var mRequestingLocationUpdates: Boolean? = null
    var ticket = ""
    private var mCurrentLocation: Location? = null
    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/UserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"signature"
    var StoredPath = DIRECTORY + pic_name + ".png"
    private var fileDetailsList: ArrayList<FileDetails>? = null

    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.activity_digital_signature, null) as NestedScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
//        location()
        disableMenuWithBackButton()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            showToast(getString(R.string.please_save_Sig))
        }


        imagesSetUp()
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4 - fileDetailsList!!.size)
            } else {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4)
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }
    }

    override fun initializeControls() {
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        podDo = StorageManager.getInstance(this).getDepartureData(this);
        docType = preferenceUtils.getIntFromPreference(PreferenceUtils.DOC_TYPE, 0);

//        tvCustomerName.setText("Customer : " + preferenceUtils.getStringFromPreference(PreferenceUtils.CUSTOMER_NAME, ""))
        tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView
        if (intent.hasExtra("PICK_TICKET")) {
            ticket = intent.getExtras()?.getString("PICK_TICKET").toString()
        }
        tvScreenTitle.setText(getString(R.string.confirm_sign))
        view = mContent
        mGetSign.setOnClickListener(this@DocSignatureScreen)
        mClear.setOnClickListener(this@DocSignatureScreen)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }

        if (Util.isNetworkAvailable(this)) {
            shipmentDetails()

        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }


    }

    private fun shipmentDetails() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

        if (ticket.equals("TICKET")) {
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(docType,spotSaleDeliveryId, this@DocSignatureScreen)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {

                        Toast.makeText(this@DocSignatureScreen, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                    } else {
                        tvShipmentNumber.setText(getString(R.string.delivery_number) + " : " + spotSaleDeliveryId)

                        var loadStockAdapter = ScheduledProductsAdapter(this@DocSignatureScreen, activeDeliveryDo.activeDeliveryDOS, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                    }
                }
                driverListRequest.execute()

            }
        } else {
            if (id.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(docType,id, this@DocSignatureScreen)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {

                        Toast.makeText(this@DocSignatureScreen, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                    } else {
                        tvShipmentNumber.setText(getString(R.string.delivery_number) + " : " + id)
                        var loadStockAdapter = ScheduledProductsAdapter(this@DocSignatureScreen, activeDeliveryDo.activeDeliveryDOS, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                    }
                }
                driverListRequest.execute()

            } else if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(docType,spotSaleDeliveryId, this@DocSignatureScreen)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (isError) {

                        Toast.makeText(this@DocSignatureScreen, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                    } else {
                        tvShipmentNumber.setText(getString(R.string.delivery_number) + " : " + spotSaleDeliveryId)
                        var loadStockAdapter = ScheduledProductsAdapter(this@DocSignatureScreen, activeDeliveryDo.activeDeliveryDOS, "Shipments")
                        recycleview.setAdapter(loadStockAdapter)
                    }
                }
                driverListRequest.execute()

            }
        }

    }

    override fun onClick(v: View?) {
        Util.preventTwoClick(v)
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()
            mGetSign.setEnabled(false)
        } else if (v === mGetSign) {

            if (!lattitudeFused.isNullOrEmpty()) {
                stopLocationUpdates()

                if (Build.VERSION.SDK_INT >= 23) {
                    if (isStoragePermissionGranted()) {

                        showAppCompatAlert("", getString(R.string.are_confirm_delivery), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

                    }
                } else {

                    showAppCompatAlert("", getString(R.string.are_confirm_delivery), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

                }
            } else {
                showLoader()
                stopLocationUpdates()
                locationFetch()
//                showAppCompatAlert("", resources.getString(R.string.location_capture), "OK", "", "", false)
            }


        }
    }

    override fun onButtonYesClick(from: String) {
        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        } else {
            Toast.makeText(this, getString(R.string.the_app_not_allowed), Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        @SuppressLint("WrongThread")
        fun save(v: View, StoredPath: String) {
            Log.v("log_tag", "Width: " + v.getWidth())
            Log.v("log_tag", "Height: " + v.getHeight())
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
            }
            val canvas = bitmap?.let { Canvas(it) }
            try {
                val mFileOutStream = FileOutputStream(StoredPath)
                v.draw(canvas)
                mFileOutStream.flush()
                mFileOutStream.close()
                val bos = ByteArrayOutputStream();
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                val bitmapdata = bos.toByteArray();
                Log.e("Signature Size : ", "Size : " + bitmapdata)
                encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                podDo.signatureEncode = encodedImage;
                savedPath = StoredPath
                StorageManager.getInstance(context).saveDepartureData(this@DocSignatureScreen, podDo)
                if (Util.isNetworkAvailable(context)) {
                    if (!path.isEmpty) {
                        validatedelivery()
                    } else {
                        showToast(context.getString(R.string.please_do_sig))
                    }

                } else {
                    showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                }


            } catch (e: Exception) {
                Log.v("log_tag", e.toString())
            }

        }

        fun clear() {
            path.reset()
            invalidate()
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.getWidth(),
                    maxImageSize.toFloat() / realImage.getHeight())
            val width = Math.round(ratio.toFloat() * realImage.getWidth())
            val height = Math.round(ratio.toFloat() * realImage.getHeight())
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        protected override fun onDraw(canvas: Canvas) {
            nested_scroll.requestDisallowInterceptTouchEvent(true);
            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }

    private fun validatedelivery() {

        setUpImagesAs64Bit()

        if (ticket.equals("TICKET")) {
            val shipmentNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
            validatedeliveryRequest(shipmentNumber)
        } else {
            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, getResources().getString(R.string.checkin_non_scheduled))
            if (ShipmentType.equals(getResources().getString(R.string.checkin_non_scheduled), true)) {
                val shipmentNumber = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")

                val driverListRequest = ValidateDeliveryRequest(podDo.capturedImagesListBulk,podDo.signature,podDo.deliveryNotes, shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@DocSignatureScreen)
                driverListRequest.setOnResultListener { isError, validateDo, msg ->
                    hideLoader()
                    if (isError) {
                        hideLoader()

                        if (msg.isNotEmpty()) {
                            showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                        }
                    } else {
                        stopLocUpdates()

                        validateDO = validateDo
                        if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                            hideLoader()

                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                            }
                            prepareDeliveryNoteCreation()
                        } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {

                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                            }
                            prepareDeliveryNoteCreation()

                        } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {

                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_already_validate), Toast.LENGTH_SHORT).show()
                            }
                            prepareDeliveryNoteCreation()
                        } else {
                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)
                            } else {
                                showToast(getString(R.string.shipment_not_validate))
                            }

                        }

                    }
                }
                driverListRequest.execute()
            } else {
                var activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)

                val driverListRequest = ValidateDeliveryRequest(podDo.capturedImagesListBulk,podDo.signature,podDo.deliveryNotes, activeDeliverySavedDo.shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@DocSignatureScreen)
                driverListRequest.setOnResultListener { isError, validateDo, msg ->
                    hideLoader()
                    if (isError) {
                        hideLoader()
                        if (msg.isNotEmpty()) {
                            showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                        } else {
                            showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                        }
                    } else {
                        validateDO = validateDo
                        if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                            hideLoader()

                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                            }

                            prepareDeliveryNoteCreation()

                        } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                            hideLoader()

                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                            }

                            prepareDeliveryNoteCreation()

                        } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {

                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_already_validate), Toast.LENGTH_SHORT).show()
                            }

                            prepareDeliveryNoteCreation()
                        } else {
                            if (validateDo.message.length > 0) {
                                showToast("" + validateDo.message)

                            } else {

                                Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_not_validate), Toast.LENGTH_SHORT).show()
                            }


                        }

                    }
                }
                driverListRequest.execute()
            }
        }


    }

    private fun setUpImagesAs64Bit() {

        for (list in this.fileDetailsList!!) {
            try {
                val fis = FileInputStream(File(list.filePath))
                val bitmap = BitmapFactory.decodeStream(fis)
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val bytes = baos.toByteArray()
                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                LogUtils.debug("PIC-->", encImage)
                podDo.capturedImagesListBulk!!.add(encImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        StorageManager.getInstance(this).saveDepartureData(this@DocSignatureScreen, podDo)

    }

    private fun validatedeliveryRequest(shipmentNumber: String?) {
        val driverListRequest = ValidateDeliveryRequest(podDo.capturedImagesListBulk,podDo.signature,podDo.deliveryNotes, shipmentNumber, lattitudeFused, longitudeFused, addressFused, this@DocSignatureScreen)
        driverListRequest.setOnResultListener { isError, validateDo, msg ->
            hideLoader()
            if (isError) {
                hideLoader()

                if (msg.isNotEmpty()) {
                    showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.success), false)

                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.success), false)

                }
            } else {
                validateDO = validateDo
                if (validateDo.flag == 1 && validateDo.flag2 == 2) {
                    hideLoader()

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                    }
                    prepareDeliveryNoteCreation()

                } else if (validateDo.flag == 1 && validateDo.flag2 == 1) {
                    hideLoader()

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_validated), Toast.LENGTH_SHORT).show()
                    }

                    prepareDeliveryNoteCreation()

                } else if (validateDo.flag == 2 && validateDo.flag2 == 2) {

                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)

                    } else {

                        Toast.makeText(this@DocSignatureScreen, getString(R.string.shipment_already_validate), Toast.LENGTH_SHORT).show()
                    }

                    prepareDeliveryNoteCreation()
                } else {
                    if (validateDo.message.length > 0) {
                        showToast("" + validateDo.message)
                    } else {
                        showToast(getString(R.string.shipment_not_validate))
                    }

                }

            }
        }
        driverListRequest.execute()

    }


    private fun prepareDeliveryNoteCreation() {
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")
        val spotSaleDeliveryId = preferenceUtils.getStringFromPreference(PreferenceUtils.SPOT_DELIVERY_NUMBER, "")
        if (id.length > 0) {
            val driverListRequest = ActiveDeliveryRequest(docType,id, this@DocSignatureScreen)
            driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                hideLoader()
                if (activeDeliveryDo != null) {
                    if (isError) {
                        hideLoader()

                    } else {
                        if (validateDO.email == 2) {
                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                            } else {
                                CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                            }
                        } else {
                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")
                        }


//                        if (validateDO.print == 2) {
//                            if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
//                                printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
//                            } else {
//                                printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
//                            }
//                        } else {
//                            preferenceUtils.saveString(PreferenceUtils.DELIVERY_NOTE, "SUCCESS")
//                        }


                    }
                } else {
                    showToast("Server Error. Please Try again!!")

//                    showAppCompatAlert("Error", "Server Error. Please Try again!!", "Ok", "", "", false)
                }
            }
            driverListRequest.execute()

        } else {
            if (spotSaleDeliveryId.length > 0) {
                val driverListRequest = ActiveDeliveryRequest(docType,spotSaleDeliveryId, this@DocSignatureScreen)
                driverListRequest.setOnResultListener { isError, activeDeliveryDo ->
                    hideLoader()
                    if (activeDeliveryDo != null) {
                        if (isError) {
                            showToast(getString(R.string.server_erro))
//                            showAppCompatAlert("Error", "Delivery Note pdf data error", "Ok", "", "", false)
                        } else {
//                            if (validateDO.print == 2) {
//
//                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
//                                    printDocument(activeDeliveryDo, PrinterConstants.PrintBulkDeliveryNotes);
//                                } else {
//                                    printDocument(activeDeliveryDo, PrinterConstants.PrintCylinderDeliveryNotes);
//                                }
//                            }
                            if (validateDO.email == 2) {

                                if (preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentProductsType, "").equals(AppConstants.MeterReadingProduct)) {
                                    BulkDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                } else {
                                    CYLDeliveryNotePDF(this).createDeliveryPDF(activeDeliveryDo, "Email").CreateDeliveryPDF().execute()
                                }
                            }

                        }
                    } else {
                        showToast(getString(R.string.server_erro))
                    }
                }
                driverListRequest.execute()
            } else {

                showAppCompatAlert(getString(R.string.info), getString(R.string.please_create_del), getString(R.string.ok), "", "", false)

            }

        }
        podDo.podTimeCaptureValidateDeliveryTime = CalendarUtils.getTime()
        podDo.podTimeCaptureValidateDeliveryDate = CalendarUtils.getDate()
        StorageManager.getInstance(this).saveDepartureData(this, podDo)
        val intent = Intent()
        intent.putExtra("SIGNATURE", StoredPath)
        intent.putExtra("SignatureEncode", encodedImage)
        setResult(11, intent)
        finish()

    }

    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@DocSignatureScreen)
        if (printConnection.isBluetoothEnabled()) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast(getString(R.string.please_enable_your_bluetooth))
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices()
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.getName()
                val mac = device.getAddress() // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac);
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter)
    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        hideLoader()
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE);
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }


    override fun onBackPressed() {
        showToast("Please save signature")

    }


    private fun imagesSetUp() {

        fileDetailsList = ArrayList()
        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = View.VISIBLE

            } else {
                btnAddImages.visibility = View.GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = View.VISIBLE
            } else {
                recycleviewImages.visibility = View.GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 888) {
            val images = data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {
                Log.d("image-->", images.size.toString() + "----" + images)
                var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    fileDetailsList?.add(0, fileDetails)
                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }
    }


    fun locationFetch() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mSettingsClient = LocationServices.getSettingsClient(this)

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                // location is received
                mCurrentLocation = locationResult!!.lastLocation
                mLastUpdateTime = DateFormat.getTimeInstance().format(Date())

                updateLocUI()
            }
        }

        mRequestingLocationUpdates = false

        mLocationRequest = LocationRequest()
        mLocationRequest!!.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(permission: com.karumi.dexter.listener.PermissionRequest?, token: PermissionToken?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        mRequestingLocationUpdates = true
                        startLocUpdates()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        if (response.isPermanentlyDenied()) {

                            showToast(getString(R.string.location_permission_denied))
                            // open device settings when the permission is
                            // denied permanently
//                            openSettings()
                        }
                    }

                    fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun updateLocUI() {
        if (mCurrentLocation != null) {
            stopLocUpdates()
            hideLoader()

            lattitudeFused = mCurrentLocation!!.getLatitude().toString()
            longitudeFused = mCurrentLocation!!.getLongitude().toString()
            var gcd = Geocoder(getBaseContext(), Locale.getDefault());

            try {
                var addresses = gcd.getFromLocation(mCurrentLocation!!.getLatitude(),
                        mCurrentLocation!!.getLongitude(), 1);
                if (addresses.size > 0) {
                    System.out.println(addresses.get(0).getLocality());
                    var cityName = addresses.get(0).getLocality();
                    addressFused = cityName
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }
            showAppCompatAlert("", getString(R.string.are_confirm_delivery), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)


        }

    }

    @SuppressLint("MissingPermission")
    private fun startLocUpdates() {
        mSettingsClient!!
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this) {
                    //                    Log.i(TAG, "All location settings are satisfied.")

//                    Toast.makeText(applicationContext, "Started location updates!", Toast.LENGTH_SHORT).show()


                    mFusedLocationClient!!.requestLocationUpdates(mLocationRequest,
                            mLocationCallback, Looper.myLooper())

                    updateLocUI()
                }
                .addOnFailureListener(this) { e ->
                    val statusCode = (e as ApiException).statusCode
                    when (statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " + "location settings ")
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the
                                // result in onActivityResult().
                                val rae = e as ResolvableApiException
                                rae.startResolutionForResult(this@DocSignatureScreen, REQUEST_CHECK_SETTINGS)
                            } catch (sie: IntentSender.SendIntentException) {
//                                Log.i(TAG, "PendingIntent unable to execute request.")
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                            val errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings."
//                            Log.e(TAG, errorMessage)

                            Toast.makeText(this@DocSignatureScreen, errorMessage, Toast.LENGTH_LONG).show()
                        }
                    }

                    updateLocUI()
                }
    }

    fun stopLocUpdates() {
        // Removing location updates
//        try {
//            mFusedLocationClient!!
//                    .removeLocationUpdates(mLocationCallback)
//                    .addOnCompleteListener(this) {
//
//                    }
//        } catch (e: java.lang.Exception) {
//
//        }
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient!!
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(this) {

                    }
        }

    }
}