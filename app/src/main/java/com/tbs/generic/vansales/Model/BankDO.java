package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class BankDO implements Serializable {

    public String bankId = "";
    public String bankName = "";
    public String bankAddress = "";
    public String countryId = "";
    public String contryName = "";
    public String postalCode = "";
    public String city = "";
    public String currency = "";
    public String bankAccount = "";
    public String company = "";
    public String companyDescription = "";



}
