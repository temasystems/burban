package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.RequestApproveDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONArray;
import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class UpdateDamageCylindersRequest extends AsyncTask<String, Void, Boolean> {

    private RequestApproveDO requestApproveDO;
    private Context mContext;
    private String shipmentID, date, reason, comment;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    private ArrayList<ActiveDeliveryDO> activeDeliveryDOS;
    private int flaG;

    public UpdateDamageCylindersRequest(String shipment, ArrayList<ActiveDeliveryDO> activeDeliveryDoS, Context mContext) {

        this.mContext = mContext;

        this.activeDeliveryDOS= activeDeliveryDoS;
        this.shipmentID=shipment;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, RequestApproveDO createInvoiceDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XSDHNUM", shipmentID);
            JSONArray jsonArray =new JSONArray();
            if(activeDeliveryDOS!=null && activeDeliveryDOS.size()>0) {
                for (int i = 0; i < activeDeliveryDOS.size(); i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1.put("I_XSDDLIN", activeDeliveryDOS.get(i).linenumber+"");
                    jsonObject1.put("I_XQTY",  activeDeliveryDOS.get(i).orderedQuantity+"");
                    jsonObject1.put("I_XREASON", activeDeliveryDOS.get(i).reasonId+"");
                    jsonArray.put(i, jsonObject1);
                }
                jsonObject.put("GRP2", jsonArray);
            }
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.UPDATE_DAMAGE, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("Approval Request : xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            requestApproveDO = new RequestApproveDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        //      createPaymentDO = new CustomerDetailsDo();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_XFLG")) {
                            if (text.length() > 0) {

                                requestApproveDO.flag = Integer.parseInt(text);
                            }


                        }
                        else  if (attribute.equalsIgnoreCase("I_XSDHNUM")) {
                            if (text.length() > 0) {

                                requestApproveDO.shipment = text;
                            }


                        }
                        else  if (attribute.equalsIgnoreCase("I_RESDATE")) {
                            if (text.length() > 0) {

                                requestApproveDO.date =text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("I_YREASON")) {
                            if (text.length() > 0) {

                                requestApproveDO.reason =text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("I_YCOMENT")) {
                            if (text.length() > 0) {

                                requestApproveDO.comment = text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YCONF")) {
                            if (text.length() > 0) {

                                requestApproveDO.status = text;
                            }


                        }
                    }


                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();

        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, requestApproveDO);
        }
    }
}