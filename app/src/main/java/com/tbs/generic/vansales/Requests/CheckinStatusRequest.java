package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.CheckinDO;
import com.tbs.generic.vansales.Model.VRSelectionDO;
import com.tbs.generic.vansales.Model.VRSelectionMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CheckinStatusRequest extends AsyncTask<String, Void, Boolean> {

    private CheckinDO checkinDO;
    private Context mContext;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;


    public CheckinStatusRequest(Context mContext) {

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, CheckinDO checkinDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_XDRIVER", id);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.CHECKIN_STATUS, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("VR selection xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            checkinDO = new CheckinDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_XVCRNUM")) {
                            checkinDO.transactionID = text;


                        } else if (attribute.equalsIgnoreCase("O_XDNAME")) {
                            if (text.length() > 0) {

                                checkinDO.driverName = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XVEHODOMTRUN")) {
                            if (text.length() > 0) {

                                checkinDO.unit = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XACTCHKIN")) {
                            if (text.length() > 0) {

                                checkinDO.actualCheckin = text;
                            }

                        }  else if (attribute.equalsIgnoreCase("O_XACTCHKOUT")) {
                            if (text.length() > 0) {

                                checkinDO.actualCheckout = text;
                            }

                        }  else if (attribute.equalsIgnoreCase("O_XSTARTODMTR")) {
                            if (text.length() > 0) {

                                checkinDO.startRead = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XENDODMTR")) {
                            if (text.length() > 0) {

                                checkinDO.endRead = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XDRIVERINST")) {
                            if (text.length() > 0) {

                                checkinDO.instructions = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XCURODOMTR")) {
                            if (text.length() > 0) {

                                checkinDO.odometer = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XTRIP")) {
                            if (text.length() > 0) {

                                checkinDO.trip = Integer.valueOf(text);
                            }

                        } else if (attribute.equalsIgnoreCase("O_XVEHCODE")) {
                            if (text.length() > 0) {

                                checkinDO.vehicleCode = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XVEHNO")) {
                            if (text.length() > 0) {

                                checkinDO.vehicleNumber = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XSCHDATE")) {
                            if (text.length() > 0) {

                                checkinDO.scheduleDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XFCY")) {
                            if (text.length() > 0) {

                                checkinDO.siteID = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XFCYDES")) {
                            if (text.length() > 0) {

                                checkinDO.siteDescription = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XNUMPC")) {
                            if (text.length() > 0) {

                                checkinDO.vrID = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XSTATUS")) {
                            if (text.length() > 0) {

                                checkinDO.status = Integer.parseInt(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XMSG")) {
                            if (text.length() > 0) {

                                checkinDO.message = text;
                            }
                        }
                     else if (attribute.equalsIgnoreCase("O_XSLOC")) {
                            if (text.length() > 0) {
                                checkinDO.location = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_XLOCTYP")) {
                            if (text.length() > 0) {
                                checkinDO.locationType = text;
                            }
                        }
                    else if (attribute.equalsIgnoreCase("O_XCARRIER")) {
                            checkinDO.vehicleCarrier = text;

                        }   else if (attribute.equalsIgnoreCase("O_XOCOUNT")) {
                            if (text.length() > 0) {

                                checkinDO.nShipments = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_XARRDATE")) {
                            checkinDO.aDate = text;

                        } else if (attribute.equalsIgnoreCase("O_XDLDATE")) {
                            checkinDO.dDate = text;

                        } else if (attribute.equalsIgnoreCase("O_XDPTIME")) {
                            checkinDO.dTime = text;

                        } else if (attribute.equalsIgnoreCase("O_XARRTIME")) {
                            checkinDO.aTime = text;

                        }else if (attribute.equalsIgnoreCase("O_XNOTE")) {
                            if (text.length() > 0) {

                                checkinDO.notes = text;
                            }

                        }
                       

                        else if (attribute.equalsIgnoreCase("O_XCHKIN")) {
                            if (text.length() > 0) {

                                checkinDO.checkinFlag = Integer.parseInt(text);
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_XDETFLG")) {
                            if (text.length() > 0) {

                                checkinDO.statusFlag = Integer.parseInt(text);
                            }

                        }
                        text = "";                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }



                } else if (eventType == XmlPullParser.TEXT) {
                    if (xpp.getText().length() > 0) {

                        text = xpp.getText();
                    } else {
                        text = "";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, checkinDO);
        }

    }
}