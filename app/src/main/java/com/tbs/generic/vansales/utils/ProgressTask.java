package com.tbs.generic.vansales.utils;

import android.content.Context;

public class ProgressTask {
    private CustomProgressDialog customProgressDialog;
    private static ProgressTask instance;

    private ProgressTask() {
    }

    public static ProgressTask getInstance() {
        if (instance == null) {
            instance = new ProgressTask();
        }
        return instance;
    }

    public void showProgress(Context mContext, boolean isCancalable, String mMessage) {
        customProgressDialog = new CustomProgressDialog(mContext, mMessage);
        customProgressDialog.show();
    }

    public void closeProgress() {
        if (customProgressDialog != null && customProgressDialog.isShowing())
            customProgressDialog.dismiss();
    }
}
