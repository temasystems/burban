package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class LoginDO implements Serializable {


    public String Image = "";
    public String driverCode = "";
    public String driverName = "";
    public String site = "";
    public String siteDescription = "";
    public String company = "";
    public String companyDescription  = "";
    public int chequeDays  = 0;
    public int flag = 0;
    public int role = 0;
    public String email = "";
    public String time = "24";
    public String date = "dd/MM/yyyy";
    public String version = "";
    public String sequence = "";



}
