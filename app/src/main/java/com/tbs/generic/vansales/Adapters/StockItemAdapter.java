package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.SerializationActivity;
import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.Model.StockItemDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.listeners.LoadStockListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class StockItemAdapter extends RecyclerView.Adapter<StockItemAdapter.MyViewHolder> implements Filterable {
    private ArrayList<StockItemDo> stockItemDos;
    ValueFilter valueFilter;
    private Context context;
    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<StockItemDo> filterList = new ArrayList<>();
                for (int i = 0; i < stockItemDos.size(); i++) {
                    if ((stockItemDos.get(i).productName.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(stockItemDos.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = stockItemDos.size();
                results.values = stockItemDos;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            stockItemDos = (ArrayList<StockItemDo>) results.values;
            notifyDataSetChanged();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription, tvNumber;
        Button btnSerial;

        public MyViewHolder(View view) {
            super(view);
            tvProductName = view.findViewById(R.id.tvName);
            tvDescription = view.findViewById(R.id.tvDescription);
            btnSerial = view.findViewById(R.id.btnSerial);
            tvNumber = view.findViewById(R.id.tvNumber);
        }
    }

    public void refreshAdapter(ArrayList<StockItemDo> loadStockDoS) {
        this.stockItemDos = loadStockDoS;
        notifyDataSetChanged();

    }

    private String from = "";

    public StockItemAdapter(Context context, ArrayList<StockItemDo> loadStockDoS, String from) {
        this.context = context;
        this.stockItemDos = loadStockDoS;
        this.from = from;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_vehicle_stock_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    private ArrayList<LoadStockDO> selectedLoadStockDOs = new ArrayList<>();

    public ArrayList<LoadStockDO> getSelectedLoadStockDOs() {
        return selectedLoadStockDOs;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        } else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_secnd_grey));
        }
        DecimalFormat df = new DecimalFormat("0.00");

        final StockItemDo stockItemDo = stockItemDos.get(position);
        holder.tvDescription.setText(stockItemDo.productDescription);


        if (stockItemDo.serialNumber.isEmpty()) {
            holder.btnSerial.setVisibility(View.GONE);
            holder.tvProductName.setText(stockItemDo.productName);
        } else {
            holder.btnSerial.setVisibility(View.VISIBLE);
            holder.tvProductName.setText(stockItemDo.productName);

        }
        holder.tvNumber.setText("" + df.format(stockItemDo.updateQty) + "  " + stockItemDo.weightUnit);
        holder.btnSerial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!stockItemDo.serialNumber.isEmpty()) {
                    Intent intent = new Intent(context, SerializationActivity.class);
                    intent.putExtra("P_ID", stockItemDo.productName);
                    intent.putExtra("PRODUCT", stockItemDo.productDescription);
                    intent.putExtra("PRODUCTDO", stockItemDo);
                    intent.putExtra("STOCK", 2);
                    ((BaseActivity) context).startActivityForResult(intent, 10);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return stockItemDos != null && stockItemDos.size() > 0 ? stockItemDos.size() : 0;
    }

    private int getCount() {
        int count = 0;
        for (LoadStockDO loadStockDO : AppConstants.listStockDO) {
            if (loadStockDO.quantity > 0) {
                count = count + 1;
            }
        }
        return count;
    }

    public void filterList(ArrayList<StockItemDo> filterdNames) {
        this.stockItemDos = filterdNames;
        notifyDataSetChanged();
    }


}
